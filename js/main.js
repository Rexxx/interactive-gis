var map, timerId, gammaType;

midojo.addOnLoad(function() {
   //ria.RestService.addProxy(" /riaproxy?url=");
});

var mapOptions = {
   projection: "epsg:3857",
	maxExtent: new OpenLayers.Bounds(
	8000000.0, 6500000.0, 10000000.0, 8500000.0),
	maxResolution: 5000.0,
	units: "m"
};

function init() {
	if (map instanceof OpenLayers.Map) {
		map.destroy();
	}
	
	map = new OpenLayers.Map("divMap", mapOptions);
	map.addLayers([getTileLayer()]);
	map.zoomToMaxExtent();
	
	gammaType = true;
}

function getTileLayer() {
	var base = new OpenLayers.Layer.TileServiceLayer(
			"NSO", "/stud17/mg11b_tiles",				
			"http://spectrum.ssga.local:8080/rest/Spatial/MapTilingService");
	base.setIsBaseLayer(true);
	return base;
}

function hideShowDiv() {
	if ($("#divMap").css("display") == "none") {
		$("#advert").fadeOut(10);
		$("#divMap").slideDown("slow");
		clearInterval(timerId);
	}
	else {
		$("#divMap").slideUp("fast");
		$("#advert").fadeIn(1000);		
		timerId = setInterval(function(){advertDesign();}, 100);		
	}
}

function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}

function advertDesign() {
	var borderStyleType = randomInteger(1, 4);
	var borderColorType = randomInteger(1, 5);
	
	switch(borderStyleType) {
		case 1: $("#advert").css("border-style", "solid");
		break;
		case 2: $("#advert").css("border-style", "dotted");
		break;
		case 3: $("#advert").css("border-style", "dashed");
		break;		
		case 4: $("#advert").css("border-style", "double");
		break;		
	}
	
	switch(borderColorType) {
		case 1: $("#advert").css("border-color", "blue");
		break;
		case 2: $("#advert").css("border-color", "red");
		break;
		case 3: $("#advert").css("border-color", "green");
		break;		
		case 4: $("#advert").css("border-color", "purple");
		break;		
		case 5: $("#advert").css("border-color", "cyan");
		break;		
	}	
}

function changeGamma() {
	if (gammaType == true) {
		$("body").css("backgroundImage", "url(img/bg-black.jpg)");
		$("h1").css("color", "white");
		$("h2").css("color", "#bbb");
		$("h2 a").css("color", "#eee");
		gammaType = false;
	}
	else {
		$("body").css("backgroundImage", "url(img/bg-white.jpg)");		
		$("h1").css("color", "black");
		$("h2").css("color", "#777");
		$("h2 a").css("color", "black");		
		gammaType = true;
	}
}