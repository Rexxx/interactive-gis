(function() {
  if(typeof this["loadFirebugConsole"] == "function") {
    this["loadFirebugConsole"]()
  }else {
    this.console = this.console || {};
    var cn = ["assert", "count", "debug", "dir", "dirxml", "error", "group", "groupEnd", "info", "profile", "profileEnd", "time", "timeEnd", "trace", "warn", "log"];
    var i$$1 = 0;
    var tn;
    for(;tn = cn[i$$1++];) {
      if(!console[tn]) {
        (function() {
          var tcn = tn + "";
          console[tcn] = "log" in console ? function() {
            var a = Array.apply({}, arguments);
            a.unshift(tcn + ":");
            console["log"](a.join(" "))
          } : function() {
          };
          console[tcn]._fake = true
        })()
      }
    }
  }
  if(typeof midojo == "undefined") {
    midojo = {_scopeName:"midojo", _scopePrefix:"", _scopePrefixArgs:"", _scopeSuffix:"", _scopeMap:{}, _scopeMapRev:{}}
  }
  var d = midojo;
  if(typeof midijit == "undefined") {
    midijit = {_scopeName:"midijit"}
  }
  if(typeof midojox == "undefined") {
    midojox = {_scopeName:"midojox"}
  }
  if(!d._scopeArgs) {
    d._scopeArgs = [midojo, midijit, midojox]
  }
  d.global = this;
  d.config = {isDebug:false, debugAtAllCosts:false};
  if(typeof djConfig != "undefined") {
    var opt;
    for(opt in djConfig) {
      d.config[opt] = djConfig[opt]
    }
  }
  midojo.locale = d.config.locale;
  var rev = "$Rev: 21516 $".match(/\d+/);
  midojo.version = {major:1, minor:4, patch:2, flag:"", revision:rev ? +rev[0] : NaN, toString:function() {
    with(d.version) {
      return major + "." + minor + "." + patch + flag + " (" + revision + ")"
    }
  }};
  if(typeof OpenAjax != "undefined") {
    OpenAjax.hub.registerLibrary(midojo._scopeName, "http://dojotoolkit.org", d.version.toString())
  }
  var extraNames;
  var extraLen;
  var empty = {};
  for(i$$1 in{toString:1}) {
    extraNames = [];
    break
  }
  midojo._extraNames = extraNames = extraNames || ["hasOwnProperty", "valueOf", "isPrototypeOf", "propertyIsEnumerable", "toLocaleString", "toString", "constructor"];
  extraLen = extraNames.length;
  midojo._mixin = function(target, source) {
    var name;
    var s$$1;
    var i$$2;
    for(name in source) {
      s$$1 = source[name];
      if(!(name in target) || target[name] !== s$$1 && (!(name in empty) || empty[name] !== s$$1)) {
        target[name] = s$$1
      }
    }
    if(extraLen && source) {
      i$$2 = 0;
      for(;i$$2 < extraLen;++i$$2) {
        name = extraNames[i$$2];
        s$$1 = source[name];
        if(!(name in target) || target[name] !== s$$1 && (!(name in empty) || empty[name] !== s$$1)) {
          target[name] = s$$1
        }
      }
    }
    return target
  };
  midojo.mixin = function(obj, props) {
    if(!obj) {
      obj = {}
    }
    var i$$3 = 1;
    var l$$1 = arguments.length;
    for(;i$$3 < l$$1;i$$3++) {
      d._mixin(obj, arguments[i$$3])
    }
    return obj
  };
  midojo._getProp = function(parts, create, context) {
    var obj$$1 = context || d.global;
    var i$$4 = 0;
    var p;
    for(;obj$$1 && (p = parts[i$$4]);i$$4++) {
      if(i$$4 == 0 && d._scopeMap[p]) {
        p = d._scopeMap[p]
      }
      obj$$1 = p in obj$$1 ? obj$$1[p] : create ? obj$$1[p] = {} : undefined
    }
    return obj$$1
  };
  midojo.setObject = function(name$$1, value, context$$1) {
    var parts$$1 = name$$1.split(".");
    var p$$1 = parts$$1.pop();
    var obj$$2 = d._getProp(parts$$1, true, context$$1);
    return obj$$2 && p$$1 ? obj$$2[p$$1] = value : undefined
  };
  midojo.getObject = function(name$$2, create$$1, context$$2) {
    return d._getProp(name$$2.split("."), create$$1, context$$2)
  };
  midojo.exists = function(name$$3, obj$$3) {
    return!!d.getObject(name$$3, false, obj$$3)
  };
  midojo["eval"] = function(scriptFragment) {
    return d.global.eval ? d.global.eval(scriptFragment) : eval(scriptFragment)
  };
  d.deprecated = d.experimental = function() {
  }
})();
(function() {
  var d$$1 = midojo;
  d$$1.mixin(d$$1, {_loadedModules:{}, _inFlightCount:0, _hasResource:{}, _modulePrefixes:{midojo:{name:"midojo", value:"."}, doh:{name:"doh", value:"../util/doh"}, tests:{name:"tests", value:"tests"}}, _moduleHasPrefix:function(module) {
    var mp = d$$1._modulePrefixes;
    return!!(mp[module] && mp[module].value)
  }, _getModulePrefix:function(module$$1) {
    var mp$$1 = d$$1._modulePrefixes;
    if(d$$1._moduleHasPrefix(module$$1)) {
      return mp$$1[module$$1].value
    }
    return module$$1
  }, _loadedUrls:[], _postLoad:false, _loaders:[], _unloaders:[], _loadNotifying:false});
  midojo._loadPath = function(relpath, module$$2, cb) {
    var uri = (relpath.charAt(0) == "/" || relpath.match(/^\w+:/) ? "" : d$$1.baseUrl) + relpath;
    try {
      return!module$$2 ? d$$1._loadUri(uri, cb) : d$$1._loadUriAndCheck(uri, module$$2, cb)
    }catch(e) {
      console.error(e);
      return false
    }
  };
  midojo._loadUri = function(uri$$1, cb$$1) {
    if(d$$1._loadedUrls[uri$$1]) {
      return true
    }
    d$$1._inFlightCount++;
    var contents = d$$1._getText(uri$$1, true);
    if(contents) {
      d$$1._loadedUrls[uri$$1] = true;
      d$$1._loadedUrls.push(uri$$1);
      if(cb$$1) {
        contents = "(" + contents + ")"
      }else {
        contents = d$$1._scopePrefix + contents + d$$1._scopeSuffix
      }
      if(!d$$1.isIE) {
        contents += "\r\n//@ sourceURL=" + uri$$1
      }
      var value$$1 = d$$1["eval"](contents);
      if(cb$$1) {
        cb$$1(value$$1)
      }
    }
    if(--d$$1._inFlightCount == 0 && d$$1._postLoad && d$$1._loaders.length) {
      setTimeout(function() {
        if(d$$1._inFlightCount == 0) {
          d$$1._callLoaded()
        }
      }, 0)
    }
    return!!contents
  };
  midojo._loadUriAndCheck = function(uri$$2, moduleName, cb$$2) {
    var ok = false;
    try {
      ok = d$$1._loadUri(uri$$2, cb$$2)
    }catch(e$$1) {
      console.error("failed loading " + uri$$2 + " with error: " + e$$1)
    }
    return!!(ok && d$$1._loadedModules[moduleName])
  };
  midojo.loaded = function() {
    d$$1._loadNotifying = true;
    d$$1._postLoad = true;
    var mll = d$$1._loaders;
    d$$1._loaders = [];
    var x = 0;
    for(;x < mll.length;x++) {
      mll[x]()
    }
    d$$1._loadNotifying = false;
    if(d$$1._postLoad && d$$1._inFlightCount == 0 && mll.length) {
      d$$1._callLoaded()
    }
  };
  midojo.unloaded = function() {
    var mll$$1 = d$$1._unloaders;
    for(;mll$$1.length;) {
      mll$$1.pop()()
    }
  };
  d$$1._onto = function(arr, obj$$4, fn) {
    if(!fn) {
      arr.push(obj$$4)
    }else {
      if(fn) {
        var func = typeof fn == "string" ? obj$$4[fn] : fn;
        arr.push(function() {
          func.call(obj$$4)
        })
      }
    }
  };
  midojo.ready = midojo.addOnLoad = function(obj$$5, functionName) {
    d$$1._onto(d$$1._loaders, obj$$5, functionName);
    if(d$$1._postLoad && d$$1._inFlightCount == 0 && !d$$1._loadNotifying) {
      d$$1._callLoaded()
    }
  };
  var dca = d$$1.config.addOnLoad;
  if(dca) {
    d$$1.addOnLoad[dca instanceof Array ? "apply" : "call"](d$$1, dca)
  }
  midojo._modulesLoaded = function() {
    if(d$$1._postLoad) {
      return
    }
    if(d$$1._inFlightCount > 0) {
      console.warn("files still in flight!");
      return
    }
    d$$1._callLoaded()
  };
  midojo._callLoaded = function() {
    if(typeof setTimeout == "object" || d$$1.config.useXDomain && d$$1.isOpera) {
      setTimeout(d$$1.isAIR ? function() {
        d$$1.loaded()
      } : d$$1._scopeName + ".loaded();", 0)
    }else {
      d$$1.loaded()
    }
  };
  midojo._getModuleSymbols = function(modulename) {
    var syms = modulename.split(".");
    var i$$5 = syms.length;
    for(;i$$5 > 0;i$$5--) {
      var parentModule = syms.slice(0, i$$5).join(".");
      if(i$$5 == 1 && !d$$1._moduleHasPrefix(parentModule)) {
        syms[0] = "../" + syms[0]
      }else {
        var parentModulePath = d$$1._getModulePrefix(parentModule);
        if(parentModulePath != parentModule) {
          syms.splice(0, i$$5, parentModulePath);
          break
        }
      }
    }
    return syms
  };
  midojo._global_omit_module_check = false;
  midojo.loadInit = function(init) {
    init()
  };
  midojo._loadModule = midojo.require = function(moduleName$$1, omitModuleCheck) {
    omitModuleCheck = d$$1._global_omit_module_check || omitModuleCheck;
    var module$$3 = d$$1._loadedModules[moduleName$$1];
    if(module$$3) {
      return module$$3
    }
    var relpath$$1 = d$$1._getModuleSymbols(moduleName$$1).join("/") + ".js";
    var modArg = !omitModuleCheck ? moduleName$$1 : null;
    var ok$$1 = d$$1._loadPath(relpath$$1, modArg);
    if(!ok$$1 && !omitModuleCheck) {
      throw new Error("Could not load '" + moduleName$$1 + "'; last tried '" + relpath$$1 + "'");
    }
    if(!omitModuleCheck && !d$$1._isXDomain) {
      module$$3 = d$$1._loadedModules[moduleName$$1];
      if(!module$$3) {
        throw new Error("symbol '" + moduleName$$1 + "' is not defined after loading '" + relpath$$1 + "'");
      }
    }
    return module$$3
  };
  midojo.provide = function(resourceName) {
    resourceName = resourceName + "";
    return d$$1._loadedModules[resourceName] = d$$1.getObject(resourceName, true)
  };
  midojo.platformRequire = function(modMap) {
    var common = modMap.common || [];
    var result = common.concat(modMap[d$$1._name] || modMap["default"] || []);
    var x$$1 = 0;
    for(;x$$1 < result.length;x$$1++) {
      var curr = result[x$$1];
      if(curr.constructor == Array) {
        d$$1._loadModule.apply(d$$1, curr)
      }else {
        d$$1._loadModule(curr)
      }
    }
  };
  midojo.requireIf = function(condition, resourceName$$1) {
    if(condition === true) {
      var args = [];
      var i$$6 = 1;
      for(;i$$6 < arguments.length;i$$6++) {
        args.push(arguments[i$$6])
      }
      d$$1.require.apply(d$$1, args)
    }
  };
  midojo.requireAfterIf = d$$1.requireIf;
  midojo.registerModulePath = function(module$$4, prefix) {
    d$$1._modulePrefixes[module$$4] = {name:module$$4, value:prefix}
  };
  midojo.requireLocalization = function(moduleName$$2, bundleName, locale, availableFlatLocales) {
    d$$1.require("midojo.i18n");
    d$$1.i18n._requireLocalization.apply(d$$1.hostenv, arguments)
  };
  var ore = new RegExp("^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?$");
  var ire = new RegExp("^((([^\\[:]+):)?([^@]+)@)?(\\[([^\\]]+)\\]|([^\\[:]*))(:([0-9]+))?$");
  midojo._Url = function() {
    var n = null;
    var _a = arguments;
    var uri$$3 = [_a[0]];
    var i$$7 = 1;
    for(;i$$7 < _a.length;i$$7++) {
      if(!_a[i$$7]) {
        continue
      }
      var relobj = new d$$1._Url(_a[i$$7] + "");
      var uriobj = new d$$1._Url(uri$$3[0] + "");
      if(relobj.path == "" && !relobj.scheme && !relobj.authority && !relobj.query) {
        if(relobj.fragment != n) {
          uriobj.fragment = relobj.fragment
        }
        relobj = uriobj
      }else {
        if(!relobj.scheme) {
          relobj.scheme = uriobj.scheme;
          if(!relobj.authority) {
            relobj.authority = uriobj.authority;
            if(relobj.path.charAt(0) != "/") {
              var path = uriobj.path.substring(0, uriobj.path.lastIndexOf("/") + 1) + relobj.path;
              var segs = path.split("/");
              var j = 0;
              for(;j < segs.length;j++) {
                if(segs[j] == ".") {
                  if(j == segs.length - 1) {
                    segs[j] = ""
                  }else {
                    segs.splice(j, 1);
                    j--
                  }
                }else {
                  if(j > 0 && !(j == 1 && segs[0] == "") && segs[j] == ".." && segs[j - 1] != "..") {
                    if(j == segs.length - 1) {
                      segs.splice(j, 1);
                      segs[j - 1] = ""
                    }else {
                      segs.splice(j - 1, 2);
                      j -= 2
                    }
                  }
                }
              }
              relobj.path = segs.join("/")
            }
          }
        }
      }
      uri$$3 = [];
      if(relobj.scheme) {
        uri$$3.push(relobj.scheme, ":")
      }
      if(relobj.authority) {
        uri$$3.push("//", relobj.authority)
      }
      uri$$3.push(relobj.path);
      if(relobj.query) {
        uri$$3.push("?", relobj.query)
      }
      if(relobj.fragment) {
        uri$$3.push("#", relobj.fragment)
      }
    }
    this.uri = uri$$3.join("");
    var r = this.uri.match(ore);
    this.scheme = r[2] || (r[1] ? "" : n);
    this.authority = r[4] || (r[3] ? "" : n);
    this.path = r[5];
    this.query = r[7] || (r[6] ? "" : n);
    this.fragment = r[9] || (r[8] ? "" : n);
    if(this.authority != n) {
      r = this.authority.match(ire);
      this.user = r[3] || n;
      this.password = r[4] || n;
      this.host = r[6] || r[7];
      this.port = r[9] || n
    }
  };
  midojo._Url.prototype.toString = function() {
    return this.uri
  };
  midojo.moduleUrl = function(module$$5, url) {
    var loc = d$$1._getModuleSymbols(module$$5).join("/");
    if(!loc) {
      return null
    }
    if(loc.lastIndexOf("/") != loc.length - 1) {
      loc += "/"
    }
    var colonIndex = loc.indexOf(":");
    if(loc.charAt(0) != "/" && (colonIndex == -1 || colonIndex > loc.indexOf("/"))) {
      loc = d$$1.baseUrl + loc
    }
    return new d$$1._Url(loc, url)
  }
})();
if(typeof window != "undefined") {
  midojo.isBrowser = true;
  midojo._name = "browser";
  (function() {
    var d$$2 = midojo;
    if(document && document.getElementsByTagName) {
      var scripts = document.getElementsByTagName("script");
      var rePkg = /ria\.js(\W|$)/i;
      var i$$8 = 0;
      for(;i$$8 < scripts.length;i$$8++) {
        var src = scripts[i$$8].getAttribute("src");
        if(!src) {
          continue
        }
        var m = src.match(rePkg);
        if(m) {
          if(!d$$2.config.baseUrl) {
            d$$2.config.baseUrl = src.substring(0, m.index)
          }
          var cfg = scripts[i$$8].getAttribute("djConfig");
          if(cfg) {
            var cfgo = eval("({ " + cfg + " })");
            var x$$2;
            for(x$$2 in cfgo) {
              midojo.config[x$$2] = cfgo[x$$2]
            }
          }
          break
        }
      }
    }
    d$$2.baseUrl = d$$2.config.baseUrl;
    var n$$1 = navigator;
    var dua = n$$1.userAgent;
    var dav = n$$1.appVersion;
    var tv = parseFloat(dav);
    if(dua.indexOf("Opera") >= 0) {
      d$$2.isOpera = tv
    }
    if(dua.indexOf("AdobeAIR") >= 0) {
      d$$2.isAIR = 1
    }
    d$$2.isKhtml = dav.indexOf("Konqueror") >= 0 ? tv : 0;
    d$$2.isWebKit = parseFloat(dua.split("WebKit/")[1]) || undefined;
    d$$2.isChrome = parseFloat(dua.split("Chrome/")[1]) || undefined;
    d$$2.isMac = dav.indexOf("Macintosh") >= 0;
    var index = Math.max(dav.indexOf("WebKit"), dav.indexOf("Safari"), 0);
    if(index && !midojo.isChrome) {
      d$$2.isSafari = parseFloat(dav.split("Version/")[1]);
      if(!d$$2.isSafari || parseFloat(dav.substr(index + 7)) <= 419.3) {
        d$$2.isSafari = 2
      }
    }
    if(dua.indexOf("Gecko") >= 0 && !d$$2.isKhtml && !d$$2.isWebKit) {
      d$$2.isMozilla = d$$2.isMoz = tv
    }
    if(d$$2.isMoz) {
      d$$2.isFF = parseFloat(dua.split("Firefox/")[1] || dua.split("Minefield/")[1]) || undefined
    }
    if(document.all && !d$$2.isOpera) {
      d$$2.isIE = parseFloat(dav.split("MSIE ")[1]) || undefined;
      var mode = document.documentMode;
      if(mode && mode != 5 && Math.floor(d$$2.isIE) != mode) {
        d$$2.isIE = mode
      }
    }
    if(midojo.isIE && window.location.protocol === "file:") {
      midojo.config.ieForceActiveXXhr = true
    }
    d$$2.isQuirks = document.compatMode == "BackCompat";
    d$$2.locale = midojo.config.locale || (d$$2.isIE ? n$$1.userLanguage : n$$1.language).toLowerCase();
    d$$2._XMLHTTP_PROGIDS = ["Msxml2.XMLHTTP", "Microsoft.XMLHTTP", "Msxml2.XMLHTTP.4.0"];
    d$$2._xhrObj = function() {
      var http;
      var last_e;
      if(!midojo.isIE || !midojo.config.ieForceActiveXXhr) {
        try {
          http = new XMLHttpRequest
        }catch(e$$2) {
        }
      }
      if(!http) {
        var i$$9 = 0;
        for(;i$$9 < 3;++i$$9) {
          var progid = d$$2._XMLHTTP_PROGIDS[i$$9];
          try {
            http = new ActiveXObject(progid)
          }catch(e$$3) {
            last_e = e$$3
          }
          if(http) {
            d$$2._XMLHTTP_PROGIDS = [progid];
            break
          }
        }
      }
      if(!http) {
        throw new Error("XMLHTTP not available: " + last_e);
      }
      return http
    };
    d$$2._isDocumentOk = function(http$$1) {
      var stat = http$$1.status || 0;
      var lp = location.protocol;
      return stat >= 200 && stat < 300 || stat == 304 || stat == 1223 || !stat && (lp == "file:" || lp == "chrome:" || lp == "app:")
    };
    var owloc = window.location + "";
    var base = document.getElementsByTagName("base");
    var hasBase = base && base.length > 0;
    d$$2._getText = function(uri$$4, fail_ok) {
      var http$$2 = d$$2._xhrObj();
      if(!hasBase && midojo._Url) {
        uri$$4 = (new midojo._Url(owloc, uri$$4)).toString()
      }
      if(d$$2.config.cacheBust) {
        uri$$4 += "";
        uri$$4 += (uri$$4.indexOf("?") == -1 ? "?" : "&") + String(d$$2.config.cacheBust).replace(/\W+/g, "")
      }
      http$$2.open("GET", uri$$4, false);
      try {
        http$$2.send(null);
        if(!d$$2._isDocumentOk(http$$2)) {
          var err = Error("Unable to load " + uri$$4 + " status:" + http$$2.status);
          err.status = http$$2.status;
          err.responseText = http$$2.responseText;
          throw err;
        }
      }catch(e$$4) {
        if(fail_ok) {
          return null
        }
        throw e$$4;
      }
      return http$$2.responseText
    };
    var _w = window;
    var _handleNodeEvent = function(evtName, fp) {
      var _a$$1 = _w.attachEvent || _w.addEventListener;
      evtName = _w.attachEvent ? evtName : evtName.substring(2);
      _a$$1(evtName, function() {
        fp.apply(_w, arguments)
      }, false)
    };
    d$$2._windowUnloaders = [];
    d$$2.windowUnloaded = function() {
      var mll$$2 = d$$2._windowUnloaders;
      for(;mll$$2.length;) {
        mll$$2.pop()()
      }
    };
    var _onWindowUnloadAttached = 0;
    d$$2.addOnWindowUnload = function(obj$$6, functionName$$1) {
      d$$2._onto(d$$2._windowUnloaders, obj$$6, functionName$$1);
      if(!_onWindowUnloadAttached) {
        _onWindowUnloadAttached = 1;
        _handleNodeEvent("onunload", d$$2.windowUnloaded)
      }
    };
    var _onUnloadAttached = 0;
    d$$2.addOnUnload = function(obj$$7, functionName$$2) {
      d$$2._onto(d$$2._unloaders, obj$$7, functionName$$2);
      if(!_onUnloadAttached) {
        _onUnloadAttached = 1;
        _handleNodeEvent("onbeforeunload", midojo.unloaded)
      }
    }
  })();
  midojo._initFired = false;
  midojo._loadInit = function(e$$5) {
    if(!midojo._initFired) {
      midojo._initFired = true;
      if(!midojo.config.afterOnLoad && window.detachEvent) {
        window.detachEvent("onload", midojo._loadInit)
      }
      if(midojo._inFlightCount == 0) {
        midojo._modulesLoaded()
      }
    }
  };
  if(!midojo.config.afterOnLoad) {
    if(document.addEventListener) {
      document.addEventListener("DOMContentLoaded", midojo._loadInit, false);
      window.addEventListener("load", midojo._loadInit, false)
    }else {
      if(window.attachEvent) {
        window.attachEvent("onload", midojo._loadInit)
      }
    }
  }
  if(midojo.isIE) {
    if(!midojo.config.afterOnLoad && !midojo.config.skipIeDomLoaded) {
      document.write("<scr" + 'ipt defer src="//:" ' + "onreadystatechange=\"if(this.readyState=='complete'){" + midojo._scopeName + '._loadInit();}">' + "</scr" + "ipt>")
    }
    try {
      document.namespaces.add("v", "urn:schemas-microsoft-com:vml");
      var vmlElems = ["*", "group", "roundrect", "oval", "shape", "rect", "imagedata"];
      var i = 0;
      var l = 1;
      var s = document.createStyleSheet();
      if(midojo.isIE >= 8) {
        i = 1;
        l = vmlElems.length
      }
      for(;i < l;++i) {
        s.addRule("v\\:" + vmlElems[i], "behavior:url(#default#VML); display:inline-block")
      }
    }catch(e$$6) {
    }
  }
}
(function() {
  var mp$$2 = midojo.config["modulePaths"];
  if(mp$$2) {
    var param;
    for(param in mp$$2) {
      midojo.registerModulePath(param, mp$$2[param])
    }
  }
})();
if(midojo.config.isDebug) {
  midojo.provide("midojo._firebug.firebug");
  midojo.deprecated = function(behaviour, extra, removal) {
    var message = "DEPRECATED: " + behaviour;
    if(extra) {
      message += " " + extra
    }
    if(removal) {
      message += " -- will be removed in version: " + removal
    }
    console.warn(message)
  };
  midojo.experimental = function(moduleName, extra) {
    var message = "EXPERIMENTAL: " + moduleName + " -- APIs subject to change without notice.";
    if(extra) {
      message += " " + extra
    }
    console.warn(message)
  };
  (function() {
    var isNewIE = /Trident/.test(window.navigator.userAgent);
    if(isNewIE) {
      var calls = ["log", "info", "debug", "warn", "error"];
      for(var i = 0;i < calls.length;i++) {
        var m = calls[i];
        var n = "_" + calls[i];
        console[n] = console[m];
        console[m] = function() {
          var type = n;
          return function() {
            console[type](Array.prototype.slice.call(arguments).join(" "))
          }
        }()
      }
      try {
        console.clear()
      }catch(e) {
      }
    }
    if(!midojo.isFF && (!midojo.isChrome || midojo.isChrome < 3) && (!midojo.isSafari || midojo.isSafari < 4) && !isNewIE && !window.firebug && typeof console != "undefined" && !console.firebug && !midojo.config.useCustomLogger && !midojo.isAIR) {
      try {
        if(window != window.parent) {
          if(window.parent["console"]) {
            window.console = window.parent.console
          }
          return
        }
      }catch(e) {
      }
      var _firebugDoc = document;
      var _firebugWin = window;
      var __consoleAnchorId__ = 0;
      var consoleFrame = null;
      var consoleBody = null;
      var consoleObjectInspector = null;
      var fireBugTabs = null;
      var commandLine = null;
      var consoleToolbar = null;
      var frameVisible = false;
      var messageQueue = [];
      var groupStack = [];
      var timeMap = {};
      var countMap = {};
      var consoleDomInspector = null;
      var _inspectionMoveConnection;
      var _inspectionClickConnection;
      var _inspectionEnabled = false;
      var _inspectionTimer = null;
      var _inspectTempNode = document.createElement("div");
      var _inspectCurrentNode;
      var _restoreBorderStyle;
      window.console = {_connects:[], log:function() {
        logFormatted(arguments, "")
      }, debug:function() {
        logFormatted(arguments, "debug")
      }, info:function() {
        logFormatted(arguments, "info")
      }, warn:function() {
        logFormatted(arguments, "warning")
      }, error:function() {
        logFormatted(arguments, "error")
      }, assert:function(truth, message) {
        if(!truth) {
          var args = [];
          for(var i = 1;i < arguments.length;++i) {
            args.push(arguments[i])
          }
          logFormatted(args.length ? args : ["Assertion Failure"], "error");
          throw message ? message : "Assertion Failure";
        }
      }, dir:function(obj) {
        var str = printObject(obj);
        str = str.replace(/\n/g, "<br />");
        str = str.replace(/\t/g, "&nbsp;&nbsp;&nbsp;&nbsp;");
        logRow([str], "dir")
      }, dirxml:function(node) {
        var html = [];
        appendNode(node, html);
        logRow(html, "dirxml")
      }, group:function() {
        logRow(arguments, "group", pushGroup)
      }, groupEnd:function() {
        logRow(arguments, "", popGroup)
      }, time:function(name) {
        timeMap[name] = (new Date).getTime()
      }, timeEnd:function(name) {
        if(name in timeMap) {
          var delta = (new Date).getTime() - timeMap[name];
          logFormatted([name + ":", delta + "ms"]);
          delete timeMap[name]
        }
      }, count:function(name) {
        if(!countMap[name]) {
          countMap[name] = 0
        }
        countMap[name]++;
        logFormatted([name + ": " + countMap[name]])
      }, trace:function(_value) {
        var stackAmt = _value || 3;
        var f = console.trace.caller;
        console.log(">>> console.trace(stack)");
        for(var i = 0;i < stackAmt;i++) {
          var func = f.toString();
          var args = [];
          for(var a = 0;a < f.arguments.length;a++) {
            args.push(f.arguments[a])
          }
          if(f.arguments.length) {
            console.dir({"function":func, "arguments":args})
          }else {
            console.dir({"function":func})
          }
          f = f.caller
        }
      }, profile:function() {
        this.warn(["profile() not supported."])
      }, profileEnd:function() {
      }, clear:function() {
        if(consoleBody) {
          while(consoleBody.childNodes.length) {
            midojo.destroy(consoleBody.firstChild)
          }
        }
        midojo.forEach(this._connects, midojo.disconnect)
      }, open:function() {
        toggleConsole(true)
      }, close:function() {
        if(frameVisible) {
          toggleConsole()
        }
      }, _restoreBorder:function() {
        if(_inspectCurrentNode) {
          _inspectCurrentNode.style.border = _restoreBorderStyle
        }
      }, openDomInspector:function() {
        _inspectionEnabled = true;
        consoleBody.style.display = "none";
        consoleDomInspector.style.display = "block";
        consoleObjectInspector.style.display = "none";
        document.body.style.cursor = "pointer";
        _inspectionMoveConnection = midojo.connect(document, "mousemove", function(evt) {
          if(!_inspectionEnabled) {
            return
          }
          if(!_inspectionTimer) {
            _inspectionTimer = setTimeout(function() {
              _inspectionTimer = null
            }, 50)
          }else {
            return
          }
          var node = evt.target;
          if(node && _inspectCurrentNode !== node) {
            var parent = true;
            console._restoreBorder();
            var html = [];
            appendNode(node, html);
            consoleDomInspector.innerHTML = html.join("");
            _inspectCurrentNode = node;
            _restoreBorderStyle = _inspectCurrentNode.style.border;
            _inspectCurrentNode.style.border = "#0000FF 1px solid"
          }
        });
        setTimeout(function() {
          _inspectionClickConnection = midojo.connect(document, "click", function(evt) {
            document.body.style.cursor = "";
            _inspectionEnabled = !_inspectionEnabled;
            midojo.disconnect(_inspectionClickConnection)
          })
        }, 30)
      }, _closeDomInspector:function() {
        document.body.style.cursor = "";
        midojo.disconnect(_inspectionMoveConnection);
        midojo.disconnect(_inspectionClickConnection);
        _inspectionEnabled = false;
        console._restoreBorder()
      }, openConsole:function() {
        consoleBody.style.display = "block";
        consoleDomInspector.style.display = "none";
        consoleObjectInspector.style.display = "none";
        console._closeDomInspector()
      }, openObjectInspector:function() {
        consoleBody.style.display = "none";
        consoleDomInspector.style.display = "none";
        consoleObjectInspector.style.display = "block";
        console._closeDomInspector()
      }, recss:function() {
        var i, a, s;
        a = document.getElementsByTagName("link");
        for(i = 0;i < a.length;i++) {
          s = a[i];
          if(s.rel.toLowerCase().indexOf("stylesheet") >= 0 && s.href) {
            var h = s.href.replace(/(&|%5C?)forceReload=\d+/, "");
            s.href = h + (h.indexOf("?") >= 0 ? "&" : "?") + "forceReload=" + (new Date).valueOf()
          }
        }
      }};
      function toggleConsole(forceOpen) {
        frameVisible = forceOpen || !frameVisible;
        if(consoleFrame) {
          consoleFrame.style.display = frameVisible ? "block" : "none"
        }
      }
      function focusCommandLine() {
        toggleConsole(true);
        if(commandLine) {
          commandLine.focus()
        }
      }
      function openWin(x, y, w, h) {
        var win = window.open("", "_firebug", "status=0,menubar=0,resizable=1,top=" + y + ",left=" + x + ",width=" + w + ",height=" + h + ",scrollbars=1,addressbar=0");
        if(!win) {
          var msg = "Firebug Lite could not open a pop-up window, most likely because of a blocker.\n" + "Either enable pop-ups for this domain, or change the djConfig to popup=false.";
          alert(msg)
        }
        createResizeHandler(win);
        var newDoc = win.document;
        var HTMLstring = '<html style="height:100%;"><head><title>Firebug Lite</title></head>\n' + '<body bgColor="#ccc" style="height:97%;" onresize="opener.onFirebugResize()">\n' + '<div id="fb"></div>' + "</body></html>";
        newDoc.write(HTMLstring);
        newDoc.close();
        return win
      }
      function createResizeHandler(wn) {
        var d = new Date;
        d.setTime(d.getTime() + 60 * 24 * 60 * 60 * 1E3);
        d = d.toUTCString();
        var dc = wn.document, getViewport;
        if(wn.innerWidth) {
          getViewport = function() {
            return{w:wn.innerWidth, h:wn.innerHeight}
          }
        }else {
          if(dc.documentElement && dc.documentElement.clientWidth) {
            getViewport = function() {
              return{w:dc.documentElement.clientWidth, h:dc.documentElement.clientHeight}
            }
          }else {
            if(dc.body) {
              getViewport = function() {
                return{w:dc.body.clientWidth, h:dc.body.clientHeight}
              }
            }
          }
        }
        window.onFirebugResize = function() {
          layout(getViewport().h);
          clearInterval(wn._firebugWin_resize);
          wn._firebugWin_resize = setTimeout(function() {
            var x = wn.screenLeft, y = wn.screenTop, w = wn.outerWidth || wn.document.body.offsetWidth, h = wn.outerHeight || wn.document.body.offsetHeight;
            document.cookie = "_firebugPosition=" + [x, y, w, h].join(",") + "; expires=" + d + "; path=/"
          }, 5E3)
        }
      }
      function createFrame() {
        if(consoleFrame) {
          return
        }
        if(midojo.config.popup) {
          var containerHeight = "100%";
          var cookieMatch = document.cookie.match(/(?:^|; )_firebugPosition=([^;]*)/);
          var p = cookieMatch ? cookieMatch[1].split(",") : [2, 2, 320, 480];
          _firebugWin = openWin(p[0], p[1], p[2], p[3]);
          _firebugDoc = _firebugWin.document;
          midojo.config.debugContainerId = "fb";
          _firebugWin.console = window.console;
          _firebugWin.midojo = window.midojo
        }else {
          _firebugDoc = document;
          containerHeight = (midojo.config.debugHeight || 300) + "px"
        }
        var styleElement = _firebugDoc.createElement("link");
        styleElement.href = midojo.moduleUrl("midojo._firebug", "firebug.css");
        styleElement.rel = "stylesheet";
        styleElement.type = "text/css";
        var styleParent = _firebugDoc.getElementsByTagName("head");
        if(styleParent) {
          styleParent = styleParent[0]
        }
        if(!styleParent) {
          styleParent = _firebugDoc.getElementsByTagName("html")[0]
        }
        if(midojo.isIE) {
          window.setTimeout(function() {
            styleParent.appendChild(styleElement)
          }, 0)
        }else {
          styleParent.appendChild(styleElement)
        }
        if(midojo.config.debugContainerId) {
          consoleFrame = _firebugDoc.getElementById(midojo.config.debugContainerId)
        }
        if(!consoleFrame) {
          consoleFrame = _firebugDoc.createElement("div");
          _firebugDoc.body.appendChild(consoleFrame)
        }
        consoleFrame.className += " firebug";
        consoleFrame.style.height = containerHeight;
        consoleFrame.style.display = frameVisible ? "block" : "none";
        var buildLink = function(label, title, method, _class) {
          return'<li class="' + _class + '"><a href="javascript:void(0);" onclick="console.' + method + '(); return false;" title="' + title + '">' + label + "</a></li>"
        };
        consoleFrame.innerHTML = '<div id="firebugToolbar">' + '  <ul id="fireBugTabs" class="tabs">' + buildLink("Clear", "Remove All Console Logs", "clear", "") + buildLink("ReCSS", "Refresh CSS without reloading page", "recss", "") + buildLink("Console", "Show Console Logs", "openConsole", "gap") + buildLink("DOM", "Show DOM Inspector", "openDomInspector", "") + buildLink("Object", "Show Object Inspector", "openObjectInspector", "") + (midojo.config.popup ? "" : buildLink("Close", "Close the console", 
        "close", "gap")) + "\t</ul>" + "</div>" + '<input type="text" id="firebugCommandLine" />' + '<div id="firebugLog"></div>' + '<div id="objectLog" style="display:none;">Click on an object in the Log display</div>' + '<div id="domInspect" style="display:none;">Hover over HTML elements in the main page. Click to hold selection.</div>';
        consoleToolbar = _firebugDoc.getElementById("firebugToolbar");
        commandLine = _firebugDoc.getElementById("firebugCommandLine");
        addEvent(commandLine, "keydown", onCommandLineKeyDown);
        addEvent(_firebugDoc, midojo.isIE || midojo.isSafari ? "keydown" : "keypress", onKeyDown);
        consoleBody = _firebugDoc.getElementById("firebugLog");
        consoleObjectInspector = _firebugDoc.getElementById("objectLog");
        consoleDomInspector = _firebugDoc.getElementById("domInspect");
        fireBugTabs = _firebugDoc.getElementById("fireBugTabs");
        layout();
        flush()
      }
      midojo.addOnLoad(createFrame);
      function clearFrame() {
        _firebugDoc = null;
        if(_firebugWin.console) {
          _firebugWin.console.clear()
        }
        _firebugWin = null;
        consoleFrame = null;
        consoleBody = null;
        consoleObjectInspector = null;
        consoleDomInspector = null;
        commandLine = null;
        messageQueue = [];
        groupStack = [];
        timeMap = {}
      }
      function evalCommandLine() {
        var text = commandLine.value;
        commandLine.value = "";
        logRow([">  ", text], "command");
        var value;
        try {
          value = eval(text)
        }catch(e) {
          console.debug(e)
        }
        console.log(value)
      }
      function layout(h) {
        var tHeight = 25;
        var height = h ? h - (tHeight + commandLine.offsetHeight + 25 + h * 0.01) + "px" : consoleFrame.offsetHeight - tHeight - commandLine.offsetHeight + "px";
        consoleBody.style.top = tHeight + "px";
        consoleBody.style.height = height;
        consoleObjectInspector.style.height = height;
        consoleObjectInspector.style.top = tHeight + "px";
        consoleDomInspector.style.height = height;
        consoleDomInspector.style.top = tHeight + "px";
        commandLine.style.bottom = 0;
        midojo.addOnWindowUnload(clearFrame)
      }
      function logRow(message, className, handler) {
        if(consoleBody) {
          writeMessage(message, className, handler)
        }else {
          messageQueue.push([message, className, handler])
        }
      }
      function flush() {
        var queue = messageQueue;
        messageQueue = [];
        for(var i = 0;i < queue.length;++i) {
          writeMessage(queue[i][0], queue[i][1], queue[i][2])
        }
      }
      function writeMessage(message, className, handler) {
        var isScrolledToBottom = consoleBody.scrollTop + consoleBody.offsetHeight >= consoleBody.scrollHeight;
        handler = handler || writeRow;
        handler(message, className);
        if(isScrolledToBottom) {
          consoleBody.scrollTop = consoleBody.scrollHeight - consoleBody.offsetHeight
        }
      }
      function appendRow(row) {
        var container = groupStack.length ? groupStack[groupStack.length - 1] : consoleBody;
        container.appendChild(row)
      }
      function writeRow(message, className) {
        var row = consoleBody.ownerDocument.createElement("div");
        row.className = "logRow" + (className ? " logRow-" + className : "");
        row.innerHTML = message.join("");
        appendRow(row)
      }
      function pushGroup(message, className) {
        logFormatted(message, className);
        var groupRowBox = consoleBody.ownerDocument.createElement("div");
        groupRowBox.className = "logGroupBox";
        appendRow(groupRowBox);
        groupStack.push(groupRowBox)
      }
      function popGroup() {
        groupStack.pop()
      }
      function logFormatted(objects, className) {
        var html = [];
        var format = objects[0];
        var objIndex = 0;
        if(typeof format != "string") {
          format = "";
          objIndex = -1
        }
        var parts = parseFormat(format);
        for(var i = 0;i < parts.length;++i) {
          var part = parts[i];
          if(part && typeof part == "object") {
            part.appender(objects[++objIndex], html)
          }else {
            appendText(part, html)
          }
        }
        var ids = [];
        var obs = [];
        for(i = objIndex + 1;i < objects.length;++i) {
          appendText(" ", html);
          var object = objects[i];
          if(object === undefined || object === null) {
            appendNull(object, html)
          }else {
            if(typeof object == "string") {
              appendText(object, html)
            }else {
              if(object instanceof Date) {
                appendText(object.toString(), html)
              }else {
                if(object.nodeType == 9) {
                  appendText("[ XmlDoc ]", html)
                }else {
                  var id = "_a" + __consoleAnchorId__++;
                  ids.push(id);
                  obs.push(object);
                  var str = '<a id="' + id + '" href="javascript:void(0);">' + getObjectAbbr(object) + "</a>";
                  appendLink(str, html)
                }
              }
            }
          }
        }
        logRow(html, className);
        for(i = 0;i < ids.length;i++) {
          var btn = _firebugDoc.getElementById(ids[i]);
          if(!btn) {
            continue
          }
          btn.obj = obs[i];
          _firebugWin.console._connects.push(midojo.connect(btn, "onclick", function() {
            console.openObjectInspector();
            try {
              printObject(this.obj)
            }catch(e) {
              this.obj = e
            }
            consoleObjectInspector.innerHTML = "<pre>" + printObject(this.obj) + "</pre>"
          }))
        }
      }
      function parseFormat(format) {
        var parts = [];
        var reg = /((^%|[^\\]%)(\d+)?(\.)([a-zA-Z]))|((^%|[^\\]%)([a-zA-Z]))/;
        var appenderMap = {s:appendText, d:appendInteger, i:appendInteger, f:appendFloat};
        for(var m = reg.exec(format);m;m = reg.exec(format)) {
          var type = m[8] ? m[8] : m[5];
          var appender = type in appenderMap ? appenderMap[type] : appendObject;
          var precision = m[3] ? parseInt(m[3]) : m[4] == "." ? -1 : 0;
          parts.push(format.substr(0, m[0][0] == "%" ? m.index : m.index + 1));
          parts.push({appender:appender, precision:precision});
          format = format.substr(m.index + m[0].length)
        }
        parts.push(format);
        return parts
      }
      function escapeHTML(value) {
        function replaceChars(ch) {
          switch(ch) {
            case "<":
              return"&lt;";
            case ">":
              return"&gt;";
            case "&":
              return"&amp;";
            case "'":
              return"&#39;";
            case '"':
              return"&quot;"
          }
          return"?"
        }
        return String(value).replace(/[<>&"']/g, replaceChars)
      }
      function objectToString(object) {
        try {
          return object + ""
        }catch(e) {
          return null
        }
      }
      function appendLink(object, html) {
        html.push(objectToString(object))
      }
      function appendText(object, html) {
        html.push(escapeHTML(objectToString(object)))
      }
      function appendNull(object, html) {
        html.push('<span class="objectBox-null">', escapeHTML(objectToString(object)), "</span>")
      }
      function appendString(object, html) {
        html.push('<span class="objectBox-string">&quot;', escapeHTML(objectToString(object)), "&quot;</span>")
      }
      function appendInteger(object, html) {
        html.push('<span class="objectBox-number">', escapeHTML(objectToString(object)), "</span>")
      }
      function appendFloat(object, html) {
        html.push('<span class="objectBox-number">', escapeHTML(objectToString(object)), "</span>")
      }
      function appendFunction(object, html) {
        html.push('<span class="objectBox-function">', getObjectAbbr(object), "</span>")
      }
      function appendObject(object, html) {
        try {
          if(object === undefined) {
            appendNull("undefined", html)
          }else {
            if(object === null) {
              appendNull("null", html)
            }else {
              if(typeof object == "string") {
                appendString(object, html)
              }else {
                if(typeof object == "number") {
                  appendInteger(object, html)
                }else {
                  if(typeof object == "function") {
                    appendFunction(object, html)
                  }else {
                    if(object.nodeType == 1) {
                      appendSelector(object, html)
                    }else {
                      if(typeof object == "object") {
                        appendObjectFormatted(object, html)
                      }else {
                        appendText(object, html)
                      }
                    }
                  }
                }
              }
            }
          }
        }catch(e) {
        }
      }
      function appendObjectFormatted(object, html) {
        var text = objectToString(object);
        var reObject = /\[object (.*?)\]/;
        var m = reObject.exec(text);
        html.push('<span class="objectBox-object">', m ? m[1] : text, "</span>")
      }
      function appendSelector(object, html) {
        html.push('<span class="objectBox-selector">');
        html.push('<span class="selectorTag">', escapeHTML(object.nodeName.toLowerCase()), "</span>");
        if(object.id) {
          html.push('<span class="selectorId">#', escapeHTML(object.id), "</span>")
        }
        if(object.className) {
          html.push('<span class="selectorClass">.', escapeHTML(object.className), "</span>")
        }
        html.push("</span>")
      }
      function appendNode(node, html) {
        if(node.nodeType == 1) {
          html.push('<div class="objectBox-element">', '&lt;<span class="nodeTag">', node.nodeName.toLowerCase(), "</span>");
          for(var i = 0;i < node.attributes.length;++i) {
            var attr = node.attributes[i];
            if(!attr.specified) {
              continue
            }
            html.push('&nbsp;<span class="nodeName">', attr.nodeName.toLowerCase(), '</span>=&quot;<span class="nodeValue">', escapeHTML(attr.nodeValue), "</span>&quot;")
          }
          if(node.firstChild) {
            html.push('&gt;</div><div class="nodeChildren">');
            for(var child = node.firstChild;child;child = child.nextSibling) {
              appendNode(child, html)
            }
            html.push('</div><div class="objectBox-element">&lt;/<span class="nodeTag">', node.nodeName.toLowerCase(), "&gt;</span></div>")
          }else {
            html.push("/&gt;</div>")
          }
        }else {
          if(node.nodeType == 3) {
            html.push('<div class="nodeText">', escapeHTML(node.nodeValue), "</div>")
          }
        }
      }
      function addEvent(object, name, handler) {
        if(document.all) {
          object.attachEvent("on" + name, handler)
        }else {
          object.addEventListener(name, handler, false)
        }
      }
      function removeEvent(object, name, handler) {
        if(document.all) {
          object.detachEvent("on" + name, handler)
        }else {
          object.removeEventListener(name, handler, false)
        }
      }
      function cancelEvent(event) {
        if(document.all) {
          event.cancelBubble = true
        }else {
          event.stopPropagation()
        }
      }
      function onError(msg, href, lineNo) {
        var lastSlash = href.lastIndexOf("/");
        var fileName = lastSlash == -1 ? href : href.substr(lastSlash + 1);
        var html = ['<span class="errorMessage">', msg, "</span>", '<div class="objectBox-sourceLink">', fileName, " (line ", lineNo, ")</div>"];
        logRow(html, "error")
      }
      var onKeyDownTime = (new Date).getTime();
      function onKeyDown(event) {
        var timestamp = (new Date).getTime();
        if(timestamp > onKeyDownTime + 200) {
          event = midojo.fixEvent(event);
          var keys = midojo.keys;
          var ekc = event.keyCode;
          onKeyDownTime = timestamp;
          if(ekc == keys.F12) {
            toggleConsole()
          }else {
            if((ekc == keys.NUMPAD_ENTER || ekc == 76) && event.shiftKey && (event.metaKey || event.ctrlKey)) {
              focusCommandLine()
            }else {
              return
            }
          }
          cancelEvent(event)
        }
      }
      function onCommandLineKeyDown(e) {
        var dk = midojo.keys;
        if(e.keyCode == 13 && commandLine.value) {
          addToHistory(commandLine.value);
          evalCommandLine()
        }else {
          if(e.keyCode == 27) {
            commandLine.value = ""
          }else {
            if(e.keyCode == dk.UP_ARROW || e.charCode == dk.UP_ARROW) {
              navigateHistory("older")
            }else {
              if(e.keyCode == dk.DOWN_ARROW || e.charCode == dk.DOWN_ARROW) {
                navigateHistory("newer")
              }else {
                if(e.keyCode == dk.HOME || e.charCode == dk.HOME) {
                  historyPosition = 1;
                  navigateHistory("older")
                }else {
                  if(e.keyCode == dk.END || e.charCode == dk.END) {
                    historyPosition = 999999;
                    navigateHistory("newer")
                  }
                }
              }
            }
          }
        }
      }
      var historyPosition = -1;
      var historyCommandLine = null;
      function addToHistory(value) {
        var history = cookie("firebug_history");
        history = history ? midojo.fromJson(history) : [];
        var pos = midojo.indexOf(history, value);
        if(pos != -1) {
          history.splice(pos, 1)
        }
        history.push(value);
        cookie("firebug_history", midojo.toJson(history), 30);
        while(history.length && !cookie("firebug_history")) {
          history.shift();
          cookie("firebug_history", midojo.toJson(history), 30)
        }
        historyCommandLine = null;
        historyPosition = -1
      }
      function navigateHistory(direction) {
        var history = cookie("firebug_history");
        history = history ? midojo.fromJson(history) : [];
        if(!history.length) {
          return
        }
        if(historyCommandLine === null) {
          historyCommandLine = commandLine.value
        }
        if(historyPosition == -1) {
          historyPosition = history.length
        }
        if(direction == "older") {
          --historyPosition;
          if(historyPosition < 0) {
            historyPosition = 0
          }
        }else {
          if(direction == "newer") {
            ++historyPosition;
            if(historyPosition > history.length) {
              historyPosition = history.length
            }
          }
        }
        if(historyPosition == history.length) {
          commandLine.value = historyCommandLine;
          historyCommandLine = null
        }else {
          commandLine.value = history[historyPosition]
        }
      }
      function cookie(name, value) {
        var c = document.cookie;
        if(arguments.length == 1) {
          var matches = c.match(new RegExp("(?:^|; )" + name + "=([^;]*)"));
          return matches ? decodeURIComponent(matches[1]) : undefined
        }else {
          var d = new Date;
          d.setMonth(d.getMonth() + 1);
          document.cookie = name + "=" + encodeURIComponent(value) + (d.toUtcString ? "; expires=" + d.toUTCString() : "")
        }
      }
      function isArray(it) {
        return it && it instanceof Array || typeof it == "array"
      }
      function objectLength(o) {
        var cnt = 0;
        for(var nm in o) {
          cnt++
        }
        return cnt
      }
      function printObject(o, i, txt, used) {
        var ind = " \t";
        txt = txt || "";
        i = i || ind;
        used = used || [];
        var opnCls;
        if(o && o.nodeType == 1) {
          var html = [];
          appendNode(o, html);
          return html.join("")
        }
        var br = ",\n", cnt = 0, length = objectLength(o);
        if(o instanceof Date) {
          return i + o.toString() + br
        }
        looking:for(var nm in o) {
          cnt++;
          if(cnt == length) {
            br = "\n"
          }
          if(o[nm] === window || o[nm] === document) {
            continue
          }else {
            if(o[nm] === null) {
              txt += i + nm + " : NULL" + br
            }else {
              if(o[nm] && o[nm].nodeType) {
                if(o[nm].nodeType == 1) {
                }else {
                  if(o[nm].nodeType == 3) {
                    txt += i + nm + " : [ TextNode " + o[nm].data + " ]" + br
                  }
                }
              }else {
                if(typeof o[nm] == "object" && (o[nm] instanceof String || o[nm] instanceof Number || o[nm] instanceof Boolean)) {
                  txt += i + nm + " : " + o[nm] + "," + br
                }else {
                  if(o[nm] instanceof Date) {
                    txt += i + nm + " : " + o[nm].toString() + br
                  }else {
                    if(typeof o[nm] == "object" && o[nm]) {
                      for(var j = 0, seen;seen = used[j];j++) {
                        if(o[nm] === seen) {
                          txt += i + nm + " : RECURSION" + br;
                          continue looking
                        }
                      }
                      used.push(o[nm]);
                      opnCls = isArray(o[nm]) ? ["[", "]"] : ["{", "}"];
                      txt += i + nm + " : " + opnCls[0] + "\n";
                      txt += printObject(o[nm], i + ind, "", used);
                      txt += i + opnCls[1] + br
                    }else {
                      if(typeof o[nm] == "undefined") {
                        txt += i + nm + " : undefined" + br
                      }else {
                        if(nm == "toString" && typeof o[nm] == "function") {
                          var toString = o[nm]();
                          if(typeof toString == "string" && toString.match(/function ?(.*?)\(/)) {
                            toString = escapeHTML(getObjectAbbr(o[nm]))
                          }
                          txt += i + nm + " : " + toString + br
                        }else {
                          txt += i + nm + " : " + escapeHTML(getObjectAbbr(o[nm])) + br
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        return txt
      }
      function getObjectAbbr(obj) {
        var isError = obj instanceof Error;
        if(obj.nodeType == 1) {
          return escapeHTML("< " + obj.tagName.toLowerCase() + ' id="' + obj.id + '" />')
        }
        if(obj.nodeType == 3) {
          return escapeHTML('[TextNode: "' + obj.nodeValue + '"]')
        }
        var nm = obj && (obj.id || obj.name || obj.ObjectID || obj.widgetId);
        if(!isError && nm) {
          return"{" + nm + "}"
        }
        var obCnt = 2;
        var arCnt = 4;
        var cnt = 0;
        if(isError) {
          nm = "[ Error: " + (obj.message || obj.description || obj) + " ]"
        }else {
          if(isArray(obj)) {
            nm = "[" + obj.slice(0, arCnt).join(",");
            if(obj.length > arCnt) {
              nm += " ... (" + obj.length + " items)"
            }
            nm += "]"
          }else {
            if(typeof obj == "function") {
              nm = obj + "";
              var reg = /function\s*([^\(]*)(\([^\)]*\))[^\{]*\{/;
              var m = reg.exec(nm);
              if(m) {
                if(!m[1]) {
                  m[1] = "function"
                }
                nm = m[1] + m[2]
              }else {
                nm = "function()"
              }
            }else {
              if(typeof obj != "object" || typeof obj == "string") {
                nm = obj + ""
              }else {
                nm = "{";
                for(var i in obj) {
                  cnt++;
                  if(cnt > obCnt) {
                    break
                  }
                  nm += i + ":" + escapeHTML(obj[i]) + "  "
                }
                nm += "}"
              }
            }
          }
        }
        return nm
      }
      addEvent(document, midojo.isIE || midojo.isSafari ? "keydown" : "keypress", onKeyDown);
      if(document.documentElement.getAttribute("debug") == "true" || midojo.config.isDebug) {
        toggleConsole(true)
      }
      midojo.addOnWindowUnload(function() {
        removeEvent(document, midojo.isIE || midojo.isSafari ? "keydown" : "keypress", onKeyDown);
        window.onFirebugResize = null;
        window.console = null
      })
    }
  })()
}
if(midojo.config.debugAtAllCosts) {
  midojo.config.useXDomain = true;
  midojo.provide("midojo.i18n");
  midojo.i18n.getLocalization = function(packageName, bundleName, locale) {
    locale = midojo.i18n.normalizeLocale(locale);
    var elements = locale.split("-");
    var module = [packageName, "nls", bundleName].join(".");
    var bundle = midojo._loadedModules[module];
    if(bundle) {
      var localization;
      for(var i = elements.length;i > 0;i--) {
        var loc = elements.slice(0, i).join("_");
        if(bundle[loc]) {
          localization = bundle[loc];
          break
        }
      }
      if(!localization) {
        localization = bundle.ROOT
      }
      if(localization) {
        var clazz = function() {
        };
        clazz.prototype = localization;
        return new clazz
      }
    }
    throw new Error("Bundle not found: " + bundleName + " in " + packageName + " , locale=" + locale);
  };
  midojo.i18n.normalizeLocale = function(locale) {
    var result = locale ? locale.toLowerCase() : midojo.locale;
    if(result == "root") {
      result = "ROOT"
    }
    return result
  };
  midojo.i18n._requireLocalization = function(moduleName, bundleName, locale, availableFlatLocales) {
    var targetLocale = midojo.i18n.normalizeLocale(locale);
    var bundlePackage = [moduleName, "nls", bundleName].join(".");
    var bestLocale = "";
    if(availableFlatLocales) {
      var flatLocales = availableFlatLocales.split(",");
      for(var i = 0;i < flatLocales.length;i++) {
        if(targetLocale["indexOf"](flatLocales[i]) == 0) {
          if(flatLocales[i].length > bestLocale.length) {
            bestLocale = flatLocales[i]
          }
        }
      }
      if(!bestLocale) {
        bestLocale = "ROOT"
      }
    }
    var tempLocale = availableFlatLocales ? bestLocale : targetLocale;
    var bundle = midojo._loadedModules[bundlePackage];
    var localizedBundle = null;
    if(bundle) {
      if(midojo.config.localizationComplete && bundle._built) {
        return
      }
      var jsLoc = tempLocale.replace(/-/g, "_");
      var translationPackage = bundlePackage + "." + jsLoc;
      localizedBundle = midojo._loadedModules[translationPackage]
    }
    if(!localizedBundle) {
      bundle = midojo["provide"](bundlePackage);
      var syms = midojo._getModuleSymbols(moduleName);
      var modpath = syms.concat("nls").join("/");
      var parent;
      midojo.i18n._searchLocalePath(tempLocale, availableFlatLocales, function(loc) {
        var jsLoc = loc.replace(/-/g, "_");
        var translationPackage = bundlePackage + "." + jsLoc;
        var loaded = false;
        if(!midojo._loadedModules[translationPackage]) {
          midojo["provide"](translationPackage);
          var module = [modpath];
          if(loc != "ROOT") {
            module.push(loc)
          }
          module.push(bundleName);
          var filespec = module.join("/") + ".js";
          loaded = midojo._loadPath(filespec, null, function(hash) {
            var clazz = function() {
            };
            clazz.prototype = parent;
            bundle[jsLoc] = new clazz;
            for(var j in hash) {
              bundle[jsLoc][j] = hash[j]
            }
          })
        }else {
          loaded = true
        }
        if(loaded && bundle[jsLoc]) {
          parent = bundle[jsLoc]
        }else {
          bundle[jsLoc] = parent
        }
        if(availableFlatLocales) {
          return true
        }
      })
    }
    if(availableFlatLocales && targetLocale != bestLocale) {
      bundle[targetLocale.replace(/-/g, "_")] = bundle[bestLocale.replace(/-/g, "_")]
    }
  };
  (function() {
    var extra = midojo.config.extraLocale;
    if(extra) {
      if(!extra instanceof Array) {
        extra = [extra]
      }
      var req = midojo.i18n._requireLocalization;
      midojo.i18n._requireLocalization = function(m, b, locale, availableFlatLocales) {
        req(m, b, locale, availableFlatLocales);
        if(locale) {
          return
        }
        for(var i = 0;i < extra.length;i++) {
          req(m, b, extra[i], availableFlatLocales)
        }
      }
    }
  })();
  midojo.i18n._searchLocalePath = function(locale, down, searchFunc) {
    locale = midojo.i18n.normalizeLocale(locale);
    var elements = locale.split("-");
    var searchlist = [];
    for(var i = elements.length;i > 0;i--) {
      searchlist.push(elements.slice(0, i).join("-"))
    }
    searchlist.push(false);
    if(down) {
      searchlist.reverse()
    }
    for(var j = searchlist.length - 1;j >= 0;j--) {
      var loc = searchlist[j] || "ROOT";
      var stop = searchFunc(loc);
      if(stop) {
        break
      }
    }
  };
  midojo.i18n._preloadLocalizations = function(bundlePrefix, localesGenerated) {
    function preload(locale) {
      locale = midojo.i18n.normalizeLocale(locale);
      midojo.i18n._searchLocalePath(locale, true, function(loc) {
        for(var i = 0;i < localesGenerated.length;i++) {
          if(localesGenerated[i] == loc) {
            midojo["require"](bundlePrefix + "_" + loc);
            return true
          }
        }
        return false
      })
    }
    preload();
    var extra = midojo.config.extraLocale || [];
    for(var i = 0;i < extra.length;i++) {
      preload(extra[i])
    }
  }
}
;midojo.provide("midojo._base");
midojo.provide("midojo._base.lang");
(function() {
  var d = midojo, opts = Object.prototype.toString;
  midojo.isString = function(it) {
    return typeof it == "string" || it instanceof String
  };
  midojo.isArray = function(it) {
    return it && (it instanceof Array || typeof it == "array")
  };
  midojo.isFunction = function(it) {
    return opts.call(it) === "[object Function]"
  };
  midojo.isObject = function(it) {
    return it !== undefined && (it === null || typeof it == "object" || d.isArray(it) || d.isFunction(it))
  };
  midojo.isArrayLike = function(it) {
    return it && it !== undefined && !d.isString(it) && !d.isFunction(it) && !(it.tagName && it.tagName.toLowerCase() == "form") && (d.isArray(it) || isFinite(it.length))
  };
  midojo.isAlien = function(it) {
    return it && !d.isFunction(it) && /\{\s*\[native code\]\s*\}/.test(String(it))
  };
  midojo.extend = function(constructor, props) {
    for(var i = 1, l = arguments.length;i < l;i++) {
      d._mixin(constructor.prototype, arguments[i])
    }
    return constructor
  };
  midojo._hitchArgs = function(scope, method) {
    var pre = d._toArray(arguments, 2);
    var named = d.isString(method);
    return function() {
      var args = d._toArray(arguments);
      var f = named ? (scope || d.global)[method] : method;
      return f && f.apply(scope || this, pre.concat(args))
    }
  };
  midojo.hitch = function(scope, method) {
    if(arguments.length > 2) {
      return d._hitchArgs.apply(d, arguments)
    }
    if(!method) {
      method = scope;
      scope = null
    }
    if(d.isString(method)) {
      scope = scope || d.global;
      if(!scope[method]) {
        throw['midojo.hitch: scope["', method, '"] is null (scope="', scope, '")'].join("");
      }
      return function() {
        return scope[method].apply(scope, arguments || [])
      }
    }
    return!scope ? method : function() {
      return method.apply(scope, arguments || [])
    }
  };
  midojo.delegate = midojo._delegate = function() {
    function TMP() {
    }
    return function(obj, props) {
      TMP.prototype = obj;
      var tmp = new TMP;
      TMP.prototype = null;
      if(props) {
        d._mixin(tmp, props)
      }
      return tmp
    }
  }();
  var efficient = function(obj, offset, startWith) {
    return(startWith || []).concat(Array.prototype.slice.call(obj, offset || 0))
  };
  var slow = function(obj, offset, startWith) {
    var arr = startWith || [];
    for(var x = offset || 0;x < obj.length;x++) {
      arr.push(obj[x])
    }
    return arr
  };
  midojo._toArray = d.isIE ? function(obj) {
    return(obj.item ? slow : efficient).apply(this, arguments)
  } : efficient;
  midojo.partial = function(method) {
    var arr = [null];
    return d.hitch.apply(d, arr.concat(d._toArray(arguments)))
  };
  var extraNames = d._extraNames, extraLen = extraNames.length, empty = {};
  midojo.clone = function(o) {
    if(!o || typeof o != "object" || d.isFunction(o)) {
      return o
    }
    if(o.nodeType && "cloneNode" in o) {
      return o.cloneNode(true)
    }
    if(o instanceof Date) {
      return new Date(o.getTime())
    }
    var r, i, l, s, name;
    if(d.isArray(o)) {
      r = [];
      for(i = 0, l = o.length;i < l;++i) {
        if(i in o) {
          r.push(d.clone(o[i]))
        }
      }
    }else {
      r = o.constructor ? new o.constructor : {}
    }
    for(name in o) {
      s = o[name];
      if(!(name in r) || r[name] !== s && (!(name in empty) || empty[name] !== s)) {
        r[name] = d.clone(s)
      }
    }
    if(extraLen) {
      for(i = 0;i < extraLen;++i) {
        name = extraNames[i];
        s = o[name];
        if(!(name in r) || r[name] !== s && (!(name in empty) || empty[name] !== s)) {
          r[name] = s
        }
      }
    }
    return r
  };
  midojo.trim = String.prototype.trim ? function(str) {
    return str.trim()
  } : function(str) {
    return str.replace(/^\s\s*/, "").replace(/\s\s*$/, "")
  };
  var _pattern = /\{([^\}]+)\}/g;
  midojo.replace = function(tmpl, map, pattern) {
    return tmpl.replace(pattern || _pattern, d.isFunction(map) ? map : function(_, k) {
      return d.getObject(k, false, map)
    })
  }
})();
midojo.provide("midojo._base.declare");
midojo.provide("midojo._base.array");
(function() {
  var _getParts = function(arr, obj, cb) {
    return[typeof arr == "string" ? arr.split("") : arr, obj || midojo.global, typeof cb == "string" ? new Function("item", "index", "array", cb) : cb]
  };
  var everyOrSome = function(every, arr, callback, thisObject) {
    var _p = _getParts(arr, thisObject, callback);
    arr = _p[0];
    for(var i = 0, l = arr.length;i < l;++i) {
      var result = !!_p[2].call(_p[1], arr[i], i, arr);
      if(every ^ result) {
        return result
      }
    }
    return every
  };
  midojo.mixin(midojo, {indexOf:function(array, value, fromIndex, findLast) {
    var step = 1, end = array.length || 0, i = 0;
    if(findLast) {
      i = end - 1;
      step = end = -1
    }
    if(fromIndex != undefined) {
      i = fromIndex
    }
    if(findLast && i > end || i < end) {
      for(;i != end;i += step) {
        if(array[i] == value) {
          return i
        }
      }
    }
    return-1
  }, lastIndexOf:function(array, value, fromIndex) {
    return midojo.indexOf(array, value, fromIndex, true)
  }, forEach:function(arr, callback, thisObject) {
    if(!arr || !arr.length) {
      return
    }
    var _p = _getParts(arr, thisObject, callback);
    arr = _p[0];
    for(var i = 0, l = arr.length;i < l;++i) {
      _p[2].call(_p[1], arr[i], i, arr)
    }
  }, every:function(arr, callback, thisObject) {
    return everyOrSome(true, arr, callback, thisObject)
  }, some:function(arr, callback, thisObject) {
    return everyOrSome(false, arr, callback, thisObject)
  }, map:function(arr, callback, thisObject) {
    var _p = _getParts(arr, thisObject, callback);
    arr = _p[0];
    var outArr = arguments[3] ? new arguments[3] : [];
    for(var i = 0, l = arr.length;i < l;++i) {
      outArr.push(_p[2].call(_p[1], arr[i], i, arr))
    }
    return outArr
  }, filter:function(arr, callback, thisObject) {
    var _p = _getParts(arr, thisObject, callback);
    arr = _p[0];
    var outArr = [];
    for(var i = 0, l = arr.length;i < l;++i) {
      if(_p[2].call(_p[1], arr[i], i, arr)) {
        outArr.push(arr[i])
      }
    }
    return outArr
  }})
})();
(function() {
  var d = midojo, mix = d._mixin, op = Object.prototype, opts = op.toString, xtor = new Function, counter = 0, cname = "constructor";
  function err(msg) {
    throw new Error("declare: " + msg);
  }
  function c3mro(bases) {
    var result = [], roots = [{cls:0, refs:[]}], nameMap = {}, clsCount = 1, l = bases.length, i = 0, j, lin, base, top, proto, rec, name, refs;
    for(;i < l;++i) {
      base = bases[i];
      if(!base) {
        err("mixin #" + i + " is null")
      }
      lin = base._meta ? base._meta.bases : [base];
      top = 0;
      for(j = lin.length - 1;j >= 0;--j) {
        proto = lin[j].prototype;
        if(!proto.hasOwnProperty("declaredClass")) {
          proto.declaredClass = "uniqName_" + counter++
        }
        name = proto.declaredClass;
        if(!nameMap.hasOwnProperty(name)) {
          nameMap[name] = {count:0, refs:[], cls:lin[j]};
          ++clsCount
        }
        rec = nameMap[name];
        if(top && top !== rec) {
          rec.refs.push(top);
          ++top.count
        }
        top = rec
      }
      ++top.count;
      roots[0].refs.push(top)
    }
    while(roots.length) {
      top = roots.pop();
      result.push(top.cls);
      --clsCount;
      while(refs = top.refs, refs.length == 1) {
        top = refs[0];
        if(!top || --top.count) {
          top = 0;
          break
        }
        result.push(top.cls);
        --clsCount
      }
      if(top) {
        for(i = 0, l = refs.length;i < l;++i) {
          top = refs[i];
          if(!--top.count) {
            roots.push(top)
          }
        }
      }
    }
    if(clsCount) {
      err("can't build consistent linearization")
    }
    base = bases[0];
    result[0] = base ? base._meta && base === result[result.length - base._meta.bases.length] ? base._meta.bases.length : 1 : 0;
    return result
  }
  function inherited(args, a, f) {
    var name, chains, bases, caller, meta, base, proto, opf, pos, cache = this._inherited = this._inherited || {};
    if(typeof args == "string") {
      name = args;
      args = a;
      a = f
    }
    f = 0;
    caller = args.callee;
    name = name || caller.nom;
    if(!name) {
      err("can't deduce a name to call inherited()")
    }
    meta = this.constructor._meta;
    bases = meta.bases;
    pos = cache.p;
    if(name != cname) {
      if(cache.c !== caller) {
        pos = 0;
        base = bases[0];
        meta = base._meta;
        if(meta.hidden[name] !== caller) {
          chains = meta.chains;
          if(chains && typeof chains[name] == "string") {
            err("calling chained method with inherited: " + name)
          }
          do {
            meta = base._meta;
            proto = base.prototype;
            if(meta && (proto[name] === caller && proto.hasOwnProperty(name) || meta.hidden[name] === caller)) {
              break
            }
          }while(base = bases[++pos]);
          pos = base ? pos : -1
        }
      }
      base = bases[++pos];
      if(base) {
        proto = base.prototype;
        if(base._meta && proto.hasOwnProperty(name)) {
          f = proto[name]
        }else {
          opf = op[name];
          do {
            proto = base.prototype;
            f = proto[name];
            if(f && (base._meta ? proto.hasOwnProperty(name) : f !== opf)) {
              break
            }
          }while(base = bases[++pos])
        }
      }
      f = base && f || op[name]
    }else {
      if(cache.c !== caller) {
        pos = 0;
        meta = bases[0]._meta;
        if(meta && meta.ctor !== caller) {
          chains = meta.chains;
          if(!chains || chains.constructor !== "manual") {
            err("calling chained constructor with inherited")
          }
          while(base = bases[++pos]) {
            meta = base._meta;
            if(meta && meta.ctor === caller) {
              break
            }
          }
          pos = base ? pos : -1
        }
      }
      while(base = bases[++pos]) {
        meta = base._meta;
        f = meta ? meta.ctor : base;
        if(f) {
          break
        }
      }
      f = base && f
    }
    cache.c = f;
    cache.p = pos;
    if(f) {
      return a === true ? f : f.apply(this, a || args)
    }
  }
  function getInherited(name, args) {
    if(typeof name == "string") {
      return this.inherited(name, args, true)
    }
    return this.inherited(name, true)
  }
  function isInstanceOf(cls) {
    var bases = this.constructor._meta.bases;
    for(var i = 0, l = bases.length;i < l;++i) {
      if(bases[i] === cls) {
        return true
      }
    }
    return this instanceof cls
  }
  function safeMixin(target, source) {
    var name, t, i = 0, l = d._extraNames.length;
    for(name in source) {
      t = source[name];
      if((t !== op[name] || !(name in op)) && name != cname) {
        if(opts.call(t) == "[object Function]") {
          t.nom = name
        }
        target[name] = t
      }
    }
    for(;i < l;++i) {
      name = d._extraNames[i];
      t = source[name];
      if((t !== op[name] || !(name in op)) && name != cname) {
        if(opts.call(t) == "[object Function]") {
          t.nom = name
        }
        target[name] = t
      }
    }
    return target
  }
  function extend(source) {
    safeMixin(this.prototype, source);
    return this
  }
  function chainedConstructor(bases, ctorSpecial) {
    return function() {
      var a = arguments, args = a, a0 = a[0], f, i, m, l = bases.length, preArgs;
      if(ctorSpecial && (a0 && a0.preamble || this.preamble)) {
        preArgs = new Array(bases.length);
        preArgs[0] = a;
        for(i = 0;;) {
          a0 = a[0];
          if(a0) {
            f = a0.preamble;
            if(f) {
              a = f.apply(this, a) || a
            }
          }
          f = bases[i].prototype;
          f = f.hasOwnProperty("preamble") && f.preamble;
          if(f) {
            a = f.apply(this, a) || a
          }
          if(++i == l) {
            break
          }
          preArgs[i] = a
        }
      }
      for(i = l - 1;i >= 0;--i) {
        f = bases[i];
        m = f._meta;
        f = m ? m.ctor : f;
        if(f) {
          f.apply(this, preArgs ? preArgs[i] : a)
        }
      }
      f = this.postscript;
      if(f) {
        f.apply(this, args)
      }
    }
  }
  function singleConstructor(ctor, ctorSpecial) {
    return function() {
      var a = arguments, t = a, a0 = a[0], f;
      if(ctorSpecial) {
        if(a0) {
          f = a0.preamble;
          if(f) {
            t = f.apply(this, t) || t
          }
        }
        f = this.preamble;
        if(f) {
          f.apply(this, t)
        }
      }
      if(ctor) {
        ctor.apply(this, a)
      }
      f = this.postscript;
      if(f) {
        f.apply(this, a)
      }
    }
  }
  function simpleConstructor(bases) {
    return function() {
      var a = arguments, i = 0, f;
      for(;f = bases[i];++i) {
        m = f._meta;
        f = m ? m.ctor : f;
        if(f) {
          f.apply(this, a);
          break
        }
      }
      f = this.postscript;
      if(f) {
        f.apply(this, a)
      }
    }
  }
  function chain(name, bases, reversed) {
    return function() {
      var b, m, f, i = 0, step = 1;
      if(reversed) {
        i = bases.length - 1;
        step = -1
      }
      for(;b = bases[i];i += step) {
        m = b._meta;
        f = (m ? m.hidden : b.prototype)[name];
        if(f) {
          f.apply(this, arguments)
        }
      }
    }
  }
  d.declare = function(className, superclass, props) {
    var proto, i, t, ctor, name, bases, chains, mixins = 1, parents = superclass;
    if(typeof className != "string") {
      props = superclass;
      superclass = className;
      className = ""
    }
    props = props || {};
    if(opts.call(superclass) == "[object Array]") {
      bases = c3mro(superclass);
      t = bases[0];
      mixins = bases.length - t;
      superclass = bases[mixins]
    }else {
      bases = [0];
      if(superclass) {
        t = superclass._meta;
        bases = bases.concat(t ? t.bases : superclass)
      }
    }
    if(superclass) {
      for(i = mixins - 1;;--i) {
        xtor.prototype = superclass.prototype;
        proto = new xtor;
        if(!i) {
          break
        }
        t = bases[i];
        mix(proto, t._meta ? t._meta.hidden : t.prototype);
        ctor = new Function;
        ctor.superclass = superclass;
        ctor.prototype = proto;
        superclass = proto.constructor = ctor
      }
    }else {
      proto = {}
    }
    safeMixin(proto, props);
    t = props.constructor;
    if(t !== op.constructor) {
      t.nom = cname;
      proto.constructor = t
    }
    xtor.prototype = 0;
    for(i = mixins - 1;i;--i) {
      t = bases[i]._meta;
      if(t && t.chains) {
        chains = mix(chains || {}, t.chains)
      }
    }
    if(proto["-chains-"]) {
      chains = mix(chains || {}, proto["-chains-"])
    }
    t = !chains || !chains.hasOwnProperty(cname);
    bases[0] = ctor = chains && chains.constructor === "manual" ? simpleConstructor(bases) : bases.length == 1 ? singleConstructor(props.constructor, t) : chainedConstructor(bases, t);
    ctor._meta = {bases:bases, hidden:props, chains:chains, parents:parents, ctor:props.constructor};
    ctor.superclass = superclass && superclass.prototype;
    ctor.extend = extend;
    ctor.prototype = proto;
    proto.constructor = ctor;
    proto.getInherited = getInherited;
    proto.inherited = inherited;
    proto.isInstanceOf = isInstanceOf;
    if(className) {
      proto.declaredClass = className;
      d.setObject(className, ctor)
    }
    if(chains) {
      for(name in chains) {
        if(proto[name] && typeof chains[name] == "string" && name != cname) {
          t = proto[name] = chain(name, bases, chains[name] === "after");
          t.nom = name
        }
      }
    }
    return ctor
  };
  d.safeMixin = safeMixin
})();
midojo.provide("midojo._base.connect");
midojo._listener = {getDispatcher:function() {
  return function() {
    var ap = Array.prototype, c = arguments.callee, ls = c._listeners, t = c.target;
    var r = t && t.apply(this, arguments);
    var lls;
    if(!midojo.isRhino) {
      lls = [].concat(ls)
    }else {
      lls = [];
      for(var i in ls) {
        lls[i] = ls[i]
      }
    }
    for(var i in lls) {
      if(!(i in ap)) {
        lls[i].apply(this, arguments)
      }
    }
    return r
  }
}, add:function(source, method, listener) {
  source = source || midojo.global;
  var f = source[method];
  if(!f || !f._listeners) {
    var d = midojo._listener.getDispatcher();
    d.target = f;
    d._listeners = [];
    f = source[method] = d
  }
  return f._listeners.push(listener)
}, remove:function(source, method, handle) {
  var f = (source || midojo.global)[method];
  if(f && f._listeners && handle--) {
    delete f._listeners[handle]
  }
}};
midojo.connect = function(obj, event, context, method, dontFix) {
  var a = arguments, args = [], i = 0;
  args.push(midojo.isString(a[0]) ? null : a[i++], a[i++]);
  var a1 = a[i + 1];
  args.push(midojo.isString(a1) || midojo.isFunction(a1) ? a[i++] : null, a[i++]);
  for(var l = a.length;i < l;i++) {
    args.push(a[i])
  }
  return midojo._connect.apply(this, args)
};
midojo._connect = function(obj, event, context, method) {
  var l = midojo._listener, h = l.add(obj, event, midojo.hitch(context, method));
  return[obj, event, h, l]
};
midojo.disconnect = function(handle) {
  if(handle && handle[0] !== undefined) {
    midojo._disconnect.apply(this, handle);
    delete handle[0]
  }
};
midojo._disconnect = function(obj, event, handle, listener) {
  listener.remove(obj, event, handle)
};
midojo._topics = {};
midojo.subscribe = function(topic, context, method) {
  return[topic, midojo._listener.add(midojo._topics, topic, midojo.hitch(context, method))]
};
midojo.unsubscribe = function(handle) {
  if(handle) {
    midojo._listener.remove(midojo._topics, handle[0], handle[1])
  }
};
midojo.publish = function(topic, args) {
  var f = midojo._topics[topic];
  if(f) {
    f.apply(this, args || [])
  }
};
midojo.connectPublisher = function(topic, obj, event) {
  var pf = function() {
    midojo.publish(topic, arguments)
  };
  return event ? midojo.connect(obj, event, pf) : midojo.connect(obj, pf)
};
midojo.provide("midojo._base.Deferred");
midojo.Deferred = function(canceller) {
  this.chain = [];
  this.id = this._nextId();
  this.fired = -1;
  this.paused = 0;
  this.results = [null, null];
  this.canceller = canceller;
  this.silentlyCancelled = false;
  this.isFiring = false
};
midojo.extend(midojo.Deferred, {_nextId:function() {
  var n = 1;
  return function() {
    return n++
  }
}(), cancel:function() {
  var err;
  if(this.fired == -1) {
    if(this.canceller) {
      err = this.canceller(this)
    }else {
      this.silentlyCancelled = true
    }
    if(this.fired == -1) {
      if(!(err instanceof Error)) {
        var res = err;
        var msg = "Deferred Cancelled";
        if(err && err.toString) {
          msg += ": " + err.toString()
        }
        err = new Error(msg);
        err.dojoType = "cancel";
        err.cancelResult = res
      }
      this.errback(err)
    }
  }else {
    if(this.fired == 0 && this.results[0] instanceof midojo.Deferred) {
      this.results[0].cancel()
    }
  }
}, _resback:function(res) {
  this.fired = res instanceof Error ? 1 : 0;
  this.results[this.fired] = res;
  this._fire()
}, _check:function() {
  if(this.fired != -1) {
    if(!this.silentlyCancelled) {
      throw new Error("already called!");
    }
    this.silentlyCancelled = false;
    return
  }
}, callback:function(res) {
  this._check();
  this._resback(res)
}, errback:function(res) {
  this._check();
  if(!(res instanceof Error)) {
    res = new Error(res)
  }
  this._resback(res)
}, addBoth:function(cb, cbfn) {
  var enclosed = midojo.hitch.apply(midojo, arguments);
  return this.addCallbacks(enclosed, enclosed)
}, addCallback:function(cb, cbfn) {
  return this.addCallbacks(midojo.hitch.apply(midojo, arguments))
}, addErrback:function(cb, cbfn) {
  return this.addCallbacks(null, midojo.hitch.apply(midojo, arguments))
}, addCallbacks:function(cb, eb) {
  this.chain.push([cb, eb]);
  if(this.fired >= 0 && !this.isFiring) {
    this._fire()
  }
  return this
}, _fire:function() {
  this.isFiring = true;
  var chain = this.chain;
  var fired = this.fired;
  var res = this.results[fired];
  var self = this;
  var cb = null;
  while(chain.length > 0 && this.paused == 0) {
    var f = chain.shift()[fired];
    if(!f) {
      continue
    }
    var func = function() {
      var ret = f(res);
      if(typeof ret != "undefined") {
        res = ret
      }
      fired = res instanceof Error ? 1 : 0;
      if(res instanceof midojo.Deferred) {
        cb = function(res) {
          self._resback(res);
          self.paused--;
          if(self.paused == 0 && self.fired >= 0) {
            self._fire()
          }
        };
        this.paused++
      }
    };
    if(midojo.config.debugAtAllCosts) {
      func.call(this)
    }else {
      try {
        func.call(this)
      }catch(err) {
        fired = 1;
        res = err
      }
    }
  }
  this.fired = fired;
  this.results[fired] = res;
  this.isFiring = false;
  if(cb && this.paused) {
    res.addBoth(cb)
  }
}});
midojo.provide("midojo._base.json");
midojo.fromJson = function(json) {
  return eval("(" + json + ")")
};
midojo._escapeString = function(str) {
  return('"' + str.replace(/(["\\])/g, "\\$1") + '"').replace(/[\f]/g, "\\f").replace(/[\b]/g, "\\b").replace(/[\n]/g, "\\n").replace(/[\t]/g, "\\t").replace(/[\r]/g, "\\r")
};
midojo.toJsonIndentStr = "\t";
midojo.toJson = function(it, prettyPrint, _indentStr) {
  if(it === undefined) {
    return"undefined"
  }
  var objtype = typeof it;
  if(objtype == "number" || objtype == "boolean") {
    return it + ""
  }
  if(it === null) {
    return"null"
  }
  if(midojo.isString(it)) {
    return midojo._escapeString(it)
  }
  var recurse = arguments.callee;
  var newObj;
  _indentStr = _indentStr || "";
  var nextIndent = prettyPrint ? _indentStr + midojo.toJsonIndentStr : "";
  var tf = it.__json__ || it.json;
  if(midojo.isFunction(tf)) {
    newObj = tf.call(it);
    if(it !== newObj) {
      return recurse(newObj, prettyPrint, nextIndent)
    }
  }
  if(it.nodeType && it.cloneNode) {
    throw new Error("Can't serialize DOM nodes");
  }
  var sep = prettyPrint ? " " : "";
  var newLine = prettyPrint ? "\n" : "";
  if(midojo.isArray(it)) {
    var res = midojo.map(it, function(obj) {
      var val = recurse(obj, prettyPrint, nextIndent);
      if(typeof val != "string") {
        val = "undefined"
      }
      return newLine + nextIndent + val
    });
    return"[" + res.join("," + sep) + newLine + _indentStr + "]"
  }
  if(objtype == "function") {
    return null
  }
  var output = [], key;
  for(key in it) {
    var keyStr, val;
    if(typeof key == "number") {
      keyStr = '"' + key + '"'
    }else {
      if(typeof key == "string") {
        keyStr = midojo._escapeString(key)
      }else {
        continue
      }
    }
    val = recurse(it[key], prettyPrint, nextIndent);
    if(typeof val != "string") {
      continue
    }
    output.push(newLine + nextIndent + keyStr + ":" + sep + val)
  }
  return"{" + output.join("," + sep) + newLine + _indentStr + "}"
};
midojo.provide("midojo._base.Color");
(function() {
  var d = midojo;
  midojo.Color = function(color) {
    if(color) {
      this.setColor(color)
    }
  };
  midojo.Color.named = {black:[0, 0, 0], silver:[192, 192, 192], gray:[128, 128, 128], white:[255, 255, 255], maroon:[128, 0, 0], red:[255, 0, 0], purple:[128, 0, 128], fuchsia:[255, 0, 255], green:[0, 128, 0], lime:[0, 255, 0], olive:[128, 128, 0], yellow:[255, 255, 0], navy:[0, 0, 128], blue:[0, 0, 255], teal:[0, 128, 128], aqua:[0, 255, 255], transparent:d.config.transparentColor || [255, 255, 255]};
  midojo.extend(midojo.Color, {r:255, g:255, b:255, a:1, _set:function(r, g, b, a) {
    var t = this;
    t.r = r;
    t.g = g;
    t.b = b;
    t.a = a
  }, setColor:function(color) {
    if(d.isString(color)) {
      d.colorFromString(color, this)
    }else {
      if(d.isArray(color)) {
        d.colorFromArray(color, this)
      }else {
        this._set(color.r, color.g, color.b, color.a);
        if(!(color instanceof d.Color)) {
          this.sanitize()
        }
      }
    }
    return this
  }, sanitize:function() {
    return this
  }, toRgb:function() {
    var t = this;
    return[t.r, t.g, t.b]
  }, toRgba:function() {
    var t = this;
    return[t.r, t.g, t.b, t.a]
  }, toHex:function() {
    var arr = d.map(["r", "g", "b"], function(x) {
      var s = this[x].toString(16);
      return s.length < 2 ? "0" + s : s
    }, this);
    return"#" + arr.join("")
  }, toCss:function(includeAlpha) {
    var t = this, rgb = t.r + ", " + t.g + ", " + t.b;
    return(includeAlpha ? "rgba(" + rgb + ", " + t.a : "rgb(" + rgb) + ")"
  }, toString:function() {
    return this.toCss(true)
  }});
  midojo.blendColors = function(start, end, weight, obj) {
    var t = obj || new d.Color;
    d.forEach(["r", "g", "b", "a"], function(x) {
      t[x] = start[x] + (end[x] - start[x]) * weight;
      if(x != "a") {
        t[x] = Math.round(t[x])
      }
    });
    return t.sanitize()
  };
  midojo.colorFromRgb = function(color, obj) {
    var m = color.toLowerCase().match(/^rgba?\(([\s\.,0-9]+)\)/);
    return m && midojo.colorFromArray(m[1].split(/\s*,\s*/), obj)
  };
  midojo.colorFromHex = function(color, obj) {
    var t = obj || new d.Color, bits = color.length == 4 ? 4 : 8, mask = (1 << bits) - 1;
    color = Number("0x" + color.substr(1));
    if(isNaN(color)) {
      return null
    }
    d.forEach(["b", "g", "r"], function(x) {
      var c = color & mask;
      color >>= bits;
      t[x] = bits == 4 ? 17 * c : c
    });
    t.a = 1;
    return t
  };
  midojo.colorFromArray = function(a, obj) {
    var t = obj || new d.Color;
    t._set(Number(a[0]), Number(a[1]), Number(a[2]), Number(a[3]));
    if(isNaN(t.a)) {
      t.a = 1
    }
    return t.sanitize()
  };
  midojo.colorFromString = function(str, obj) {
    var a = d.Color.named[str];
    return a && d.colorFromArray(a, obj) || d.colorFromRgb(str, obj) || d.colorFromHex(str, obj)
  }
})();
midojo.provide("midojo._base.browser");
midojo.provide("midojo._base.window");
midojo.doc = window["document"] || null;
midojo.body = function() {
  return midojo.doc.body || midojo.doc.getElementsByTagName("body")[0]
};
midojo.setContext = function(globalObject, globalDocument) {
  midojo.global = globalObject;
  midojo.doc = globalDocument
};
midojo.withGlobal = function(globalObject, callback, thisObject, cbArguments) {
  var oldGlob = midojo.global;
  try {
    midojo.global = globalObject;
    return midojo.withDoc.call(null, globalObject.document, callback, thisObject, cbArguments)
  }finally {
    midojo.global = oldGlob
  }
};
midojo.withDoc = function(documentObject, callback, thisObject, cbArguments) {
  var oldDoc = midojo.doc, oldLtr = midojo._bodyLtr, oldQ = midojo.isQuirks;
  try {
    midojo.doc = documentObject;
    delete midojo._bodyLtr;
    midojo.isQuirks = midojo.doc.compatMode == "BackCompat";
    if(thisObject && typeof callback == "string") {
      callback = thisObject[callback]
    }
    return callback.apply(thisObject, cbArguments || [])
  }finally {
    midojo.doc = oldDoc;
    delete midojo._bodyLtr;
    if(oldLtr !== undefined) {
      midojo._bodyLtr = oldLtr
    }
    midojo.isQuirks = oldQ
  }
};
midojo.provide("midojo._base.event");
(function() {
  var del = midojo._event_listener = {add:function(node, name, fp) {
    if(!node) {
      return
    }
    name = del._normalizeEventName(name);
    fp = del._fixCallback(name, fp);
    var oname = name;
    if(!midojo.isIE && (name == "mouseenter" || name == "mouseleave")) {
      var ofp = fp;
      name = name == "mouseenter" ? "mouseover" : "mouseout";
      fp = function(e) {
        if(!midojo.isDescendant(e.relatedTarget, node)) {
          return ofp.call(this, e)
        }
      }
    }
    node.addEventListener(name, fp, false);
    return fp
  }, remove:function(node, event, handle) {
    if(node) {
      event = del._normalizeEventName(event);
      if(!midojo.isIE && (event == "mouseenter" || event == "mouseleave")) {
        event = event == "mouseenter" ? "mouseover" : "mouseout"
      }
      node.removeEventListener(event, handle, false)
    }
  }, _normalizeEventName:function(name) {
    return name.slice(0, 2) == "on" ? name.slice(2) : name
  }, _fixCallback:function(name, fp) {
    return name != "keypress" ? fp : function(e) {
      return fp.call(this, del._fixEvent(e, this))
    }
  }, _fixEvent:function(evt, sender) {
    switch(evt.type) {
      case "keypress":
        del._setKeyChar(evt);
        break
    }
    return evt
  }, _setKeyChar:function(evt) {
    evt.keyChar = evt.charCode ? String.fromCharCode(evt.charCode) : "";
    evt.charOrCode = evt.keyChar || evt.keyCode
  }, _punctMap:{106:42, 111:47, 186:59, 187:43, 188:44, 189:45, 190:46, 191:47, 192:96, 219:91, 220:92, 221:93, 222:39}};
  midojo.fixEvent = function(evt, sender) {
    return del._fixEvent(evt, sender)
  };
  midojo.stopEvent = function(evt) {
    evt.preventDefault();
    evt.stopPropagation()
  };
  var node_listener = midojo._listener;
  midojo._connect = function(obj, event, context, method, dontFix) {
    var isNode = obj && (obj.nodeType || obj.attachEvent || obj.addEventListener);
    var lid = isNode ? dontFix ? 2 : 1 : 0, l = [midojo._listener, del, node_listener][lid];
    var h = l.add(obj, event, midojo.hitch(context, method));
    return[obj, event, h, lid]
  };
  midojo._disconnect = function(obj, event, handle, listener) {
    [midojo._listener, del, node_listener][listener].remove(obj, event, handle)
  };
  midojo.keys = {BACKSPACE:8, TAB:9, CLEAR:12, ENTER:13, SHIFT:16, CTRL:17, ALT:18, META:midojo.isSafari ? 91 : 224, PAUSE:19, CAPS_LOCK:20, ESCAPE:27, SPACE:32, PAGE_UP:33, PAGE_DOWN:34, END:35, HOME:36, LEFT_ARROW:37, UP_ARROW:38, RIGHT_ARROW:39, DOWN_ARROW:40, INSERT:45, DELETE:46, HELP:47, LEFT_WINDOW:91, RIGHT_WINDOW:92, SELECT:93, NUMPAD_0:96, NUMPAD_1:97, NUMPAD_2:98, NUMPAD_3:99, NUMPAD_4:100, NUMPAD_5:101, NUMPAD_6:102, NUMPAD_7:103, NUMPAD_8:104, NUMPAD_9:105, NUMPAD_MULTIPLY:106, NUMPAD_PLUS:107, 
  NUMPAD_ENTER:108, NUMPAD_MINUS:109, NUMPAD_PERIOD:110, NUMPAD_DIVIDE:111, F1:112, F2:113, F3:114, F4:115, F5:116, F6:117, F7:118, F8:119, F9:120, F10:121, F11:122, F12:123, F13:124, F14:125, F15:126, NUM_LOCK:144, SCROLL_LOCK:145, copyKey:midojo.isMac && !midojo.isAIR ? midojo.isSafari ? 91 : 224 : 17};
  var evtCopyKey = midojo.isMac ? "metaKey" : "ctrlKey";
  midojo.isCopyKey = function(e) {
    return e[evtCopyKey]
  };
  if(midojo.isIE) {
    midojo.mouseButtons = {LEFT:1, MIDDLE:4, RIGHT:2, isButton:function(e, button) {
      return e.button & button
    }, isLeft:function(e) {
      return e.button & 1
    }, isMiddle:function(e) {
      return e.button & 4
    }, isRight:function(e) {
      return e.button & 2
    }}
  }else {
    midojo.mouseButtons = {LEFT:0, MIDDLE:1, RIGHT:2, isButton:function(e, button) {
      return e.button == button
    }, isLeft:function(e) {
      return e.button == 0
    }, isMiddle:function(e) {
      return e.button == 1
    }, isRight:function(e) {
      return e.button == 2
    }}
  }
  if(midojo.isIE) {
    var _trySetKeyCode = function(e, code) {
      try {
        return e.keyCode = code
      }catch(e) {
        return 0
      }
    };
    var iel = midojo._listener;
    var listenersName = midojo._ieListenersName = "_" + midojo._scopeName + "_listeners";
    if(!midojo.config._allow_leaks) {
      node_listener = iel = midojo._ie_listener = {handlers:[], add:function(source, method, listener) {
        source = source || midojo.global;
        var f = source[method];
        if(!f || !f[listenersName]) {
          var d = midojo._getIeDispatcher();
          d.target = f && ieh.push(f) - 1;
          d[listenersName] = [];
          f = source[method] = d
        }
        return f[listenersName].push(ieh.push(listener) - 1)
      }, remove:function(source, method, handle) {
        var f = (source || midojo.global)[method], l = f && f[listenersName];
        if(f && l && handle--) {
          delete ieh[l[handle]];
          delete l[handle]
        }
      }};
      var ieh = iel.handlers
    }
    midojo.mixin(del, {add:function(node, event, fp) {
      if(!node) {
        return
      }
      event = del._normalizeEventName(event);
      if(event == "onkeypress") {
        var kd = node.onkeydown;
        if(!kd || !kd[listenersName] || !kd._stealthKeydownHandle) {
          var h = del.add(node, "onkeydown", del._stealthKeyDown);
          kd = node.onkeydown;
          kd._stealthKeydownHandle = h;
          kd._stealthKeydownRefs = 1
        }else {
          kd._stealthKeydownRefs++
        }
      }
      return iel.add(node, event, del._fixCallback(fp))
    }, remove:function(node, event, handle) {
      event = del._normalizeEventName(event);
      iel.remove(node, event, handle);
      if(event == "onkeypress") {
        var kd = node.onkeydown;
        if(--kd._stealthKeydownRefs <= 0) {
          iel.remove(node, "onkeydown", kd._stealthKeydownHandle);
          delete kd._stealthKeydownHandle
        }
      }
    }, _normalizeEventName:function(eventName) {
      return eventName.slice(0, 2) != "on" ? "on" + eventName : eventName
    }, _nop:function() {
    }, _fixEvent:function(evt, sender) {
      if(!evt) {
        var w = sender && (sender.ownerDocument || sender.document || sender).parentWindow || window;
        evt = w.event
      }
      if(!evt) {
        return evt
      }
      evt.target = evt.srcElement;
      evt.currentTarget = sender || evt.srcElement;
      evt.layerX = evt.offsetX;
      evt.layerY = evt.offsetY;
      var se = evt.srcElement, doc = se && se.ownerDocument || document;
      var docBody = midojo.isIE < 6 || doc["compatMode"] == "BackCompat" ? doc.body : doc.documentElement;
      var offset = midojo._getIeDocumentElementOffset();
      evt.pageX = evt.clientX + midojo._fixIeBiDiScrollLeft(docBody.scrollLeft || 0) - offset.x;
      evt.pageY = evt.clientY + (docBody.scrollTop || 0) - offset.y;
      if(evt.type == "mouseover") {
        evt.relatedTarget = evt.fromElement
      }
      if(evt.type == "mouseout") {
        evt.relatedTarget = evt.toElement
      }
      evt.stopPropagation = del._stopPropagation;
      evt.preventDefault = del._preventDefault;
      return del._fixKeys(evt)
    }, _fixKeys:function(evt) {
      switch(evt.type) {
        case "keypress":
          var c = "charCode" in evt ? evt.charCode : evt.keyCode;
          if(c == 10) {
            c = 0;
            evt.keyCode = 13
          }else {
            if(c == 13 || c == 27) {
              c = 0
            }else {
              if(c == 3) {
                c = 99
              }
            }
          }
          evt.charCode = c;
          del._setKeyChar(evt);
          break
      }
      return evt
    }, _stealthKeyDown:function(evt) {
      var kp = evt.currentTarget.onkeypress;
      if(!kp || !kp[listenersName]) {
        return
      }
      var k = evt.keyCode;
      var unprintable = k != 13 && k != 32 && k != 27 && (k < 48 || k > 90) && (k < 96 || k > 111) && (k < 186 || k > 192) && (k < 219 || k > 222);
      if(unprintable || evt.ctrlKey) {
        var c = unprintable ? 0 : k;
        if(evt.ctrlKey) {
          if(k == 3 || k == 13) {
            return
          }else {
            if(c > 95 && c < 106) {
              c -= 48
            }else {
              if(!evt.shiftKey && c >= 65 && c <= 90) {
                c += 32
              }else {
                c = del._punctMap[c] || c
              }
            }
          }
        }
        var faux = del._synthesizeEvent(evt, {type:"keypress", faux:true, charCode:c});
        kp.call(evt.currentTarget, faux);
        evt.cancelBubble = faux.cancelBubble;
        evt.returnValue = faux.returnValue;
        _trySetKeyCode(evt, faux.keyCode)
      }
    }, _stopPropagation:function() {
      this.cancelBubble = true
    }, _preventDefault:function() {
      this.bubbledKeyCode = this.keyCode;
      if(this.ctrlKey) {
        _trySetKeyCode(this, 0)
      }
      this.returnValue = false
    }});
    midojo.stopEvent = function(evt) {
      evt = evt || window.event;
      del._stopPropagation.call(evt);
      del._preventDefault.call(evt)
    }
  }
  del._synthesizeEvent = function(evt, props) {
    var faux = midojo.mixin({}, evt, props);
    del._setKeyChar(faux);
    faux.preventDefault = function() {
      evt.preventDefault()
    };
    faux.stopPropagation = function() {
      evt.stopPropagation()
    };
    return faux
  };
  if(midojo.isOpera) {
    midojo.mixin(del, {_fixEvent:function(evt, sender) {
      switch(evt.type) {
        case "keypress":
          var c = evt.which;
          if(c == 3) {
            c = 99
          }
          c = c < 41 && !evt.shiftKey ? 0 : c;
          if(evt.ctrlKey && !evt.shiftKey && c >= 65 && c <= 90) {
            c += 32
          }
          return del._synthesizeEvent(evt, {charCode:c})
      }
      return evt
    }})
  }
  if(midojo.isWebKit) {
    del._add = del.add;
    del._remove = del.remove;
    midojo.mixin(del, {add:function(node, event, fp) {
      if(!node) {
        return
      }
      var handle = del._add(node, event, fp);
      if(del._normalizeEventName(event) == "keypress") {
        handle._stealthKeyDownHandle = del._add(node, "keydown", function(evt) {
          var k = evt.keyCode;
          var unprintable = k != 13 && k != 32 && (k < 48 || k > 90) && (k < 96 || k > 111) && (k < 186 || k > 192) && (k < 219 || k > 222);
          if(unprintable || evt.ctrlKey) {
            var c = unprintable ? 0 : k;
            if(evt.ctrlKey) {
              if(k == 3 || k == 13) {
                return
              }else {
                if(c > 95 && c < 106) {
                  c -= 48
                }else {
                  if(!evt.shiftKey && c >= 65 && c <= 90) {
                    c += 32
                  }else {
                    c = del._punctMap[c] || c
                  }
                }
              }
            }
            var faux = del._synthesizeEvent(evt, {type:"keypress", faux:true, charCode:c});
            fp.call(evt.currentTarget, faux)
          }
        })
      }
      return handle
    }, remove:function(node, event, handle) {
      if(node) {
        if(handle._stealthKeyDownHandle) {
          del._remove(node, "keydown", handle._stealthKeyDownHandle)
        }
        del._remove(node, event, handle)
      }
    }, _fixEvent:function(evt, sender) {
      switch(evt.type) {
        case "keypress":
          if(evt.faux) {
            return evt
          }
          var c = evt.charCode;
          c = c >= 32 ? c : 0;
          return del._synthesizeEvent(evt, {charCode:c, faux:true})
      }
      return evt
    }})
  }
})();
if(midojo.isIE) {
  midojo._ieDispatcher = function(args, sender) {
    var ap = Array.prototype, h = midojo._ie_listener.handlers, c = args.callee, ls = c[midojo._ieListenersName], t = h[c.target];
    var r = t && t.apply(sender, args);
    var lls = [].concat(ls);
    for(var i in lls) {
      var f = h[lls[i]];
      if(!(i in ap) && f) {
        f.apply(sender, args)
      }
    }
    return r
  };
  midojo._getIeDispatcher = function() {
    return new Function(midojo._scopeName + "._ieDispatcher(arguments, this)")
  };
  midojo._event_listener._fixCallback = function(fp) {
    var f = midojo._event_listener._fixEvent;
    return function(e) {
      return fp.call(this, f(e, this))
    }
  }
}
;
midojo.provide("midojo._base.html");
try {
  document.execCommand("BackgroundImageCache", false, true)
}catch(e) {
}
if(midojo.isIE || midojo.isOpera) {
  midojo.byId = function(id, doc) {
    if(typeof id != "string") {
      return id
    }
    var _d = doc || midojo.doc, te = _d.getElementById(id);
    if(te && (te.attributes.id.value == id || te.id == id)) {
      return te
    }else {
      var eles = _d.all[id];
      if(!eles || eles.nodeName) {
        eles = [eles]
      }
      var i = 0;
      while(te = eles[i++]) {
        if(te.attributes && te.attributes.id && te.attributes.id.value == id || te.id == id) {
          return te
        }
      }
    }
  }
}else {
  midojo.byId = function(id, doc) {
    return typeof id == "string" ? (doc || midojo.doc).getElementById(id) : id
  }
}
(function() {
  var d = midojo;
  var byId = d.byId;
  var _destroyContainer = null, _destroyDoc;
  d.addOnWindowUnload(function() {
    _destroyContainer = null
  });
  midojo._destroyElement = midojo.destroy = function(node) {
    node = byId(node);
    try {
      var doc = node.ownerDocument;
      if(!_destroyContainer || _destroyDoc != doc) {
        _destroyContainer = doc.createElement("div");
        _destroyDoc = doc
      }
      _destroyContainer.appendChild(node.parentNode ? node.parentNode.removeChild(node) : node);
      _destroyContainer.innerHTML = ""
    }catch(e) {
    }
  };
  midojo.isDescendant = function(node, ancestor) {
    try {
      node = byId(node);
      ancestor = byId(ancestor);
      while(node) {
        if(node == ancestor) {
          return true
        }
        node = node.parentNode
      }
    }catch(e) {
    }
    return false
  };
  midojo.setSelectable = function(node, selectable) {
    node = byId(node);
    if(d.isMozilla) {
      node.style.MozUserSelect = selectable ? "" : "none"
    }else {
      if(d.isKhtml || d.isWebKit) {
        node.style.KhtmlUserSelect = selectable ? "auto" : "none"
      }else {
        if(d.isIE) {
          var v = node.unselectable = selectable ? "" : "on";
          d.query("*", node).forEach("item.unselectable = '" + v + "'")
        }
      }
    }
  };
  var _insertBefore = function(node, ref) {
    var parent = ref.parentNode;
    if(parent) {
      parent.insertBefore(node, ref)
    }
  };
  var _insertAfter = function(node, ref) {
    var parent = ref.parentNode;
    if(parent) {
      if(parent.lastChild == ref) {
        parent.appendChild(node)
      }else {
        parent.insertBefore(node, ref.nextSibling)
      }
    }
  };
  midojo.place = function(node, refNode, position) {
    refNode = byId(refNode);
    if(typeof node == "string") {
      node = node.charAt(0) == "<" ? d._toDom(node, refNode.ownerDocument) : byId(node)
    }
    if(typeof position == "number") {
      var cn = refNode.childNodes;
      if(!cn.length || cn.length <= position) {
        refNode.appendChild(node)
      }else {
        _insertBefore(node, cn[position < 0 ? 0 : position])
      }
    }else {
      switch(position) {
        case "before":
          _insertBefore(node, refNode);
          break;
        case "after":
          _insertAfter(node, refNode);
          break;
        case "replace":
          refNode.parentNode.replaceChild(node, refNode);
          break;
        case "only":
          d.empty(refNode);
          refNode.appendChild(node);
          break;
        case "first":
          if(refNode.firstChild) {
            _insertBefore(node, refNode.firstChild);
            break
          }
        ;
        default:
          refNode.appendChild(node)
      }
    }
    return node
  };
  midojo.boxModel = "content-box";
  if(d.isIE) {
    d.boxModel = document.compatMode == "BackCompat" ? "border-box" : "content-box"
  }
  var gcs;
  if(d.isWebKit) {
    gcs = function(node) {
      var s;
      if(node.nodeType == 1) {
        var dv = node.ownerDocument.defaultView;
        s = dv.getComputedStyle(node, null);
        if(!s && node.style) {
          node.style.display = "";
          s = dv.getComputedStyle(node, null)
        }
      }
      return s || {}
    }
  }else {
    if(d.isIE) {
      gcs = function(node) {
        return node.nodeType == 1 ? node.currentStyle : {}
      }
    }else {
      gcs = function(node) {
        return node.nodeType == 1 ? node.ownerDocument.defaultView.getComputedStyle(node, null) : {}
      }
    }
  }
  midojo.getComputedStyle = gcs;
  if(!d.isIE) {
    d._toPixelValue = function(element, value) {
      return parseFloat(value) || 0
    }
  }else {
    d._toPixelValue = function(element, avalue) {
      if(!avalue) {
        return 0
      }
      if(avalue == "medium") {
        return 4
      }
      if(avalue.slice && avalue.slice(-2) == "px") {
        return parseFloat(avalue)
      }
      with(element) {
        var sLeft = style.left;
        var rsLeft = runtimeStyle.left;
        runtimeStyle.left = currentStyle.left;
        try {
          style.left = avalue;
          avalue = style.pixelLeft
        }catch(e) {
          avalue = 0
        }
        style.left = sLeft;
        runtimeStyle.left = rsLeft
      }
      return avalue
    }
  }
  var px = d._toPixelValue;
  var astr = "DXImageTransform.Microsoft.Alpha";
  var af = function(n, f) {
    try {
      return n.filters.item(astr)
    }catch(e) {
      return f ? {} : null
    }
  };
  midojo._getOpacity = d.isIE ? function(node) {
    try {
      return af(node).Opacity / 100
    }catch(e) {
      return 1
    }
  } : function(node) {
    return gcs(node).opacity
  };
  midojo._setOpacity = d.isIE ? function(node, opacity) {
    var ov = opacity * 100;
    node.style.zoom = 1;
    af(node, 1).Enabled = !(opacity == 1);
    if(!af(node)) {
      node.style.filter += " progid:" + astr + "(Opacity=" + ov + ")"
    }else {
      af(node, 1).Opacity = ov
    }
    if(node.nodeName.toLowerCase() == "tr") {
      d.query("> td", node).forEach(function(i) {
        d._setOpacity(i, opacity)
      })
    }
    return opacity
  } : function(node, opacity) {
    return node.style.opacity = opacity
  };
  var _pixelNamesCache = {left:true, top:true};
  var _pixelRegExp = /margin|padding|width|height|max|min|offset/;
  var _toStyleValue = function(node, type, value) {
    type = type.toLowerCase();
    if(d.isIE) {
      if(value == "auto") {
        if(type == "height") {
          return node.offsetHeight
        }
        if(type == "width") {
          return node.offsetWidth
        }
      }
      if(type == "fontweight") {
        switch(value) {
          case 700:
            return"bold";
          case 400:
          ;
          default:
            return"normal"
        }
      }
    }
    if(!(type in _pixelNamesCache)) {
      _pixelNamesCache[type] = _pixelRegExp.test(type)
    }
    return _pixelNamesCache[type] ? px(node, value) : value
  };
  var _floatStyle = d.isIE ? "styleFloat" : "cssFloat", _floatAliases = {"cssFloat":_floatStyle, "styleFloat":_floatStyle, "float":_floatStyle};
  midojo.style = function(node, style, value) {
    var n = byId(node), args = arguments.length, op = style == "opacity";
    style = _floatAliases[style] || style;
    if(args == 3) {
      return op ? d._setOpacity(n, value) : n.style[style] = value
    }
    if(args == 2 && op) {
      return d._getOpacity(n)
    }
    var s = gcs(n);
    if(args == 2 && typeof style != "string") {
      for(var x in style) {
        d.style(node, x, style[x])
      }
      return s
    }
    return args == 1 ? s : _toStyleValue(n, style, s[style] || n.style[style])
  };
  midojo._getPadExtents = function(n, computedStyle) {
    var s = computedStyle || gcs(n), l = px(n, s.paddingLeft), t = px(n, s.paddingTop);
    return{l:l, t:t, w:l + px(n, s.paddingRight), h:t + px(n, s.paddingBottom)}
  };
  midojo._getBorderExtents = function(n, computedStyle) {
    var ne = "none", s = computedStyle || gcs(n), bl = s.borderLeftStyle != ne ? px(n, s.borderLeftWidth) : 0, bt = s.borderTopStyle != ne ? px(n, s.borderTopWidth) : 0;
    return{l:bl, t:bt, w:bl + (s.borderRightStyle != ne ? px(n, s.borderRightWidth) : 0), h:bt + (s.borderBottomStyle != ne ? px(n, s.borderBottomWidth) : 0)}
  };
  midojo._getPadBorderExtents = function(n, computedStyle) {
    var s = computedStyle || gcs(n), p = d._getPadExtents(n, s), b = d._getBorderExtents(n, s);
    return{l:p.l + b.l, t:p.t + b.t, w:p.w + b.w, h:p.h + b.h}
  };
  midojo._getMarginExtents = function(n, computedStyle) {
    var s = computedStyle || gcs(n), l = px(n, s.marginLeft), t = px(n, s.marginTop), r = px(n, s.marginRight), b = px(n, s.marginBottom);
    if(d.isWebKit && s.position != "absolute") {
      r = l
    }
    return{l:l, t:t, w:l + r, h:t + b}
  };
  midojo._getMarginBox = function(node, computedStyle) {
    var s = computedStyle || gcs(node), me = d._getMarginExtents(node, s);
    var l = node.offsetLeft - me.l, t = node.offsetTop - me.t, p = node.parentNode;
    if(d.isMoz) {
      var sl = parseFloat(s.left), st = parseFloat(s.top);
      if(!isNaN(sl) && !isNaN(st)) {
        l = sl, t = st
      }else {
        if(p && p.style) {
          var pcs = gcs(p);
          if(pcs.overflow != "visible") {
            var be = d._getBorderExtents(p, pcs);
            l += be.l, t += be.t
          }
        }
      }
    }else {
      if(d.isOpera || d.isIE > 7 && !d.isQuirks) {
        if(p) {
          be = d._getBorderExtents(p);
          l -= be.l;
          t -= be.t
        }
      }
    }
    return{l:l, t:t, w:node.offsetWidth + me.w, h:node.offsetHeight + me.h}
  };
  midojo._getContentBox = function(node, computedStyle) {
    var s = computedStyle || gcs(node), pe = d._getPadExtents(node, s), be = d._getBorderExtents(node, s), w = node.clientWidth, h;
    if(!w) {
      w = node.offsetWidth, h = node.offsetHeight
    }else {
      h = node.clientHeight, be.w = be.h = 0
    }
    if(d.isOpera) {
      pe.l += be.l;
      pe.t += be.t
    }
    return{l:pe.l, t:pe.t, w:w - pe.w - be.w, h:h - pe.h - be.h}
  };
  midojo._getBorderBox = function(node, computedStyle) {
    var s = computedStyle || gcs(node), pe = d._getPadExtents(node, s), cb = d._getContentBox(node, s);
    return{l:cb.l - pe.l, t:cb.t - pe.t, w:cb.w + pe.w, h:cb.h + pe.h}
  };
  midojo._setBox = function(node, l, t, w, h, u) {
    u = u || "px";
    var s = node.style;
    if(!isNaN(l)) {
      s.left = l + u
    }
    if(!isNaN(t)) {
      s.top = t + u
    }
    if(w >= 0) {
      s.width = w + u
    }
    if(h >= 0) {
      s.height = h + u
    }
  };
  midojo._isButtonTag = function(node) {
    return node.tagName == "BUTTON" || node.tagName == "INPUT" && (node.getAttribute("type") || "").toUpperCase() == "BUTTON"
  };
  midojo._usesBorderBox = function(node) {
    var n = node.tagName;
    return d.boxModel == "border-box" || n == "TABLE" || d._isButtonTag(node)
  };
  midojo._setContentSize = function(node, widthPx, heightPx, computedStyle) {
    if(d._usesBorderBox(node)) {
      var pb = d._getPadBorderExtents(node, computedStyle);
      if(widthPx >= 0) {
        widthPx += pb.w
      }
      if(heightPx >= 0) {
        heightPx += pb.h
      }
    }
    d._setBox(node, NaN, NaN, widthPx, heightPx)
  };
  midojo._setMarginBox = function(node, leftPx, topPx, widthPx, heightPx, computedStyle) {
    var s = computedStyle || gcs(node), bb = d._usesBorderBox(node), pb = bb ? _nilExtents : d._getPadBorderExtents(node, s);
    if(d.isWebKit) {
      if(d._isButtonTag(node)) {
        var ns = node.style;
        if(widthPx >= 0 && !ns.width) {
          ns.width = "4px"
        }
        if(heightPx >= 0 && !ns.height) {
          ns.height = "4px"
        }
      }
    }
    var mb = d._getMarginExtents(node, s);
    if(widthPx >= 0) {
      widthPx = Math.max(widthPx - pb.w - mb.w, 0)
    }
    if(heightPx >= 0) {
      heightPx = Math.max(heightPx - pb.h - mb.h, 0)
    }
    d._setBox(node, leftPx, topPx, widthPx, heightPx)
  };
  var _nilExtents = {l:0, t:0, w:0, h:0};
  midojo.marginBox = function(node, box) {
    var n = byId(node), s = gcs(n), b = box;
    return!b ? d._getMarginBox(n, s) : d._setMarginBox(n, b.l, b.t, b.w, b.h, s)
  };
  midojo.contentBox = function(node, box) {
    var n = byId(node), s = gcs(n), b = box;
    return!b ? d._getContentBox(n, s) : d._setContentSize(n, b.w, b.h, s)
  };
  var _sumAncestorProperties = function(node, prop) {
    if(!(node = (node || 0).parentNode)) {
      return 0
    }
    var val, retVal = 0, _b = d.body();
    while(node && node.style) {
      if(gcs(node).position == "fixed") {
        return 0
      }
      val = node[prop];
      if(val) {
        retVal += val - 0;
        if(node == _b) {
          break
        }
      }
      node = node.parentNode
    }
    return retVal
  };
  midojo._docScroll = function() {
    var n = d.global;
    return"pageXOffset" in n ? {x:n.pageXOffset, y:n.pageYOffset} : (n = d.doc.documentElement, n.clientHeight ? {x:d._fixIeBiDiScrollLeft(n.scrollLeft), y:n.scrollTop} : (n = d.body(), {x:n.scrollLeft || 0, y:n.scrollTop || 0}))
  };
  midojo._isBodyLtr = function() {
    return"_bodyLtr" in d ? d._bodyLtr : d._bodyLtr = (d.body().dir || d.doc.documentElement.dir || "ltr").toLowerCase() == "ltr"
  };
  midojo._getIeDocumentElementOffset = function() {
    var de = d.doc.documentElement;
    if(d.isIE < 8) {
      var r = de.getBoundingClientRect();
      var l = r.left, t = r.top;
      if(d.isIE < 7) {
        l += de.clientLeft;
        t += de.clientTop
      }
      return{x:l < 0 ? 0 : l, y:t < 0 ? 0 : t}
    }else {
      return{x:0, y:0}
    }
  };
  midojo._fixIeBiDiScrollLeft = function(scrollLeft) {
    var dd = d.doc;
    if(d.isIE < 8 && !d._isBodyLtr()) {
      var de = d.isQuirks ? dd.body : dd.documentElement;
      return scrollLeft + de.clientWidth - de.scrollWidth
    }
    return scrollLeft
  };
  midojo._abs = midojo.position = function(node, includeScroll) {
    var db = d.body(), dh = db.parentNode, ret;
    node = byId(node);
    if(node["getBoundingClientRect"]) {
      ret = node.getBoundingClientRect();
      ret = {x:ret.left, y:ret.top, w:ret.right - ret.left, h:ret.bottom - ret.top};
      if(d.isIE) {
        var offset = d._getIeDocumentElementOffset();
        ret.x -= offset.x + (d.isQuirks ? db.clientLeft + db.offsetLeft : 0);
        ret.y -= offset.y + (d.isQuirks ? db.clientTop + db.offsetTop : 0)
      }else {
        if(d.isFF == 3) {
          var cs = gcs(dh);
          ret.x -= px(dh, cs.marginLeft) + px(dh, cs.borderLeftWidth);
          ret.y -= px(dh, cs.marginTop) + px(dh, cs.borderTopWidth)
        }
      }
    }else {
      ret = {x:0, y:0, w:node.offsetWidth, h:node.offsetHeight};
      if(node["offsetParent"]) {
        ret.x -= _sumAncestorProperties(node, "scrollLeft");
        ret.y -= _sumAncestorProperties(node, "scrollTop");
        var curnode = node;
        do {
          var n = curnode.offsetLeft, t = curnode.offsetTop;
          ret.x += isNaN(n) ? 0 : n;
          ret.y += isNaN(t) ? 0 : t;
          cs = gcs(curnode);
          if(curnode != node) {
            if(d.isMoz) {
              ret.x += 2 * px(curnode, cs.borderLeftWidth);
              ret.y += 2 * px(curnode, cs.borderTopWidth)
            }else {
              ret.x += px(curnode, cs.borderLeftWidth);
              ret.y += px(curnode, cs.borderTopWidth)
            }
          }
          if(d.isMoz && cs.position == "static") {
            var parent = curnode.parentNode;
            while(parent != curnode.offsetParent) {
              var pcs = gcs(parent);
              if(pcs.position == "static") {
                ret.x += px(curnode, pcs.borderLeftWidth);
                ret.y += px(curnode, pcs.borderTopWidth)
              }
              parent = parent.parentNode
            }
          }
          curnode = curnode.offsetParent
        }while(curnode != dh && curnode)
      }else {
        if(node.x && node.y) {
          ret.x += isNaN(node.x) ? 0 : node.x;
          ret.y += isNaN(node.y) ? 0 : node.y
        }
      }
    }
    if(includeScroll) {
      var scroll = d._docScroll();
      ret.x += scroll.x;
      ret.y += scroll.y
    }
    return ret
  };
  midojo.coords = function(node, includeScroll) {
    var n = byId(node), s = gcs(n), mb = d._getMarginBox(n, s);
    var abs = d.position(n, includeScroll);
    mb.x = abs.x;
    mb.y = abs.y;
    return mb
  };
  var _propNames = {"class":"className", "for":"htmlFor", tabindex:"tabIndex", readonly:"readOnly", colspan:"colSpan", frameborder:"frameBorder", rowspan:"rowSpan", valuetype:"valueType"}, _attrNames = {classname:"class", htmlfor:"for", tabindex:"tabIndex", readonly:"readOnly"}, _forcePropNames = {innerHTML:1, className:1, htmlFor:d.isIE, value:1};
  var _fixAttrName = function(name) {
    return _attrNames[name.toLowerCase()] || name
  };
  var _hasAttr = function(node, name) {
    var attr = node.getAttributeNode && node.getAttributeNode(name);
    return attr && attr.specified
  };
  midojo.hasAttr = function(node, name) {
    var lc = name.toLowerCase();
    return _forcePropNames[_propNames[lc] || name] || _hasAttr(byId(node), _attrNames[lc] || name)
  };
  var _evtHdlrMap = {}, _ctr = 0, _attrId = midojo._scopeName + "attrid", _roInnerHtml = {col:1, colgroup:1, table:1, tbody:1, tfoot:1, thead:1, tr:1, title:1};
  midojo.attr = function(node, name, value) {
    node = byId(node);
    var args = arguments.length, prop;
    if(args == 2 && typeof name != "string") {
      for(var x in name) {
        d.attr(node, x, name[x])
      }
      return node
    }
    var lc = name.toLowerCase(), propName = _propNames[lc] || name, forceProp = _forcePropNames[propName], attrName = _attrNames[lc] || name;
    if(args == 3) {
      do {
        if(propName == "style" && typeof value != "string") {
          d.style(node, value);
          break
        }
        if(propName == "innerHTML") {
          if(d.isIE && node.tagName.toLowerCase() in _roInnerHtml) {
            d.empty(node);
            node.appendChild(d._toDom(value, node.ownerDocument))
          }else {
            node[propName] = value
          }
          break
        }
        if(d.isFunction(value)) {
          var attrId = d.attr(node, _attrId);
          if(!attrId) {
            attrId = _ctr++;
            d.attr(node, _attrId, attrId)
          }
          if(!_evtHdlrMap[attrId]) {
            _evtHdlrMap[attrId] = {}
          }
          var h = _evtHdlrMap[attrId][propName];
          if(h) {
            d.disconnect(h)
          }else {
            try {
              delete node[propName]
            }catch(e) {
            }
          }
          _evtHdlrMap[attrId][propName] = d.connect(node, propName, value);
          break
        }
        if(forceProp || typeof value == "boolean") {
          node[propName] = value;
          break
        }
        node.setAttribute(attrName, value)
      }while(false);
      return node
    }
    value = node[propName];
    if(forceProp && typeof value != "undefined") {
      return value
    }
    if(propName != "href" && (typeof value == "boolean" || d.isFunction(value))) {
      return value
    }
    return _hasAttr(node, attrName) ? node.getAttribute(attrName) : null
  };
  midojo.removeAttr = function(node, name) {
    byId(node).removeAttribute(_fixAttrName(name))
  };
  midojo.getNodeProp = function(node, name) {
    node = byId(node);
    var lc = name.toLowerCase(), propName = _propNames[lc] || name;
    if(propName in node && propName != "href") {
      return node[propName]
    }
    var attrName = _attrNames[lc] || name;
    return _hasAttr(node, attrName) ? node.getAttribute(attrName) : null
  };
  midojo.create = function(tag, attrs, refNode, pos) {
    var doc = d.doc;
    if(refNode) {
      refNode = byId(refNode);
      doc = refNode.ownerDocument
    }
    if(typeof tag == "string") {
      tag = doc.createElement(tag)
    }
    if(attrs) {
      d.attr(tag, attrs)
    }
    if(refNode) {
      d.place(tag, refNode, pos)
    }
    return tag
  };
  d.empty = d.isIE ? function(node) {
    node = byId(node);
    for(var c;c = node.lastChild;) {
      d.destroy(c)
    }
  } : function(node) {
    byId(node).innerHTML = ""
  };
  var tagWrap = {option:["select"], tbody:["table"], thead:["table"], tfoot:["table"], tr:["table", "tbody"], td:["table", "tbody", "tr"], th:["table", "thead", "tr"], legend:["fieldset"], caption:["table"], colgroup:["table"], col:["table", "colgroup"], li:["ul"]}, reTag = /<\s*([\w\:]+)/, masterNode = {}, masterNum = 0, masterName = "__" + d._scopeName + "ToDomId";
  for(var param in tagWrap) {
    var tw = tagWrap[param];
    tw.pre = param == "option" ? '<select multiple="multiple">' : "<" + tw.join("><") + ">";
    tw.post = "</" + tw.reverse().join("></") + ">"
  }
  d._toDom = function(frag, doc) {
    doc = doc || d.doc;
    var masterId = doc[masterName];
    if(!masterId) {
      doc[masterName] = masterId = ++masterNum + "";
      masterNode[masterId] = doc.createElement("div")
    }
    frag += "";
    var match = frag.match(reTag), tag = match ? match[1].toLowerCase() : "", master = masterNode[masterId], wrap, i, fc, df;
    if(match && tagWrap[tag]) {
      wrap = tagWrap[tag];
      master.innerHTML = wrap.pre + frag + wrap.post;
      for(i = wrap.length;i;--i) {
        master = master.firstChild
      }
    }else {
      master.innerHTML = frag
    }
    if(master.childNodes.length == 1) {
      return master.removeChild(master.firstChild)
    }
    df = doc.createDocumentFragment();
    while(fc = master.firstChild) {
      df.appendChild(fc)
    }
    return df
  };
  var _className = "className";
  midojo.hasClass = function(node, classStr) {
    return(" " + byId(node)[_className] + " ").indexOf(" " + classStr + " ") >= 0
  };
  var spaces = /\s+/, a1 = [""], str2array = function(s) {
    if(typeof s == "string" || s instanceof String) {
      if(s.indexOf(" ") < 0) {
        a1[0] = s;
        return a1
      }else {
        return s.split(spaces)
      }
    }
    return s
  };
  midojo.addClass = function(node, classStr) {
    node = byId(node);
    classStr = str2array(classStr);
    var cls = " " + node[_className] + " ";
    for(var i = 0, len = classStr.length, c;i < len;++i) {
      c = classStr[i];
      if(c && cls.indexOf(" " + c + " ") < 0) {
        cls += c + " "
      }
    }
    node[_className] = d.trim(cls)
  };
  midojo.removeClass = function(node, classStr) {
    node = byId(node);
    var cls;
    if(classStr !== undefined) {
      classStr = str2array(classStr);
      cls = " " + node[_className] + " ";
      for(var i = 0, len = classStr.length;i < len;++i) {
        cls = cls.replace(" " + classStr[i] + " ", " ")
      }
      cls = d.trim(cls)
    }else {
      cls = ""
    }
    if(node[_className] != cls) {
      node[_className] = cls
    }
  };
  midojo.toggleClass = function(node, classStr, condition) {
    if(condition === undefined) {
      condition = !d.hasClass(node, classStr)
    }
    d[condition ? "addClass" : "removeClass"](node, classStr)
  }
})();
midojo.provide("midojo._base.NodeList");
(function() {
  var d = midojo;
  var ap = Array.prototype, aps = ap.slice, apc = ap.concat;
  var tnl = function(a, parent, NodeListCtor) {
    if(!a.sort) {
      a = aps.call(a, 0)
    }
    var ctor = NodeListCtor || this._NodeListCtor || d._NodeListCtor;
    a.constructor = ctor;
    midojo._mixin(a, ctor.prototype);
    a._NodeListCtor = ctor;
    return parent ? a._stash(parent) : a
  };
  var loopBody = function(f, a, o) {
    a = [0].concat(aps.call(a, 0));
    o = o || d.global;
    return function(node) {
      a[0] = node;
      return f.apply(o, a)
    }
  };
  var adaptAsForEach = function(f, o) {
    return function() {
      this.forEach(loopBody(f, arguments, o));
      return this
    }
  };
  var adaptAsMap = function(f, o) {
    return function() {
      return this.map(loopBody(f, arguments, o))
    }
  };
  var adaptAsFilter = function(f, o) {
    return function() {
      return this.filter(loopBody(f, arguments, o))
    }
  };
  var adaptWithCondition = function(f, g, o) {
    return function() {
      var a = arguments, body = loopBody(f, a, o);
      if(g.call(o || d.global, a)) {
        return this.map(body)
      }
      this.forEach(body);
      return this
    }
  };
  var magicGuard = function(a) {
    return a.length == 1 && typeof a[0] == "string"
  };
  var orphan = function(node) {
    var p = node.parentNode;
    if(p) {
      p.removeChild(node)
    }
  };
  midojo.NodeList = function() {
    return tnl(Array.apply(null, arguments))
  };
  d._NodeListCtor = d.NodeList;
  var nl = d.NodeList, nlp = nl.prototype;
  nl._wrap = nlp._wrap = tnl;
  nl._adaptAsMap = adaptAsMap;
  nl._adaptAsForEach = adaptAsForEach;
  nl._adaptAsFilter = adaptAsFilter;
  nl._adaptWithCondition = adaptWithCondition;
  d.forEach(["slice", "splice"], function(name) {
    var f = ap[name];
    nlp[name] = function() {
      return this._wrap(f.apply(this, arguments), name == "slice" ? this : null)
    }
  });
  d.forEach(["indexOf", "lastIndexOf", "every", "some"], function(name) {
    var f = d[name];
    nlp[name] = function() {
      return f.apply(d, [this].concat(aps.call(arguments, 0)))
    }
  });
  d.forEach(["attr", "style"], function(name) {
    nlp[name] = adaptWithCondition(d[name], magicGuard)
  });
  d.forEach(["connect", "addClass", "removeClass", "toggleClass", "empty", "removeAttr"], function(name) {
    nlp[name] = adaptAsForEach(d[name])
  });
  midojo.extend(midojo.NodeList, {_normalize:function(content, refNode) {
    var parse = content.parse === true ? true : false;
    if(typeof content.template == "string") {
      var templateFunc = content.templateFunc || midojo.string && midojo.string.substitute;
      content = templateFunc ? templateFunc(content.template, content) : content
    }
    var type = typeof content;
    if(type == "string" || type == "number") {
      content = midojo._toDom(content, refNode && refNode.ownerDocument);
      if(content.nodeType == 11) {
        content = midojo._toArray(content.childNodes)
      }else {
        content = [content]
      }
    }else {
      if(!midojo.isArrayLike(content)) {
        content = [content]
      }else {
        if(!midojo.isArray(content)) {
          content = midojo._toArray(content)
        }
      }
    }
    if(parse) {
      content._runParse = true
    }
    return content
  }, _cloneNode:function(node) {
    return node.cloneNode(true)
  }, _place:function(ary, refNode, position, useClone) {
    if(refNode.nodeType != 1 && position == "only") {
      return
    }
    var rNode = refNode, tempNode;
    var length = ary.length;
    for(var i = length - 1;i >= 0;i--) {
      var node = useClone ? this._cloneNode(ary[i]) : ary[i];
      if(ary._runParse && midojo.parser && midojo.parser.parse) {
        if(!tempNode) {
          tempNode = rNode.ownerDocument.createElement("div")
        }
        tempNode.appendChild(node);
        midojo.parser.parse(tempNode);
        node = tempNode.firstChild;
        while(tempNode.firstChild) {
          tempNode.removeChild(tempNode.firstChild)
        }
      }
      if(i == length - 1) {
        midojo.place(node, rNode, position)
      }else {
        rNode.parentNode.insertBefore(node, rNode)
      }
      rNode = node
    }
  }, _stash:function(parent) {
    this._parent = parent;
    return this
  }, end:function() {
    if(this._parent) {
      return this._parent
    }else {
      return new this._NodeListCtor
    }
  }, concat:function(item) {
    var t = d.isArray(this) ? this : aps.call(this, 0), m = d.map(arguments, function(a) {
      return a && !d.isArray(a) && (typeof NodeList != "undefined" && a.constructor === NodeList || a.constructor === this._NodeListCtor) ? aps.call(a, 0) : a
    });
    return this._wrap(apc.apply(t, m), this)
  }, map:function(func, obj) {
    return this._wrap(d.map(this, func, obj), this)
  }, forEach:function(callback, thisObj) {
    d.forEach(this, callback, thisObj);
    return this
  }, coords:adaptAsMap(d.coords), position:adaptAsMap(d.position), place:function(queryOrNode, position) {
    var item = d.query(queryOrNode)[0];
    return this.forEach(function(node) {
      d.place(node, item, position)
    })
  }, orphan:function(simpleFilter) {
    return(simpleFilter ? d._filterQueryResult(this, simpleFilter) : this).forEach(orphan)
  }, adopt:function(queryOrListOrNode, position) {
    return d.query(queryOrListOrNode).place(this[0], position)._stash(this)
  }, query:function(queryStr) {
    if(!queryStr) {
      return this
    }
    var ret = this.map(function(node) {
      return d.query(queryStr, node).filter(function(subNode) {
        return subNode !== undefined
      })
    });
    return this._wrap(apc.apply([], ret), this)
  }, filter:function(simpleFilter) {
    var a = arguments, items = this, start = 0;
    if(typeof simpleFilter == "string") {
      items = d._filterQueryResult(this, a[0]);
      if(a.length == 1) {
        return items._stash(this)
      }
      start = 1
    }
    return this._wrap(d.filter(items, a[start], a[start + 1]), this)
  }, addContent:function(content, position) {
    content = this._normalize(content, this[0]);
    for(var i = 0, node;node = this[i];i++) {
      this._place(content, node, position, i > 0)
    }
    return this
  }, instantiate:function(declaredClass, properties) {
    var c = d.isFunction(declaredClass) ? declaredClass : d.getObject(declaredClass);
    properties = properties || {};
    return this.forEach(function(node) {
      new c(properties, node)
    })
  }, at:function() {
    var t = new this._NodeListCtor;
    d.forEach(arguments, function(i) {
      if(this[i]) {
        t.push(this[i])
      }
    }, this);
    return t._stash(this)
  }});
  nl.events = ["blur", "focus", "change", "click", "error", "keydown", "keypress", "keyup", "load", "mousedown", "mouseenter", "mouseleave", "mousemove", "mouseout", "mouseover", "mouseup", "submit"];
  d.forEach(nl.events, function(evt) {
    var _oe = "on" + evt;
    nlp[_oe] = function(a, b) {
      return this.connect(_oe, a, b)
    }
  })
})();
if(typeof midojo != "undefined") {
  midojo.provide("midojo._base.query")
}else {
  if(!this["acme"] && !this["queryPortability"]) {
    (function() {
      acme = {trim:function(str) {
        str = str.replace(/^\s+/, "");
        for(var i = str.length - 1;i >= 0;i--) {
          if(/\S/.test(str.charAt(i))) {
            str = str.substring(0, i + 1);
            break
          }
        }
        return str
      }, forEach:function(arr, callback, thisObject) {
        if(!arr || !arr.length) {
          return
        }
        for(var i = 0, l = arr.length;i < l;++i) {
          callback.call(thisObject || window, arr[i], i, arr)
        }
      }, byId:function(id, doc) {
        if(typeof id == "string") {
          return(doc || document).getElementById(id)
        }else {
          return id
        }
      }, doc:document, NodeList:Array};
      var n = navigator;
      var dua = n.userAgent;
      var dav = n.appVersion;
      var tv = parseFloat(dav);
      acme.isOpera = dua.indexOf("Opera") >= 0 ? tv : undefined;
      acme.isKhtml = dav.indexOf("Konqueror") >= 0 ? tv : undefined;
      acme.isWebKit = parseFloat(dua.split("WebKit/")[1]) || undefined;
      acme.isChrome = parseFloat(dua.split("Chrome/")[1]) || undefined;
      var index = Math.max(dav.indexOf("WebKit"), dav.indexOf("Safari"), 0);
      if(index && !acme.isChrome) {
        acme.isSafari = parseFloat(dav.split("Version/")[1]);
        if(!acme.isSafari || parseFloat(dav.substr(index + 7)) <= 419.3) {
          acme.isSafari = 2
        }
      }
      if(document.all && !acme.isOpera) {
        acme.isIE = parseFloat(dav.split("MSIE ")[1]) || undefined
      }
      Array._wrap = function(arr) {
        return arr
      }
    })()
  }
}
(function(d) {
  var trim = d.trim;
  var each = d.forEach;
  var qlc = d._NodeListCtor = d.NodeList;
  var getDoc = function() {
    return d.doc
  };
  var cssCaseBug = (d.isWebKit || d.isMozilla) && getDoc().compatMode == "BackCompat";
  var childNodesName = !!getDoc().firstChild["children"] ? "children" : "childNodes";
  var specials = ">~+";
  var caseSensitive = false;
  var yesman = function() {
    return true
  };
  var getQueryParts = function(query) {
    if(specials.indexOf(query.slice(-1)) >= 0) {
      query += " * "
    }else {
      query += " "
    }
    var ts = function(s, e) {
      return trim(query.slice(s, e))
    };
    var queryParts = [];
    var inBrackets = -1, inParens = -1, inMatchFor = -1, inPseudo = -1, inClass = -1, inId = -1, inTag = -1, lc = "", cc = "", pStart;
    var x = 0, ql = query.length, currentPart = null, _cp = null;
    var endTag = function() {
      if(inTag >= 0) {
        var tv = inTag == x ? null : ts(inTag, x);
        currentPart[specials.indexOf(tv) < 0 ? "tag" : "oper"] = tv;
        inTag = -1
      }
    };
    var endId = function() {
      if(inId >= 0) {
        currentPart.id = ts(inId, x).replace(/\\/g, "");
        inId = -1
      }
    };
    var endClass = function() {
      if(inClass >= 0) {
        currentPart.classes.push(ts(inClass + 1, x).replace(/\\/g, ""));
        inClass = -1
      }
    };
    var endAll = function() {
      endId();
      endTag();
      endClass()
    };
    var endPart = function() {
      endAll();
      if(inPseudo >= 0) {
        currentPart.pseudos.push({name:ts(inPseudo + 1, x)})
      }
      currentPart.loops = currentPart.pseudos.length || currentPart.attrs.length || currentPart.classes.length;
      currentPart.oquery = currentPart.query = ts(pStart, x);
      currentPart.otag = currentPart.tag = currentPart["oper"] ? null : currentPart.tag || "*";
      if(currentPart.tag) {
        currentPart.tag = currentPart.tag.toUpperCase()
      }
      if(queryParts.length && queryParts[queryParts.length - 1].oper) {
        currentPart.infixOper = queryParts.pop();
        currentPart.query = currentPart.infixOper.query + " " + currentPart.query
      }
      queryParts.push(currentPart);
      currentPart = null
    };
    for(;lc = cc, cc = query.charAt(x), x < ql;x++) {
      if(lc == "\\") {
        continue
      }
      if(!currentPart) {
        pStart = x;
        currentPart = {query:null, pseudos:[], attrs:[], classes:[], tag:null, oper:null, id:null, getTag:function() {
          return caseSensitive ? this.otag : this.tag
        }};
        inTag = x
      }
      if(inBrackets >= 0) {
        if(cc == "]") {
          if(!_cp.attr) {
            _cp.attr = ts(inBrackets + 1, x)
          }else {
            _cp.matchFor = ts(inMatchFor || inBrackets + 1, x)
          }
          var cmf = _cp.matchFor;
          if(cmf) {
            if(cmf.charAt(0) == '"' || cmf.charAt(0) == "'") {
              _cp.matchFor = cmf.slice(1, -1)
            }
          }
          currentPart.attrs.push(_cp);
          _cp = null;
          inBrackets = inMatchFor = -1
        }else {
          if(cc == "=") {
            var addToCc = "|~^$*".indexOf(lc) >= 0 ? lc : "";
            _cp.type = addToCc + cc;
            _cp.attr = ts(inBrackets + 1, x - addToCc.length);
            inMatchFor = x + 1
          }
        }
      }else {
        if(inParens >= 0) {
          if(cc == ")") {
            if(inPseudo >= 0) {
              _cp.value = ts(inParens + 1, x)
            }
            inPseudo = inParens = -1
          }
        }else {
          if(cc == "#") {
            endAll();
            inId = x + 1
          }else {
            if(cc == ".") {
              endAll();
              inClass = x
            }else {
              if(cc == ":") {
                endAll();
                inPseudo = x
              }else {
                if(cc == "[") {
                  endAll();
                  inBrackets = x;
                  _cp = {}
                }else {
                  if(cc == "(") {
                    if(inPseudo >= 0) {
                      _cp = {name:ts(inPseudo + 1, x), value:null};
                      currentPart.pseudos.push(_cp)
                    }
                    inParens = x
                  }else {
                    if(cc == " " && lc != cc) {
                      endPart()
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    return queryParts
  };
  var agree = function(first, second) {
    if(!first) {
      return second
    }
    if(!second) {
      return first
    }
    return function() {
      return first.apply(window, arguments) && second.apply(window, arguments)
    }
  };
  var getArr = function(i, arr) {
    var r = arr || [];
    if(i) {
      r.push(i)
    }
    return r
  };
  var _isElement = function(n) {
    return 1 == n.nodeType
  };
  var blank = "";
  var _getAttr = function(elem, attr) {
    if(!elem) {
      return blank
    }
    if(attr == "class") {
      return elem.className || blank
    }
    if(attr == "for") {
      return elem.htmlFor || blank
    }
    if(attr == "style") {
      return elem.style.cssText || blank
    }
    return(caseSensitive ? elem.getAttribute(attr) : elem.getAttribute(attr, 2)) || blank
  };
  var attrs = {"*=":function(attr, value) {
    return function(elem) {
      return _getAttr(elem, attr).indexOf(value) >= 0
    }
  }, "^=":function(attr, value) {
    return function(elem) {
      return _getAttr(elem, attr).indexOf(value) == 0
    }
  }, "$=":function(attr, value) {
    var tval = " " + value;
    return function(elem) {
      var ea = " " + _getAttr(elem, attr);
      return ea.lastIndexOf(value) == ea.length - value.length
    }
  }, "~=":function(attr, value) {
    var tval = " " + value + " ";
    return function(elem) {
      var ea = " " + _getAttr(elem, attr) + " ";
      return ea.indexOf(tval) >= 0
    }
  }, "|=":function(attr, value) {
    var valueDash = " " + value + "-";
    return function(elem) {
      var ea = " " + _getAttr(elem, attr);
      return ea == value || ea.indexOf(valueDash) == 0
    }
  }, "=":function(attr, value) {
    return function(elem) {
      return _getAttr(elem, attr) == value
    }
  }};
  var _noNES = typeof getDoc().firstChild.nextElementSibling == "undefined";
  var _ns = !_noNES ? "nextElementSibling" : "nextSibling";
  var _ps = !_noNES ? "previousElementSibling" : "previousSibling";
  var _simpleNodeTest = _noNES ? _isElement : yesman;
  var _lookLeft = function(node) {
    while(node = node[_ps]) {
      if(_simpleNodeTest(node)) {
        return false
      }
    }
    return true
  };
  var _lookRight = function(node) {
    while(node = node[_ns]) {
      if(_simpleNodeTest(node)) {
        return false
      }
    }
    return true
  };
  var getNodeIndex = function(node) {
    var root = node.parentNode;
    var i = 0, tret = root[childNodesName], ci = node["_i"] || -1, cl = root["_l"] || -1;
    if(!tret) {
      return-1
    }
    var l = tret.length;
    if(cl == l && ci >= 0 && cl >= 0) {
      return ci
    }
    root["_l"] = l;
    ci = -1;
    for(var te = root["firstElementChild"] || root["firstChild"];te;te = te[_ns]) {
      if(_simpleNodeTest(te)) {
        te["_i"] = ++i;
        if(node === te) {
          ci = i
        }
      }
    }
    return ci
  };
  var isEven = function(elem) {
    return!(getNodeIndex(elem) % 2)
  };
  var isOdd = function(elem) {
    return getNodeIndex(elem) % 2
  };
  var pseudos = {"checked":function(name, condition) {
    return function(elem) {
      return!!("checked" in elem ? elem.checked : elem.selected)
    }
  }, "first-child":function() {
    return _lookLeft
  }, "last-child":function() {
    return _lookRight
  }, "only-child":function(name, condition) {
    return function(node) {
      if(!_lookLeft(node)) {
        return false
      }
      if(!_lookRight(node)) {
        return false
      }
      return true
    }
  }, "empty":function(name, condition) {
    return function(elem) {
      var cn = elem.childNodes;
      var cnl = elem.childNodes.length;
      for(var x = cnl - 1;x >= 0;x--) {
        var nt = cn[x].nodeType;
        if(nt === 1 || nt == 3) {
          return false
        }
      }
      return true
    }
  }, "contains":function(name, condition) {
    var cz = condition.charAt(0);
    if(cz == '"' || cz == "'") {
      condition = condition.slice(1, -1)
    }
    return function(elem) {
      return elem.innerHTML.indexOf(condition) >= 0
    }
  }, "not":function(name, condition) {
    var p = getQueryParts(condition)[0];
    var ignores = {el:1};
    if(p.tag != "*") {
      ignores.tag = 1
    }
    if(!p.classes.length) {
      ignores.classes = 1
    }
    var ntf = getSimpleFilterFunc(p, ignores);
    return function(elem) {
      return!ntf(elem)
    }
  }, "nth-child":function(name, condition) {
    var pi = parseInt;
    if(condition == "odd") {
      return isOdd
    }else {
      if(condition == "even") {
        return isEven
      }
    }
    if(condition.indexOf("n") != -1) {
      var tparts = condition.split("n", 2);
      var pred = tparts[0] ? tparts[0] == "-" ? -1 : pi(tparts[0]) : 1;
      var idx = tparts[1] ? pi(tparts[1]) : 0;
      var lb = 0, ub = -1;
      if(pred > 0) {
        if(idx < 0) {
          idx = idx % pred && pred + idx % pred
        }else {
          if(idx > 0) {
            if(idx >= pred) {
              lb = idx - idx % pred
            }
            idx = idx % pred
          }
        }
      }else {
        if(pred < 0) {
          pred *= -1;
          if(idx > 0) {
            ub = idx;
            idx = idx % pred
          }
        }
      }
      if(pred > 0) {
        return function(elem) {
          var i = getNodeIndex(elem);
          return i >= lb && (ub < 0 || i <= ub) && i % pred == idx
        }
      }else {
        condition = idx
      }
    }
    var ncount = pi(condition);
    return function(elem) {
      return getNodeIndex(elem) == ncount
    }
  }};
  var defaultGetter = d.isIE ? function(cond) {
    var clc = cond.toLowerCase();
    if(clc == "class") {
      cond = "className"
    }
    return function(elem) {
      return caseSensitive ? elem.getAttribute(cond) : elem[cond] || elem[clc]
    }
  } : function(cond) {
    return function(elem) {
      return elem && elem.getAttribute && elem.hasAttribute(cond)
    }
  };
  var getSimpleFilterFunc = function(query, ignores) {
    if(!query) {
      return yesman
    }
    ignores = ignores || {};
    var ff = null;
    if(!("el" in ignores)) {
      ff = agree(ff, _isElement)
    }
    if(!("tag" in ignores)) {
      if(query.tag != "*") {
        ff = agree(ff, function(elem) {
          return elem && elem.tagName == query.getTag()
        })
      }
    }
    if(!("classes" in ignores)) {
      each(query.classes, function(cname, idx, arr) {
        var re = new RegExp("(?:^|\\s)" + cname + "(?:\\s|$)");
        ff = agree(ff, function(elem) {
          return re.test(elem.className)
        });
        ff.count = idx
      })
    }
    if(!("pseudos" in ignores)) {
      each(query.pseudos, function(pseudo) {
        var pn = pseudo.name;
        if(pseudos[pn]) {
          ff = agree(ff, pseudos[pn](pn, pseudo.value))
        }
      })
    }
    if(!("attrs" in ignores)) {
      each(query.attrs, function(attr) {
        var matcher;
        var a = attr.attr;
        if(attr.type && attrs[attr.type]) {
          matcher = attrs[attr.type](a, attr.matchFor)
        }else {
          if(a.length) {
            matcher = defaultGetter(a)
          }
        }
        if(matcher) {
          ff = agree(ff, matcher)
        }
      })
    }
    if(!("id" in ignores)) {
      if(query.id) {
        ff = agree(ff, function(elem) {
          return!!elem && elem.id == query.id
        })
      }
    }
    if(!ff) {
      if(!("default" in ignores)) {
        ff = yesman
      }
    }
    return ff
  };
  var _nextSibling = function(filterFunc) {
    return function(node, ret, bag) {
      while(node = node[_ns]) {
        if(_noNES && !_isElement(node)) {
          continue
        }
        if((!bag || _isUnique(node, bag)) && filterFunc(node)) {
          ret.push(node)
        }
        break
      }
      return ret
    }
  };
  var _nextSiblings = function(filterFunc) {
    return function(root, ret, bag) {
      var te = root[_ns];
      while(te) {
        if(_simpleNodeTest(te)) {
          if(bag && !_isUnique(te, bag)) {
            break
          }
          if(filterFunc(te)) {
            ret.push(te)
          }
        }
        te = te[_ns]
      }
      return ret
    }
  };
  var _childElements = function(filterFunc) {
    filterFunc = filterFunc || yesman;
    return function(root, ret, bag) {
      var te, x = 0, tret = root[childNodesName];
      while(te = tret[x++]) {
        if(_simpleNodeTest(te) && (!bag || _isUnique(te, bag)) && filterFunc(te, x)) {
          ret.push(te)
        }
      }
      return ret
    }
  };
  var _isDescendant = function(node, root) {
    var pn = node.parentNode;
    while(pn) {
      if(pn == root) {
        break
      }
      pn = pn.parentNode
    }
    return!!pn
  };
  var _getElementsFuncCache = {};
  var getElementsFunc = function(query) {
    var retFunc = _getElementsFuncCache[query.query];
    if(retFunc) {
      return retFunc
    }
    var io = query.infixOper;
    var oper = io ? io.oper : "";
    var filterFunc = getSimpleFilterFunc(query, {el:1});
    var qt = query.tag;
    var wildcardTag = "*" == qt;
    var ecs = getDoc()["getElementsByClassName"];
    if(!oper) {
      if(query.id) {
        filterFunc = !query.loops && wildcardTag ? yesman : getSimpleFilterFunc(query, {el:1, id:1});
        retFunc = function(root, arr) {
          var te = d.byId(query.id, root.ownerDocument || root);
          if(!te || !filterFunc(te)) {
            return
          }
          if(9 == root.nodeType) {
            return getArr(te, arr)
          }else {
            if(_isDescendant(te, root)) {
              return getArr(te, arr)
            }
          }
        }
      }else {
        if(ecs && /\{\s*\[native code\]\s*\}/.test(String(ecs)) && query.classes.length && !cssCaseBug) {
          filterFunc = getSimpleFilterFunc(query, {el:1, classes:1, id:1});
          var classesString = query.classes.join(" ");
          retFunc = function(root, arr, bag) {
            var ret = getArr(0, arr), te, x = 0;
            var tret = root.getElementsByClassName(classesString);
            while(te = tret[x++]) {
              if(filterFunc(te, root) && _isUnique(te, bag)) {
                ret.push(te)
              }
            }
            return ret
          }
        }else {
          if(!wildcardTag && !query.loops) {
            retFunc = function(root, arr, bag) {
              var ret = getArr(0, arr), te, x = 0;
              var tret = root.getElementsByTagName(query.getTag());
              while(te = tret[x++]) {
                if(_isUnique(te, bag)) {
                  ret.push(te)
                }
              }
              return ret
            }
          }else {
            filterFunc = getSimpleFilterFunc(query, {el:1, tag:1, id:1});
            retFunc = function(root, arr, bag) {
              var ret = getArr(0, arr), te, x = 0;
              var tret = root.getElementsByTagName(query.getTag());
              while(te = tret[x++]) {
                if(filterFunc(te, root) && _isUnique(te, bag)) {
                  ret.push(te)
                }
              }
              return ret
            }
          }
        }
      }
    }else {
      var skipFilters = {el:1};
      if(wildcardTag) {
        skipFilters.tag = 1
      }
      filterFunc = getSimpleFilterFunc(query, skipFilters);
      if("+" == oper) {
        retFunc = _nextSibling(filterFunc)
      }else {
        if("~" == oper) {
          retFunc = _nextSiblings(filterFunc)
        }else {
          if(">" == oper) {
            retFunc = _childElements(filterFunc)
          }
        }
      }
    }
    return _getElementsFuncCache[query.query] = retFunc
  };
  var filterDown = function(root, queryParts) {
    var candidates = getArr(root), qp, x, te, qpl = queryParts.length, bag, ret;
    for(var i = 0;i < qpl;i++) {
      ret = [];
      qp = queryParts[i];
      x = candidates.length - 1;
      if(x > 0) {
        bag = {};
        ret.nozip = true
      }
      var gef = getElementsFunc(qp);
      for(var j = 0;te = candidates[j];j++) {
        gef(te, ret, bag)
      }
      if(!ret.length) {
        break
      }
      candidates = ret
    }
    return ret
  };
  var _queryFuncCacheDOM = {}, _queryFuncCacheQSA = {};
  var getStepQueryFunc = function(query) {
    var qparts = getQueryParts(trim(query));
    if(qparts.length == 1) {
      var tef = getElementsFunc(qparts[0]);
      return function(root) {
        var r = tef(root, new qlc);
        if(r) {
          r.nozip = true
        }
        return r
      }
    }
    return function(root) {
      return filterDown(root, qparts)
    }
  };
  var nua = navigator.userAgent;
  var wk = "WebKit/";
  var is525 = d.isWebKit && nua.indexOf(wk) > 0 && parseFloat(nua.split(wk)[1]) > 528;
  var noZip = d.isIE ? "commentStrip" : "nozip";
  var qsa = "querySelectorAll";
  var qsaAvail = !!getDoc()[qsa] && (!d.isSafari || d.isSafari > 3.1 || is525);
  var infixSpaceRe = /n\+\d|([^ ])?([>~+])([^ =])?/g;
  var infixSpaceFunc = function(match, pre, ch, post) {
    return ch ? (pre ? pre + " " : "") + ch + (post ? " " + post : "") : match
  };
  var getQueryFunc = function(query, forceDOM) {
    query = query.replace(infixSpaceRe, infixSpaceFunc);
    if(qsaAvail) {
      var qsaCached = _queryFuncCacheQSA[query];
      if(qsaCached && !forceDOM) {
        return qsaCached
      }
    }
    var domCached = _queryFuncCacheDOM[query];
    if(domCached) {
      return domCached
    }
    var qcz = query.charAt(0);
    var nospace = -1 == query.indexOf(" ");
    if(query.indexOf("#") >= 0 && nospace) {
      forceDOM = true
    }
    var useQSA = qsaAvail && !forceDOM && specials.indexOf(qcz) == -1 && (!d.isIE || query.indexOf(":") == -1) && !(cssCaseBug && query.indexOf(".") >= 0) && query.indexOf(":contains") == -1 && query.indexOf(":checked") == -1 && query.indexOf("|=") == -1;
    if(useQSA) {
      var tq = specials.indexOf(query.charAt(query.length - 1)) >= 0 ? query + " *" : query;
      return _queryFuncCacheQSA[query] = function(root) {
        try {
          if(!(9 == root.nodeType || nospace)) {
            throw"";
          }
          var r = root[qsa](tq);
          r[noZip] = true;
          return r
        }catch(e) {
          return getQueryFunc(query, true)(root)
        }
      }
    }else {
      var parts = query.split(/\s*,\s*/);
      return _queryFuncCacheDOM[query] = parts.length < 2 ? getStepQueryFunc(query) : function(root) {
        var pindex = 0, ret = [], tp;
        while(tp = parts[pindex++]) {
          ret = ret.concat(getStepQueryFunc(tp)(root))
        }
        return ret
      }
    }
  };
  var _zipIdx = 0;
  var _nodeUID = d.isIE ? function(node) {
    if(caseSensitive) {
      return node.getAttribute("_uid") || node.setAttribute("_uid", ++_zipIdx) || _zipIdx
    }else {
      return node.uniqueID
    }
  } : function(node) {
    return node._uid || (node._uid = ++_zipIdx)
  };
  var _isUnique = function(node, bag) {
    if(!bag) {
      return 1
    }
    var id = _nodeUID(node);
    if(!bag[id]) {
      return bag[id] = 1
    }
    return 0
  };
  var _zipIdxName = "_zipIdx";
  var _zip = function(arr) {
    if(arr && arr.nozip) {
      return qlc._wrap ? qlc._wrap(arr) : arr
    }
    var ret = new qlc;
    if(!arr || !arr.length) {
      return ret
    }
    if(arr[0]) {
      ret.push(arr[0])
    }
    if(arr.length < 2) {
      return ret
    }
    _zipIdx++;
    if(d.isIE && caseSensitive) {
      var szidx = _zipIdx + "";
      arr[0].setAttribute(_zipIdxName, szidx);
      for(var x = 1, te;te = arr[x];x++) {
        if(arr[x].getAttribute(_zipIdxName) != szidx) {
          ret.push(te)
        }
        te.setAttribute(_zipIdxName, szidx)
      }
    }else {
      if(d.isIE && arr.commentStrip) {
        try {
          for(var x = 1, te;te = arr[x];x++) {
            if(_isElement(te)) {
              ret.push(te)
            }
          }
        }catch(e) {
        }
      }else {
        if(arr[0]) {
          arr[0][_zipIdxName] = _zipIdx
        }
        for(var x = 1, te;te = arr[x];x++) {
          if(arr[x][_zipIdxName] != _zipIdx) {
            ret.push(te)
          }
          te[_zipIdxName] = _zipIdx
        }
      }
    }
    return ret
  };
  d.query = function(query, root) {
    qlc = d._NodeListCtor;
    if(!query) {
      return new qlc
    }
    if(query.constructor == qlc) {
      return query
    }
    if(typeof query != "string") {
      return new qlc(query)
    }
    if(typeof root == "string") {
      root = d.byId(root);
      if(!root) {
        return new qlc
      }
    }
    root = root || getDoc();
    var od = root.ownerDocument || root.documentElement;
    caseSensitive = root.contentType && root.contentType == "application/xml" || d.isOpera && (root.doctype || od.toString() == "[object XMLDocument]") || !!od && (d.isIE ? od.xml : root.xmlVersion || od.xmlVersion);
    var r = getQueryFunc(query)(root);
    if(r && r.nozip && !qlc._wrap) {
      return r
    }
    return _zip(r)
  };
  d.query.pseudos = pseudos;
  d._filterQueryResult = function(nodeList, simpleFilter) {
    var tmpNodeList = new d._NodeListCtor;
    var filterFunc = getSimpleFilterFunc(getQueryParts(simpleFilter)[0]);
    for(var x = 0, te;te = nodeList[x];x++) {
      if(filterFunc(te)) {
        tmpNodeList.push(te)
      }
    }
    return tmpNodeList
  }
})(this["queryPortability"] || this["acme"] || midojo);
midojo.provide("midojo._base.xhr");
(function() {
  var _d = midojo, cfg = _d.config;
  function setValue(obj, name, value) {
    if(value === null) {
      return
    }
    var val = obj[name];
    if(typeof val == "string") {
      obj[name] = [val, value]
    }else {
      if(_d.isArray(val)) {
        val.push(value)
      }else {
        obj[name] = value
      }
    }
  }
  midojo.fieldToObject = function(inputNode) {
    var ret = null;
    var item = _d.byId(inputNode);
    if(item) {
      var _in = item.name;
      var type = (item.type || "").toLowerCase();
      if(_in && type && !item.disabled) {
        if(type == "radio" || type == "checkbox") {
          if(item.checked) {
            ret = item.value
          }
        }else {
          if(item.multiple) {
            ret = [];
            _d.query("option", item).forEach(function(opt) {
              if(opt.selected) {
                ret.push(opt.value)
              }
            })
          }else {
            ret = item.value
          }
        }
      }
    }
    return ret
  };
  midojo.formToObject = function(formNode) {
    var ret = {};
    var exclude = "file|submit|image|reset|button|";
    _d.forEach(midojo.byId(formNode).elements, function(item) {
      var _in = item.name;
      var type = (item.type || "").toLowerCase();
      if(_in && type && exclude.indexOf(type) == -1 && !item.disabled) {
        setValue(ret, _in, _d.fieldToObject(item));
        if(type == "image") {
          ret[_in + ".x"] = ret[_in + ".y"] = ret[_in].x = ret[_in].y = 0
        }
      }
    });
    return ret
  };
  midojo.objectToQuery = function(map) {
    var enc = encodeURIComponent;
    var pairs = [];
    var backstop = {};
    for(var name in map) {
      var value = map[name];
      if(value != backstop[name]) {
        var assign = enc(name) + "=";
        if(_d.isArray(value)) {
          for(var i = 0;i < value.length;i++) {
            pairs.push(assign + enc(value[i]))
          }
        }else {
          pairs.push(assign + enc(value))
        }
      }
    }
    return pairs.join("&")
  };
  midojo.formToQuery = function(formNode) {
    return _d.objectToQuery(_d.formToObject(formNode))
  };
  midojo.formToJson = function(formNode, prettyPrint) {
    return _d.toJson(_d.formToObject(formNode), prettyPrint)
  };
  midojo.queryToObject = function(str) {
    var ret = {};
    var qp = str.split("&");
    var dec = decodeURIComponent;
    _d.forEach(qp, function(item) {
      if(item.length) {
        var parts = item.split("=");
        var name = dec(parts.shift());
        var val = dec(parts.join("="));
        if(typeof ret[name] == "string") {
          ret[name] = [ret[name]]
        }
        if(_d.isArray(ret[name])) {
          ret[name].push(val)
        }else {
          ret[name] = val
        }
      }
    });
    return ret
  };
  midojo._blockAsync = false;
  var handlers = _d._contentHandlers = midojo.contentHandlers = {text:function(xhr) {
    return xhr.responseText
  }, json:function(xhr) {
    return _d.fromJson(xhr.responseText || null)
  }, "json-comment-filtered":function(xhr) {
    if(!midojo.config.useCommentedJson) {
      console.warn("Consider using the standard mimetype:application/json." + " json-commenting can introduce security issues. To" + " decrease the chances of hijacking, use the standard the 'json' handler and" + " prefix your json with: {}&&\n" + "Use djConfig.useCommentedJson=true to turn off this message.")
    }
    var value = xhr.responseText;
    var cStartIdx = value.indexOf("/*");
    var cEndIdx = value.lastIndexOf("*/");
    if(cStartIdx == -1 || cEndIdx == -1) {
      throw new Error("JSON was not comment filtered");
    }
    return _d.fromJson(value.substring(cStartIdx + 2, cEndIdx))
  }, javascript:function(xhr) {
    return _d.eval(xhr.responseText)
  }, xml:function(xhr) {
    var result = xhr.responseXML;
    if(_d.isIE && (!result || !result.documentElement)) {
      var ms = function(n) {
        return"MSXML" + n + ".DOMDocument"
      };
      var dp = ["Microsoft.XMLDOM", ms(6), ms(4), ms(3), ms(2)];
      _d.some(dp, function(p) {
        try {
          var dom = new ActiveXObject(p);
          dom.async = false;
          dom.loadXML(xhr.responseText);
          result = dom
        }catch(e) {
          return false
        }
        return true
      })
    }
    return result
  }, "json-comment-optional":function(xhr) {
    if(xhr.responseText && /^[^{\[]*\/\*/.test(xhr.responseText)) {
      return handlers["json-comment-filtered"](xhr)
    }else {
      return handlers["json"](xhr)
    }
  }};
  midojo._ioSetArgs = function(args, canceller, okHandler, errHandler) {
    var ioArgs = {args:args, url:args.url};
    var formObject = null;
    if(args.form) {
      var form = _d.byId(args.form);
      var actnNode = form.getAttributeNode("action");
      ioArgs.url = ioArgs.url || (actnNode ? actnNode.value : null);
      formObject = _d.formToObject(form)
    }
    var miArgs = [{}];
    if(formObject) {
      miArgs.push(formObject)
    }
    if(args.content) {
      miArgs.push(args.content)
    }
    if(args.preventCache) {
      miArgs.push({"midojo.preventCache":(new Date).valueOf()})
    }
    ioArgs.query = _d.objectToQuery(_d.mixin.apply(null, miArgs));
    ioArgs.handleAs = args.handleAs || "text";
    var d = new _d.Deferred(canceller);
    d.addCallbacks(okHandler, function(error) {
      return errHandler(error, d)
    });
    var ld = args.load;
    if(ld && _d.isFunction(ld)) {
      d.addCallback(function(value) {
        return ld.call(args, value, ioArgs)
      })
    }
    var err = args.error;
    if(err && _d.isFunction(err)) {
      d.addErrback(function(value) {
        return err.call(args, value, ioArgs)
      })
    }
    var handle = args.handle;
    if(handle && _d.isFunction(handle)) {
      d.addBoth(function(value) {
        return handle.call(args, value, ioArgs)
      })
    }
    if(cfg.ioPublish && _d.publish && ioArgs.args.ioPublish !== false) {
      d.addCallbacks(function(res) {
        _d.publish("/midojo/io/load", [d, res]);
        return res
      }, function(res) {
        _d.publish("/midojo/io/error", [d, res]);
        return res
      });
      d.addBoth(function(res) {
        _d.publish("/midojo/io/done", [d, res]);
        return res
      })
    }
    d.ioArgs = ioArgs;
    return d
  };
  var _deferredCancel = function(dfd) {
    dfd.canceled = true;
    var xhr = dfd.ioArgs.xhr;
    var _at = typeof xhr.abort;
    if(_at == "function" || _at == "object" || _at == "unknown") {
      xhr.abort()
    }
    var err = dfd.ioArgs.error;
    if(!err) {
      err = new Error("xhr cancelled");
      err.dojoType = "cancel"
    }
    return err
  };
  var _deferredOk = function(dfd) {
    var ret = handlers[dfd.ioArgs.handleAs](dfd.ioArgs.xhr);
    return ret === undefined ? null : ret
  };
  var _deferError = function(error, dfd) {
    if(!dfd.ioArgs.args.failOk) {
      console.error(error)
    }
    return error
  };
  var _inFlightIntvl = null;
  var _inFlight = [];
  var _pubCount = 0;
  var _checkPubCount = function(dfd) {
    if(_pubCount <= 0) {
      _pubCount = 0;
      if(cfg.ioPublish && _d.publish && (!dfd || dfd && dfd.ioArgs.args.ioPublish !== false)) {
        _d.publish("/midojo/io/stop")
      }
    }
  };
  var _watchInFlight = function() {
    var now = (new Date).getTime();
    if(!_d._blockAsync) {
      for(var i = 0, tif;i < _inFlight.length && (tif = _inFlight[i]);i++) {
        var dfd = tif.dfd;
        var func = function() {
          if(!dfd || dfd.canceled || !tif.validCheck(dfd)) {
            _inFlight.splice(i--, 1);
            _pubCount -= 1
          }else {
            if(tif.ioCheck(dfd)) {
              _inFlight.splice(i--, 1);
              tif.resHandle(dfd);
              _pubCount -= 1
            }else {
              if(dfd.startTime) {
                if(dfd.startTime + (dfd.ioArgs.args.timeout || 0) < now) {
                  _inFlight.splice(i--, 1);
                  var err = new Error("timeout exceeded");
                  err.dojoType = "timeout";
                  dfd.errback(err);
                  dfd.cancel();
                  _pubCount -= 1
                }
              }
            }
          }
        };
        if(midojo.config.debugAtAllCosts) {
          func.call(this)
        }else {
          try {
            func.call(this)
          }catch(e) {
            dfd.errback(e)
          }
        }
      }
    }
    _checkPubCount(dfd);
    if(!_inFlight.length) {
      clearInterval(_inFlightIntvl);
      _inFlightIntvl = null;
      return
    }
  };
  midojo._ioCancelAll = function() {
    try {
      _d.forEach(_inFlight, function(i) {
        try {
          i.dfd.cancel()
        }catch(e) {
        }
      })
    }catch(e) {
    }
  };
  if(_d.isIE) {
    _d.addOnWindowUnload(_d._ioCancelAll)
  }
  _d._ioNotifyStart = function(dfd) {
    if(cfg.ioPublish && _d.publish && dfd.ioArgs.args.ioPublish !== false) {
      if(!_pubCount) {
        _d.publish("/midojo/io/start")
      }
      _pubCount += 1;
      _d.publish("/midojo/io/send", [dfd])
    }
  };
  _d._ioWatch = function(dfd, validCheck, ioCheck, resHandle) {
    var args = dfd.ioArgs.args;
    if(args.timeout) {
      dfd.startTime = (new Date).getTime()
    }
    _inFlight.push({dfd:dfd, validCheck:validCheck, ioCheck:ioCheck, resHandle:resHandle});
    if(!_inFlightIntvl) {
      _inFlightIntvl = setInterval(_watchInFlight, 50)
    }
    if(args.sync) {
      _watchInFlight()
    }
  };
  var _defaultContentType = "application/x-www-form-urlencoded";
  var _validCheck = function(dfd) {
    return dfd.ioArgs.xhr.readyState
  };
  var _ioCheck = function(dfd) {
    return 4 == dfd.ioArgs.xhr.readyState
  };
  var _resHandle = function(dfd) {
    var xhr = dfd.ioArgs.xhr;
    if(_d._isDocumentOk(xhr)) {
      dfd.callback(dfd)
    }else {
      var err = new Error("Unable to load " + dfd.ioArgs.url + " status:" + xhr.status);
      err.status = xhr.status;
      err.responseText = xhr.responseText;
      dfd.errback(err)
    }
  };
  midojo._ioAddQueryToUrl = function(ioArgs) {
    if(ioArgs.query.length) {
      ioArgs.url += (ioArgs.url.indexOf("?") == -1 ? "?" : "&") + ioArgs.query;
      ioArgs.query = null
    }
  };
  midojo.xhr = function(method, args, hasBody) {
    var dfd = _d._ioSetArgs(args, _deferredCancel, _deferredOk, _deferError);
    var ioArgs = dfd.ioArgs;
    var xhr = ioArgs.xhr = _d._xhrObj(ioArgs.args);
    if(!xhr) {
      dfd.cancel();
      return dfd
    }
    if("postData" in args) {
      ioArgs.query = args.postData
    }else {
      if("putData" in args) {
        ioArgs.query = args.putData
      }else {
        if("rawBody" in args) {
          ioArgs.query = args.rawBody
        }else {
          if(arguments.length > 2 && !hasBody || "POST|PUT".indexOf(method.toUpperCase()) == -1) {
            _d._ioAddQueryToUrl(ioArgs)
          }
        }
      }
    }
    xhr.open(method, ioArgs.url, args.sync !== true, args.user || undefined, args.password || undefined);
    if(args.headers) {
      for(var hdr in args.headers) {
        if(hdr.toLowerCase() === "content-type" && !args.contentType) {
          args.contentType = args.headers[hdr]
        }else {
          if(args.headers[hdr]) {
            xhr.setRequestHeader(hdr, args.headers[hdr])
          }
        }
      }
    }
    xhr.setRequestHeader("Content-Type", args.contentType || _defaultContentType);
    if(!args.headers || !("X-Requested-With" in args.headers)) {
      xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest")
    }
    _d._ioNotifyStart(dfd);
    if(midojo.config.debugAtAllCosts) {
      xhr.send(ioArgs.query)
    }else {
      try {
        xhr.send(ioArgs.query)
      }catch(e) {
        ioArgs.error = e;
        dfd.cancel()
      }
    }
    _d._ioWatch(dfd, _validCheck, _ioCheck, _resHandle);
    xhr = null;
    return dfd
  };
  midojo.xhrGet = function(args) {
    return _d.xhr("GET", args)
  };
  midojo.rawXhrPost = midojo.xhrPost = function(args) {
    return _d.xhr("POST", args, true)
  };
  midojo.rawXhrPut = midojo.xhrPut = function(args) {
    return _d.xhr("PUT", args, true)
  };
  midojo.xhrDelete = function(args) {
    return _d.xhr("DELETE", args)
  }
})();
midojo.provide("midojo._base.fx");
(function() {
  var d = midojo;
  var _mixin = d._mixin;
  midojo._Line = function(start, end) {
    this.start = start;
    this.end = end
  };
  midojo._Line.prototype.getValue = function(n) {
    return(this.end - this.start) * n + this.start
  };
  midojo.Animation = function(args) {
    _mixin(this, args);
    if(d.isArray(this.curve)) {
      this.curve = new d._Line(this.curve[0], this.curve[1])
    }
  };
  d._Animation = d.Animation;
  d.extend(midojo.Animation, {duration:350, repeat:0, rate:20, _percent:0, _startRepeatCount:0, _getStep:function() {
    var _p = this._percent, _e = this.easing;
    return _e ? _e(_p) : _p
  }, _fire:function(evt, args) {
    var a = args || [];
    if(this[evt]) {
      if(d.config.debugAtAllCosts) {
        this[evt].apply(this, a)
      }else {
        try {
          this[evt].apply(this, a)
        }catch(e) {
          console.error("exception in animation handler for:", evt);
          console.error(e)
        }
      }
    }
    return this
  }, play:function(delay, gotoStart) {
    var _t = this;
    if(_t._delayTimer) {
      _t._clearTimer()
    }
    if(gotoStart) {
      _t._stopTimer();
      _t._active = _t._paused = false;
      _t._percent = 0
    }else {
      if(_t._active && !_t._paused) {
        return _t
      }
    }
    _t._fire("beforeBegin", [_t.node]);
    var de = delay || _t.delay, _p = midojo.hitch(_t, "_play", gotoStart);
    if(de > 0) {
      _t._delayTimer = setTimeout(_p, de);
      return _t
    }
    _p();
    return _t
  }, _play:function(gotoStart) {
    var _t = this;
    if(_t._delayTimer) {
      _t._clearTimer()
    }
    _t._startTime = (new Date).valueOf();
    if(_t._paused) {
      _t._startTime -= _t.duration * _t._percent
    }
    _t._endTime = _t._startTime + _t.duration;
    _t._active = true;
    _t._paused = false;
    var value = _t.curve.getValue(_t._getStep());
    if(!_t._percent) {
      if(!_t._startRepeatCount) {
        _t._startRepeatCount = _t.repeat
      }
      _t._fire("onBegin", [value])
    }
    _t._fire("onPlay", [value]);
    _t._cycle();
    return _t
  }, pause:function() {
    var _t = this;
    if(_t._delayTimer) {
      _t._clearTimer()
    }
    _t._stopTimer();
    if(!_t._active) {
      return _t
    }
    _t._paused = true;
    _t._fire("onPause", [_t.curve.getValue(_t._getStep())]);
    return _t
  }, gotoPercent:function(percent, andPlay) {
    var _t = this;
    _t._stopTimer();
    _t._active = _t._paused = true;
    _t._percent = percent;
    if(andPlay) {
      _t.play()
    }
    return _t
  }, stop:function(gotoEnd) {
    var _t = this;
    if(_t._delayTimer) {
      _t._clearTimer()
    }
    if(!_t._timer) {
      return _t
    }
    _t._stopTimer();
    if(gotoEnd) {
      _t._percent = 1
    }
    _t._fire("onStop", [_t.curve.getValue(_t._getStep())]);
    _t._active = _t._paused = false;
    return _t
  }, status:function() {
    if(this._active) {
      return this._paused ? "paused" : "playing"
    }
    return"stopped"
  }, _cycle:function() {
    var _t = this;
    if(_t._active) {
      var curr = (new Date).valueOf();
      var step = (curr - _t._startTime) / (_t._endTime - _t._startTime);
      if(step >= 1) {
        step = 1
      }
      _t._percent = step;
      if(_t.easing) {
        step = _t.easing(step)
      }
      _t._fire("onAnimate", [_t.curve.getValue(step)]);
      if(_t._percent < 1) {
        _t._startTimer()
      }else {
        _t._active = false;
        if(_t.repeat > 0) {
          _t.repeat--;
          _t.play(null, true)
        }else {
          if(_t.repeat == -1) {
            _t.play(null, true)
          }else {
            if(_t._startRepeatCount) {
              _t.repeat = _t._startRepeatCount;
              _t._startRepeatCount = 0
            }
          }
        }
        _t._percent = 0;
        _t._fire("onEnd", [_t.node]);
        !_t.repeat && _t._stopTimer()
      }
    }
    return _t
  }, _clearTimer:function() {
    clearTimeout(this._delayTimer);
    delete this._delayTimer
  }});
  var ctr = 0, _globalTimerList = [], timer = null, runner = {run:function() {
  }};
  d.extend(d.Animation, {_startTimer:function() {
    if(!this._timer) {
      this._timer = d.connect(runner, "run", this, "_cycle");
      ctr++
    }
    if(!timer) {
      timer = setInterval(d.hitch(runner, "run"), this.rate)
    }
  }, _stopTimer:function() {
    if(this._timer) {
      d.disconnect(this._timer);
      this._timer = null;
      ctr--
    }
    if(ctr <= 0) {
      clearInterval(timer);
      timer = null;
      ctr = 0
    }
  }});
  var _makeFadeable = d.isIE ? function(node) {
    var ns = node.style;
    if(!ns.width.length && d.style(node, "width") == "auto") {
      ns.width = "auto"
    }
  } : function() {
  };
  midojo._fade = function(args) {
    args.node = d.byId(args.node);
    var fArgs = _mixin({properties:{}}, args), props = fArgs.properties.opacity = {};
    props.start = !("start" in fArgs) ? function() {
      return+d.style(fArgs.node, "opacity") || 0
    } : fArgs.start;
    props.end = fArgs.end;
    var anim = d.animateProperty(fArgs);
    d.connect(anim, "beforeBegin", d.partial(_makeFadeable, fArgs.node));
    return anim
  };
  midojo.fadeIn = function(args) {
    return d._fade(_mixin({end:1}, args))
  };
  midojo.fadeOut = function(args) {
    return d._fade(_mixin({end:0}, args))
  };
  midojo._defaultEasing = function(n) {
    return 0.5 + Math.sin((n + 1.5) * Math.PI) / 2
  };
  var PropLine = function(properties) {
    this._properties = properties;
    for(var p in properties) {
      var prop = properties[p];
      if(prop.start instanceof d.Color) {
        prop.tempColor = new d.Color
      }
    }
  };
  PropLine.prototype.getValue = function(r) {
    var ret = {};
    for(var p in this._properties) {
      var prop = this._properties[p], start = prop.start;
      if(start instanceof d.Color) {
        ret[p] = d.blendColors(start, prop.end, r, prop.tempColor).toCss()
      }else {
        if(!d.isArray(start)) {
          ret[p] = (prop.end - start) * r + start + (p != "opacity" ? prop.units || "px" : 0)
        }
      }
    }
    return ret
  };
  midojo.animateProperty = function(args) {
    var n = args.node = d.byId(args.node);
    if(!args.easing) {
      args.easing = d._defaultEasing
    }
    var anim = new d.Animation(args);
    d.connect(anim, "beforeBegin", anim, function() {
      var pm = {};
      for(var p in this.properties) {
        if(p == "width" || p == "height") {
          this.node.display = "block"
        }
        var prop = this.properties[p];
        if(d.isFunction(prop)) {
          prop = prop(n)
        }
        prop = pm[p] = _mixin({}, d.isObject(prop) ? prop : {end:prop});
        if(d.isFunction(prop.start)) {
          prop.start = prop.start(n)
        }
        if(d.isFunction(prop.end)) {
          prop.end = prop.end(n)
        }
        var isColor = p.toLowerCase().indexOf("color") >= 0;
        function getStyle(node, p) {
          var v = {height:node.offsetHeight, width:node.offsetWidth}[p];
          if(v !== undefined) {
            return v
          }
          v = d.style(node, p);
          return p == "opacity" ? +v : isColor ? v : parseFloat(v)
        }
        if(!("end" in prop)) {
          prop.end = getStyle(n, p)
        }else {
          if(!("start" in prop)) {
            prop.start = getStyle(n, p)
          }
        }
        if(isColor) {
          prop.start = new d.Color(prop.start);
          prop.end = new d.Color(prop.end)
        }else {
          prop.start = p == "opacity" ? +prop.start : parseFloat(prop.start)
        }
      }
      this.curve = new PropLine(pm)
    });
    d.connect(anim, "onAnimate", d.hitch(d, "style", anim.node));
    return anim
  };
  midojo.anim = function(node, properties, duration, easing, onEnd, delay) {
    return d.animateProperty({node:node, duration:duration || d.Animation.prototype.duration, properties:properties, easing:easing, onEnd:onEnd}).play(delay || 0)
  }
})();
midojo.forEach(midojo.config.require, function(i$$10) {
  midojo["require"](i$$10)
});
midojo.provide("midojo.i18n");
midojo.i18n.getLocalization = function(packageName, bundleName$$1, locale$$1) {
  locale$$1 = midojo.i18n.normalizeLocale(locale$$1);
  var elements = locale$$1.split("-");
  var module$$6 = [packageName, "nls", bundleName$$1].join(".");
  var bundle = midojo._loadedModules[module$$6];
  if(bundle) {
    var localization;
    var i$$11 = elements.length;
    for(;i$$11 > 0;i$$11--) {
      var loc$$1 = elements.slice(0, i$$11).join("_");
      if(bundle[loc$$1]) {
        localization = bundle[loc$$1];
        break
      }
    }
    if(!localization) {
      localization = bundle.ROOT
    }
    if(localization) {
      var clazz = function() {
      };
      clazz.prototype = localization;
      return new clazz
    }
  }
  throw new Error("Bundle not found: " + bundleName$$1 + " in " + packageName + " , locale=" + locale$$1);
};
midojo.i18n.normalizeLocale = function(locale$$2) {
  var result$$1 = locale$$2 ? locale$$2.toLowerCase() : midojo.locale;
  if(result$$1 == "root") {
    result$$1 = "ROOT"
  }
  return result$$1
};
midojo.i18n._requireLocalization = function(moduleName$$3, bundleName$$2, locale$$3, availableFlatLocales$$1) {
  var targetLocale = midojo.i18n.normalizeLocale(locale$$3);
  var bundlePackage = [moduleName$$3, "nls", bundleName$$2].join(".");
  var bestLocale = "";
  if(availableFlatLocales$$1) {
    var flatLocales = availableFlatLocales$$1.split(",");
    var i$$12 = 0;
    for(;i$$12 < flatLocales.length;i$$12++) {
      if(targetLocale["indexOf"](flatLocales[i$$12]) == 0) {
        if(flatLocales[i$$12].length > bestLocale.length) {
          bestLocale = flatLocales[i$$12]
        }
      }
    }
    if(!bestLocale) {
      bestLocale = "ROOT"
    }
  }
  var tempLocale = availableFlatLocales$$1 ? bestLocale : targetLocale;
  var bundle$$1 = midojo._loadedModules[bundlePackage];
  var localizedBundle = null;
  if(bundle$$1) {
    if(midojo.config.localizationComplete && bundle$$1._built) {
      return
    }
    var jsLoc = tempLocale.replace(/-/g, "_");
    var translationPackage = bundlePackage + "." + jsLoc;
    localizedBundle = midojo._loadedModules[translationPackage]
  }
  if(!localizedBundle) {
    bundle$$1 = midojo["provide"](bundlePackage);
    var syms$$1 = midojo._getModuleSymbols(moduleName$$3);
    var modpath = syms$$1.concat("nls").join("/");
    var parent;
    midojo.i18n._searchLocalePath(tempLocale, availableFlatLocales$$1, function(loc$$2) {
      var jsLoc$$1 = loc$$2.replace(/-/g, "_");
      var translationPackage$$1 = bundlePackage + "." + jsLoc$$1;
      var loaded = false;
      if(!midojo._loadedModules[translationPackage$$1]) {
        midojo["provide"](translationPackage$$1);
        var module$$7 = [modpath];
        if(loc$$2 != "ROOT") {
          module$$7.push(loc$$2)
        }
        module$$7.push(bundleName$$2);
        var filespec = module$$7.join("/") + ".js";
        loaded = midojo._loadPath(filespec, null, function(hash) {
          var clazz$$1 = function() {
          };
          clazz$$1.prototype = parent;
          bundle$$1[jsLoc$$1] = new clazz$$1;
          var j$$1;
          for(j$$1 in hash) {
            bundle$$1[jsLoc$$1][j$$1] = hash[j$$1]
          }
        })
      }else {
        loaded = true
      }
      if(loaded && bundle$$1[jsLoc$$1]) {
        parent = bundle$$1[jsLoc$$1]
      }else {
        bundle$$1[jsLoc$$1] = parent
      }
      if(availableFlatLocales$$1) {
        return true
      }
    })
  }
  if(availableFlatLocales$$1 && targetLocale != bestLocale) {
    bundle$$1[targetLocale.replace(/-/g, "_")] = bundle$$1[bestLocale.replace(/-/g, "_")]
  }
};
(function() {
  var extra = midojo.config.extraLocale;
  if(extra) {
    if(!extra instanceof Array) {
      extra = [extra]
    }
    var req = midojo.i18n._requireLocalization;
    midojo.i18n._requireLocalization = function(m$$1, b, locale$$4, availableFlatLocales$$2) {
      req(m$$1, b, locale$$4, availableFlatLocales$$2);
      if(locale$$4) {
        return
      }
      var i$$13 = 0;
      for(;i$$13 < extra.length;i$$13++) {
        req(m$$1, b, extra[i$$13], availableFlatLocales$$2)
      }
    }
  }
})();
midojo.i18n._searchLocalePath = function(locale$$5, down, searchFunc) {
  locale$$5 = midojo.i18n.normalizeLocale(locale$$5);
  var elements$$1 = locale$$5.split("-");
  var searchlist = [];
  var i$$14 = elements$$1.length;
  for(;i$$14 > 0;i$$14--) {
    searchlist.push(elements$$1.slice(0, i$$14).join("-"))
  }
  searchlist.push(false);
  if(down) {
    searchlist.reverse()
  }
  var j$$2 = searchlist.length - 1;
  for(;j$$2 >= 0;j$$2--) {
    var loc$$3 = searchlist[j$$2] || "ROOT";
    var stop = searchFunc(loc$$3);
    if(stop) {
      break
    }
  }
};
midojo.i18n._preloadLocalizations = function(bundlePrefix, localesGenerated) {
  function preload(locale$$6) {
    locale$$6 = midojo.i18n.normalizeLocale(locale$$6);
    midojo.i18n._searchLocalePath(locale$$6, true, function(loc$$4) {
      var i$$16 = 0;
      for(;i$$16 < localesGenerated.length;i$$16++) {
        if(localesGenerated[i$$16] == loc$$4) {
          midojo["require"](bundlePrefix + "_" + loc$$4);
          return true
        }
      }
      return false
    })
  }
  preload();
  var extra$$1 = midojo.config.extraLocale || [];
  var i$$15 = 0;
  for(;i$$15 < extra$$1.length;i$$15++) {
    preload(extra$$1[i$$15])
  }
};
midojo.provide("ria.Contract");
midojo.provide("ria.Exception");
midojo.declare("ria.Exception", null, {message:null, constructor:function(message) {
  this.message = message
}, toString:function() {
  var msg = null;
  if(this.message && midojo.isString(this.message)) {
    msg = this.message
  }else {
    msg = this.declaredClass;
    if(msg.indexOf("ria.") === 0) {
      msg = msg.substring("ria.".length)
    }
  }
  return msg
}});
midojo.provide("ria.IllegalArgumentException");
midojo.declare("ria.IllegalArgumentException", ria.Exception, {});
midojo.declare("ria.Contract", null, {});
ria.Contract.pre = function(condition$$1, message) {
  if(!condition$$1) {
    throw new ria.IllegalArgumentException(message);
  }
};
midojo.provide("ria.Environment");
midojo.declare("ria.Environment", null, {getOrigin:function() {
  if(location.origin) {
    return location.origin
  }
  var origin = location.protocol + "//" + location.hostname;
  if(location.port && location.port !== "80") {
    origin += ":" + location.port
  }
  return origin
}});
midojo.provide("ria.Exception");
midojo.declare("ria.Exception", null, {message:null, constructor:function(message$$1) {
  this.message = message$$1
}, toString:function() {
  var msg = null;
  if(this.message && midojo.isString(this.message)) {
    msg = this.message
  }else {
    msg = this.declaredClass;
    if(msg.indexOf("ria.") === 0) {
      msg = msg.substring("ria.".length)
    }
  }
  return msg
}});
midojo.provide("ria.IllegalArgumentException");
midojo.declare("ria.IllegalArgumentException", ria.Exception, {});
midojo.provide("ria.RestService");
midojo.provide("ria.Environment");
midojo.declare("ria.Environment", null, {getOrigin:function() {
  if(location.origin) {
    return location.origin
  }
  var origin = location.protocol + "//" + location.hostname;
  if(location.port && location.port !== "80") {
    origin += ":" + location.port
  }
  return origin
}});
midojo.provide("midojox.io.xhrPlugins");
midojo.provide("midojo.AdapterRegistry");
midojo.AdapterRegistry = function(returnWrappers) {
  this.pairs = [];
  this.returnWrappers = returnWrappers || false
};
midojo.extend(midojo.AdapterRegistry, {register:function(name, check, wrap, directReturn, override) {
  this.pairs[override ? "unshift" : "push"]([name, check, wrap, directReturn])
}, match:function() {
  for(var i = 0;i < this.pairs.length;i++) {
    var pair = this.pairs[i];
    if(pair[1].apply(this, arguments)) {
      if(pair[3] || this.returnWrappers) {
        return pair[2]
      }else {
        return pair[2].apply(this, arguments)
      }
    }
  }
  throw new Error("No match found");
}, unregister:function(name) {
  for(var i = 0;i < this.pairs.length;i++) {
    var pair = this.pairs[i];
    if(pair[0] == name) {
      this.pairs.splice(i, 1);
      return true
    }
  }
  return false
}});
(function() {
  var registry;
  var plainXhr;
  function getPlainXhr() {
    return plainXhr = midojox.io.xhrPlugins.plainXhr = plainXhr || midojo._defaultXhr || midojo.xhr
  }
  midojox.io.xhrPlugins.register = function() {
    var plainXhr = getPlainXhr();
    if(!registry) {
      registry = new midojo.AdapterRegistry;
      midojo[midojo._defaultXhr ? "_defaultXhr" : "xhr"] = function(method, args, hasBody) {
        return registry.match.apply(registry, arguments)
      };
      registry.register("xhr", function(method, args) {
        if(!args.url.match(/^\w*:\/\//)) {
          return true
        }
        var root = window.location.href.match(/^.*?\/\/.*?\//)[0];
        return args.url.substring(0, root.length) == root
      }, plainXhr)
    }
    return registry.register.apply(registry, arguments)
  };
  midojox.io.xhrPlugins.addProxy = function(proxyUrl) {
    var plainXhr = getPlainXhr();
    midojox.io.xhrPlugins.register("proxy", function(method, args) {
      return true
    }, function(method, args, hasBody) {
      args.url = proxyUrl + encodeURIComponent(args.url);
      return plainXhr.call(midojo, method, args, hasBody)
    })
  };
  var csXhrSupport;
  midojox.io.xhrPlugins.addCrossSiteXhr = function(url, httpAdapter) {
    var plainXhr = getPlainXhr();
    if(csXhrSupport === undefined && window.XMLHttpRequest) {
      try {
        var xhr = new XMLHttpRequest;
        xhr.open("GET", "http://testing-cross-domain-capability.com", true);
        csXhrSupport = true;
        midojo.config.noRequestedWithHeaders = true
      }catch(e) {
        csXhrSupport = false
      }
    }
    midojox.io.xhrPlugins.register("cs-xhr", function(method, args) {
      return(csXhrSupport || window.XDomainRequest && args.sync !== true && (method == "GET" || method == "POST" || httpAdapter)) && args.url.substring(0, url.length) == url
    }, csXhrSupport ? plainXhr : function() {
      var normalXhrObj = midojo._xhrObj;
      midojo._xhrObj = function() {
        var xdr = new XDomainRequest;
        xdr.readyState = 1;
        xdr.setRequestHeader = function() {
        };
        xdr.getResponseHeader = function(header) {
          return header == "Content-Type" ? xdr.contentType : null
        };
        function handler(status, readyState) {
          return function() {
            xdr.readyState = readyState;
            xdr.status = status
          }
        }
        xdr.onload = handler(200, 4);
        xdr.onprogress = handler(200, 3);
        xdr.onerror = handler(404, 4);
        return xdr
      };
      var dfd = (httpAdapter ? httpAdapter(getPlainXhr()) : getPlainXhr()).apply(midojo, arguments);
      midojo._xhrObj = normalXhrObj;
      return dfd
    })
  };
  midojox.io.xhrPlugins.fullHttpAdapter = function(plainXhr, noRawBody) {
    return function(method, args, hasBody) {
      var content = {};
      var parameters = {};
      if(method != "GET") {
        parameters["http-method"] = method;
        if(args.putData && noRawBody) {
          content["http-content"] = args.putData;
          delete args.putData;
          hasBody = false
        }
        if(args.postData && noRawBody) {
          content["http-content"] = args.postData;
          delete args.postData;
          hasBody = false
        }
        method = "POST"
      }
      for(var i in args.headers) {
        var parameterName = i.match(/^X-/) ? i.substring(2).replace(/-/g, "_").toLowerCase() : "http-" + i;
        parameters[parameterName] = args.headers[i]
      }
      args.query = midojo.objectToQuery(parameters);
      midojo._ioAddQueryToUrl(args);
      args.content = midojo.mixin(args.content || {}, content);
      return plainXhr.call(midojo, method, args, hasBody)
    }
  }
})();
midojo.declare("ria.RestService", null, {_env:null, constructor:function(env) {
  if(!env) {
    env = new ria.Environment
  }
  this._env = env
}, call:function(url$$1) {
  return midojo.xhrGet({url:url$$1, handleAs:"json", headers:{"X-Requested-With":""}, load:function(result$$2, ioArgs) {
  }, error:function(error, ioArgs$$1) {
  }})
}, makeXdSafe:function(url$$2) {
  var origin$$1 = this._env.getOrigin();
  if(url$$2.indexOf(origin$$1) === 0) {
    return url$$2
  }
  return ria.RestService.toProxyUrl(url$$2)
}});
ria.RestService.addProxy = function(path$$1) {
  ria.RestService._proxyUrl = path$$1;
  midojox.io.xhrPlugins.addProxy(path$$1)
};
ria.RestService.toProxyUrl = function(url$$3) {
  if(!ria.RestService._proxyUrl) {
    return url$$3
  }
  return ria.RestService._proxyUrl + encodeURIComponent(url$$3)
};
OpenLayers.Layer.MappingServiceLayer = OpenLayers.Class(OpenLayers.Layer.Grid, {_mapName:null, _format:null, _restService:null, initialize:function(displayName, mapName, url$$4, options, restService) {
  OpenLayers.Layer.Grid.prototype.initialize.call(this, displayName, url$$4, {}, options);
  this._mapName = mapName;
  if(options && options.format) {
    this._format = options.format
  }else {
    this._format = "png"
  }
  if(!restService) {
    restService = new ria.RestService
  }
  this._restService = restService
}, getMapName:function() {
  return this._mapName
}, getURL:function(bounds) {
  bounds = this.adjustBounds(bounds);
  var projection = this.projection.getCode();
  var imageSize = this.getImageSize();
  var boundsStr = bounds.left + "," + bounds.bottom + "," + bounds.right + "," + bounds.top + "," + encodeURIComponent(projection);
  var encMapName = this._uriEncodeMapName(this._mapName);
  var url$$5 = this.url + "/maps/" + encMapName + "/image." + this._format + ";w=" + imageSize.w + ";h=" + imageSize.h + ";b=" + boundsStr;
  return this._restService.makeXdSafe(url$$5)
}, addTile:function(bounds$$1, position) {
  return new OpenLayers.Tile.Image(this, position, bounds$$1, null, this.tileSize)
}, _uriEncodeMapName:function(mapName$$1) {
  if(mapName$$1.charAt(0) === "/") {
    mapName$$1 = mapName$$1.substring(1)
  }
  var encodedName = encodeURIComponent(mapName$$1);
  return encodedName.replace(/%2F/g, "/")
}, CLASS_NAME:"OpenLayers.Layer.MappingServiceLayer"});
OpenLayers.Layer.TileServiceLayer = OpenLayers.Class(OpenLayers.Layer.Grid, {_mapName:null, _restService:null, _format:null, initialize:function(displayName$$1, mapName$$2, url$$6, options$$1, restService$$1) {
  OpenLayers.Layer.Grid.prototype.initialize.call(this, displayName$$1, url$$6, {}, options$$1);
  this._mapName = mapName$$2;
  if(options$$1 && options$$1.format) {
    this._format = options$$1.format
  }else {
    this._format = "gif"
  }
  if(!restService$$1) {
    restService$$1 = new ria.RestService
  }
  this._restService = restService$$1
}, getURL:function(bounds$$2) {
  var mapBounds = this.map.getMaxExtent();
  var res = this.map.getResolution();
  var col = (bounds$$2.left - mapBounds.left) / (res * this.tileSize.w) + 0.5;
  var row = (mapBounds.top - bounds$$2.top) / (res * this.tileSize.h) + 0.5;
  var zoom = this.map.getZoom() + 1;
  col = parseInt(col) + 1;
  row = parseInt(row) + 1;
  var encodedMapName = this._uriEncodeMapName(this._mapName);
  var url$$7 = this.url + "/" + encodedMapName + "/" + zoom + "/" + col + ":" + row + "/tile." + this._format;
  return this._restService.makeXdSafe(url$$7)
}, _uriEncodeMapName:function(mapName$$3) {
  if(mapName$$3.charAt(0) === "/") {
    mapName$$3 = mapName$$3.substring(1)
  }
  var encodedName$$1 = encodeURIComponent(mapName$$3);
  return encodedName$$1.replace(/%2F/g, "/")
}, addTile:function(bounds$$3, position$$1) {
  return new OpenLayers.Tile.Image(this, position$$1, bounds$$3, null, this.tileSize)
}, CLASS_NAME:"OpenLayers.Layer.TileServiceLayer"});
midojo.provide("ria.legend.LegendControl");
midojo.provide("midijit._Widget");
midojo.provide("midijit._base");
midojo.provide("midijit._base.focus");
midojo.provide("midijit._base.manager");
midojo.declare("midijit.WidgetSet", null, {constructor:function() {
  this._hash = {};
  this.length = 0
}, add:function(widget) {
  if(this._hash[widget.id]) {
    throw new Error("Tried to register widget with id==" + widget.id + " but that id is already registered");
  }
  this._hash[widget.id] = widget;
  this.length++
}, remove:function(id) {
  if(this._hash[id]) {
    delete this._hash[id];
    this.length--
  }
}, forEach:function(func, thisObj) {
  thisObj = thisObj || midojo.global;
  var i = 0, id;
  for(id in this._hash) {
    func.call(thisObj, this._hash[id], i++, this._hash)
  }
  return this
}, filter:function(filter, thisObj) {
  thisObj = thisObj || midojo.global;
  var res = new midijit.WidgetSet, i = 0, id;
  for(id in this._hash) {
    var w = this._hash[id];
    if(filter.call(thisObj, w, i++, this._hash)) {
      res.add(w)
    }
  }
  return res
}, byId:function(id) {
  return this._hash[id]
}, byClass:function(cls) {
  var res = new midijit.WidgetSet, id, widget;
  for(id in this._hash) {
    widget = this._hash[id];
    if(widget.declaredClass == cls) {
      res.add(widget)
    }
  }
  return res
}, toArray:function() {
  var ar = [];
  for(var id in this._hash) {
    ar.push(this._hash[id])
  }
  return ar
}, map:function(func, thisObj) {
  return midojo.map(this.toArray(), func, thisObj)
}, every:function(func, thisObj) {
  thisObj = thisObj || midojo.global;
  var x = 0, i;
  for(i in this._hash) {
    if(!func.call(thisObj, this._hash[i], x++, this._hash)) {
      return false
    }
  }
  return true
}, some:function(func, thisObj) {
  thisObj = thisObj || midojo.global;
  var x = 0, i;
  for(i in this._hash) {
    if(func.call(thisObj, this._hash[i], x++, this._hash)) {
      return true
    }
  }
  return false
}});
midijit.registry = new midijit.WidgetSet;
midijit._widgetTypeCtr = {};
midijit.getUniqueId = function(widgetType) {
  var id;
  do {
    id = widgetType + "_" + (widgetType in midijit._widgetTypeCtr ? ++midijit._widgetTypeCtr[widgetType] : midijit._widgetTypeCtr[widgetType] = 0)
  }while(midijit.byId(id));
  return midijit._scopeName == "midijit" ? id : midijit._scopeName + "_" + id
};
midijit.findWidgets = function(root) {
  var outAry = [];
  function getChildrenHelper(root) {
    for(var node = root.firstChild;node;node = node.nextSibling) {
      if(node.nodeType == 1) {
        var widgetId = node.getAttribute("widgetId");
        if(widgetId) {
          var widget = midijit.byId(widgetId);
          outAry.push(widget)
        }else {
          getChildrenHelper(node)
        }
      }
    }
  }
  getChildrenHelper(root);
  return outAry
};
midijit._destroyAll = function() {
  midijit._curFocus = null;
  midijit._prevFocus = null;
  midijit._activeStack = [];
  midojo.forEach(midijit.findWidgets(midojo.body()), function(widget) {
    if(!widget._destroyed) {
      if(widget.destroyRecursive) {
        widget.destroyRecursive()
      }else {
        if(widget.destroy) {
          widget.destroy()
        }
      }
    }
  })
};
if(midojo.isIE) {
  midojo.addOnWindowUnload(function() {
    midijit._destroyAll()
  })
}
midijit.byId = function(id) {
  return typeof id == "string" ? midijit.registry._hash[id] : id
};
midijit.byNode = function(node) {
  return midijit.registry.byId(node.getAttribute("widgetId"))
};
midijit.getEnclosingWidget = function(node) {
  while(node) {
    var id = node.getAttribute && node.getAttribute("widgetId");
    if(id) {
      return midijit.byId(id)
    }
    node = node.parentNode
  }
  return null
};
midijit._isElementShown = function(elem) {
  var style = midojo.style(elem);
  return style.visibility != "hidden" && style.visibility != "collapsed" && style.display != "none" && midojo.attr(elem, "type") != "hidden"
};
midijit.isTabNavigable = function(elem) {
  if(midojo.attr(elem, "disabled")) {
    return false
  }else {
    if(midojo.hasAttr(elem, "tabIndex")) {
      return midojo.attr(elem, "tabIndex") >= 0
    }else {
      switch(elem.nodeName.toLowerCase()) {
        case "a":
          return midojo.hasAttr(elem, "href");
        case "area":
        ;
        case "button":
        ;
        case "input":
        ;
        case "object":
        ;
        case "select":
        ;
        case "textarea":
          return true;
        case "iframe":
          if(midojo.isMoz) {
            return elem.contentDocument.designMode == "on"
          }else {
            if(midojo.isWebKit) {
              var doc = elem.contentDocument, body = doc && doc.body;
              return body && body.contentEditable == "true"
            }else {
              try {
                doc = elem.contentWindow.document;
                body = doc && doc.body;
                return body && body.firstChild && body.firstChild.contentEditable == "true"
              }catch(e) {
                return false
              }
            }
          }
        ;
        default:
          return elem.contentEditable == "true"
      }
    }
  }
};
midijit._getTabNavigable = function(root) {
  var first, last, lowest, lowestTabindex, highest, highestTabindex;
  var walkTree = function(parent) {
    midojo.query("> *", parent).forEach(function(child) {
      var isShown = midijit._isElementShown(child);
      if(isShown && midijit.isTabNavigable(child)) {
        var tabindex = midojo.attr(child, "tabIndex");
        if(!midojo.hasAttr(child, "tabIndex") || tabindex == 0) {
          if(!first) {
            first = child
          }
          last = child
        }else {
          if(tabindex > 0) {
            if(!lowest || tabindex < lowestTabindex) {
              lowestTabindex = tabindex;
              lowest = child
            }
            if(!highest || tabindex >= highestTabindex) {
              highestTabindex = tabindex;
              highest = child
            }
          }
        }
      }
      if(isShown && child.nodeName.toUpperCase() != "SELECT") {
        walkTree(child)
      }
    })
  };
  if(midijit._isElementShown(root)) {
    walkTree(root)
  }
  return{first:first, last:last, lowest:lowest, highest:highest}
};
midijit.getFirstInTabbingOrder = function(root) {
  var elems = midijit._getTabNavigable(midojo.byId(root));
  return elems.lowest ? elems.lowest : elems.first
};
midijit.getLastInTabbingOrder = function(root) {
  var elems = midijit._getTabNavigable(midojo.byId(root));
  return elems.last ? elems.last : elems.highest
};
midijit.defaultDuration = midojo.config["defaultDuration"] || 200;
midojo.mixin(midijit, {_curFocus:null, _prevFocus:null, isCollapsed:function() {
  return midijit.getBookmark().isCollapsed
}, getBookmark:function() {
  var bm, rg, tg, sel = midojo.doc.selection, cf = midijit._curFocus;
  if(midojo.global.getSelection) {
    sel = midojo.global.getSelection();
    if(sel) {
      if(sel.isCollapsed) {
        tg = cf ? cf.tagName : "";
        if(tg) {
          tg = tg.toLowerCase();
          if(tg == "textarea" || tg == "input" && (!cf.type || cf.type.toLowerCase() == "text")) {
            sel = {start:cf.selectionStart, end:cf.selectionEnd, node:cf, pRange:true};
            return{isCollapsed:sel.end <= sel.start, mark:sel}
          }
        }
        bm = {isCollapsed:true}
      }else {
        rg = sel.getRangeAt(0);
        bm = {isCollapsed:false, mark:rg.cloneRange()}
      }
    }
  }else {
    if(sel) {
      tg = cf ? cf.tagName : "";
      tg = tg.toLowerCase();
      if(cf && tg && (tg == "button" || tg == "textarea" || tg == "input")) {
        if(sel.type && sel.type.toLowerCase() == "none") {
          return{isCollapsed:true, mark:null}
        }else {
          rg = sel.createRange();
          return{isCollapsed:rg.text && rg.text.length ? false : true, mark:{range:rg, pRange:true}}
        }
      }
      bm = {};
      try {
        rg = sel.createRange();
        bm.isCollapsed = !(sel.type == "Text" ? rg.htmlText.length : rg.length)
      }catch(e) {
        bm.isCollapsed = true;
        return bm
      }
      if(sel.type.toUpperCase() == "CONTROL") {
        if(rg.length) {
          bm.mark = [];
          var i = 0, len = rg.length;
          while(i < len) {
            bm.mark.push(rg.item(i++))
          }
        }else {
          bm.isCollapsed = true;
          bm.mark = null
        }
      }else {
        bm.mark = rg.getBookmark()
      }
    }else {
      console.warn("No idea how to store the current selection for this browser!")
    }
  }
  return bm
}, moveToBookmark:function(bookmark) {
  var _doc = midojo.doc, mark = bookmark.mark;
  if(mark) {
    if(midojo.global.getSelection) {
      var sel = midojo.global.getSelection();
      if(sel && sel.removeAllRanges) {
        if(mark.pRange) {
          var r = mark;
          var n = r.node;
          n.selectionStart = r.start;
          n.selectionEnd = r.end
        }else {
          sel.removeAllRanges();
          sel.addRange(mark)
        }
      }else {
        console.warn("No idea how to restore selection for this browser!")
      }
    }else {
      if(_doc.selection && mark) {
        var rg;
        if(mark.pRange) {
          rg = mark.range
        }else {
          if(midojo.isArray(mark)) {
            rg = _doc.body.createControlRange();
            midojo.forEach(mark, function(n) {
              rg.addElement(n)
            })
          }else {
            rg = _doc.body.createTextRange();
            rg.moveToBookmark(mark)
          }
        }
        rg.select()
      }
    }
  }
}, getFocus:function(menu, openedForWindow) {
  var node = !midijit._curFocus || menu && midojo.isDescendant(midijit._curFocus, menu.domNode) ? midijit._prevFocus : midijit._curFocus;
  return{node:node, bookmark:node == midijit._curFocus && midojo.withGlobal(openedForWindow || midojo.global, midijit.getBookmark), openedForWindow:openedForWindow}
}, focus:function(handle) {
  if(!handle) {
    return
  }
  var node = "node" in handle ? handle.node : handle, bookmark = handle.bookmark, openedForWindow = handle.openedForWindow, collapsed = bookmark ? bookmark.isCollapsed : false;
  if(node) {
    var focusNode = node.tagName.toLowerCase() == "iframe" ? node.contentWindow : node;
    if(focusNode && focusNode.focus) {
      try {
        focusNode.focus()
      }catch(e) {
      }
    }
    midijit._onFocusNode(node)
  }
  if(bookmark && midojo.withGlobal(openedForWindow || midojo.global, midijit.isCollapsed) && !collapsed) {
    if(openedForWindow) {
      openedForWindow.focus()
    }
    try {
      midojo.withGlobal(openedForWindow || midojo.global, midijit.moveToBookmark, null, [bookmark])
    }catch(e2) {
    }
  }
}, _activeStack:[], registerIframe:function(iframe) {
  return midijit.registerWin(iframe.contentWindow, iframe)
}, unregisterIframe:function(handle) {
  midijit.unregisterWin(handle)
}, registerWin:function(targetWindow, effectiveNode) {
  var mousedownListener = function(evt) {
    midijit._justMouseDowned = true;
    setTimeout(function() {
      midijit._justMouseDowned = false
    }, 0);
    midijit._onTouchNode(effectiveNode || evt.target || evt.srcElement, "mouse")
  };
  var doc = midojo.isIE ? targetWindow.document.documentElement : targetWindow.document;
  if(doc) {
    if(midojo.isIE) {
      doc.attachEvent("onmousedown", mousedownListener);
      var activateListener = function(evt) {
        if(evt.srcElement.tagName.toLowerCase() != "#document" && midijit.isTabNavigable(evt.srcElement)) {
          midijit._onFocusNode(effectiveNode || evt.srcElement)
        }else {
          midijit._onTouchNode(effectiveNode || evt.srcElement)
        }
      };
      doc.attachEvent("onactivate", activateListener);
      var deactivateListener = function(evt) {
        midijit._onBlurNode(effectiveNode || evt.srcElement)
      };
      doc.attachEvent("ondeactivate", deactivateListener);
      return function() {
        doc.detachEvent("onmousedown", mousedownListener);
        doc.detachEvent("onactivate", activateListener);
        doc.detachEvent("ondeactivate", deactivateListener);
        doc = null
      }
    }else {
      doc.addEventListener("mousedown", mousedownListener, true);
      var focusListener = function(evt) {
        midijit._onFocusNode(effectiveNode || evt.target)
      };
      doc.addEventListener("focus", focusListener, true);
      var blurListener = function(evt) {
        midijit._onBlurNode(effectiveNode || evt.target)
      };
      doc.addEventListener("blur", blurListener, true);
      return function() {
        doc.removeEventListener("mousedown", mousedownListener, true);
        doc.removeEventListener("focus", focusListener, true);
        doc.removeEventListener("blur", blurListener, true);
        doc = null
      }
    }
  }
}, unregisterWin:function(handle) {
  handle && handle()
}, _onBlurNode:function(node) {
  midijit._prevFocus = midijit._curFocus;
  midijit._curFocus = null;
  if(midijit._justMouseDowned) {
    return
  }
  if(midijit._clearActiveWidgetsTimer) {
    clearTimeout(midijit._clearActiveWidgetsTimer)
  }
  midijit._clearActiveWidgetsTimer = setTimeout(function() {
    delete midijit._clearActiveWidgetsTimer;
    midijit._setStack([]);
    midijit._prevFocus = null
  }, 100)
}, _onTouchNode:function(node, by) {
  if(midijit._clearActiveWidgetsTimer) {
    clearTimeout(midijit._clearActiveWidgetsTimer);
    delete midijit._clearActiveWidgetsTimer
  }
  var newStack = [];
  try {
    while(node) {
      var popupParent = midojo.attr(node, "dijitPopupParent");
      if(popupParent) {
        node = midijit.byId(popupParent).domNode
      }else {
        if(node.tagName && node.tagName.toLowerCase() == "body") {
          if(node === midojo.body()) {
            break
          }
          node = midijit.getDocumentWindow(node.ownerDocument).frameElement
        }else {
          var id = node.getAttribute && node.getAttribute("widgetId");
          if(id) {
            newStack.unshift(id)
          }
          node = node.parentNode
        }
      }
    }
  }catch(e) {
  }
  midijit._setStack(newStack, by)
}, _onFocusNode:function(node) {
  if(!node) {
    return
  }
  if(node.nodeType == 9) {
    return
  }
  midijit._onTouchNode(node);
  if(node == midijit._curFocus) {
    return
  }
  if(midijit._curFocus) {
    midijit._prevFocus = midijit._curFocus
  }
  midijit._curFocus = node;
  midojo.publish("focusNode", [node])
}, _setStack:function(newStack, by) {
  var oldStack = midijit._activeStack;
  midijit._activeStack = newStack;
  for(var nCommon = 0;nCommon < Math.min(oldStack.length, newStack.length);nCommon++) {
    if(oldStack[nCommon] != newStack[nCommon]) {
      break
    }
  }
  var widget;
  for(var i = oldStack.length - 1;i >= nCommon;i--) {
    widget = midijit.byId(oldStack[i]);
    if(widget) {
      widget._focused = false;
      widget._hasBeenBlurred = true;
      if(widget._onBlur) {
        widget._onBlur(by)
      }
      if(widget._setStateClass) {
        widget._setStateClass()
      }
      midojo.publish("widgetBlur", [widget, by])
    }
  }
  for(i = nCommon;i < newStack.length;i++) {
    widget = midijit.byId(newStack[i]);
    if(widget) {
      widget._focused = true;
      if(widget._onFocus) {
        widget._onFocus(by)
      }
      if(widget._setStateClass) {
        widget._setStateClass()
      }
      midojo.publish("widgetFocus", [widget, by])
    }
  }
}});
midojo.addOnLoad(function() {
  var handle = midijit.registerWin(window);
  if(midojo.isIE) {
    midojo.addOnWindowUnload(function() {
      midijit.unregisterWin(handle);
      handle = null
    })
  }
});
midojo.provide("midijit._base.place");
midijit.getViewport = function() {
  var scrollRoot = midojo.doc.compatMode == "BackCompat" ? midojo.body() : midojo.doc.documentElement;
  var scroll = midojo._docScroll();
  return{w:scrollRoot.clientWidth, h:scrollRoot.clientHeight, l:scroll.x, t:scroll.y}
};
midijit.placeOnScreen = function(node, pos, corners, padding) {
  var choices = midojo.map(corners, function(corner) {
    var c = {corner:corner, pos:{x:pos.x, y:pos.y}};
    if(padding) {
      c.pos.x += corner.charAt(1) == "L" ? padding.x : -padding.x;
      c.pos.y += corner.charAt(0) == "T" ? padding.y : -padding.y
    }
    return c
  });
  return midijit._place(node, choices)
};
midijit._place = function(node, choices, layoutNode) {
  var view = midijit.getViewport();
  if(!node.parentNode || String(node.parentNode.tagName).toLowerCase() != "body") {
    midojo.body().appendChild(node)
  }
  var best = null;
  midojo.some(choices, function(choice) {
    var corner = choice.corner;
    var pos = choice.pos;
    if(layoutNode) {
      layoutNode(node, choice.aroundCorner, corner)
    }
    var style = node.style;
    var oldDisplay = style.display;
    var oldVis = style.visibility;
    style.visibility = "hidden";
    style.display = "";
    var mb = midojo.marginBox(node);
    style.display = oldDisplay;
    style.visibility = oldVis;
    var startX = Math.max(view.l, corner.charAt(1) == "L" ? pos.x : pos.x - mb.w), startY = Math.max(view.t, corner.charAt(0) == "T" ? pos.y : pos.y - mb.h), endX = Math.min(view.l + view.w, corner.charAt(1) == "L" ? startX + mb.w : pos.x), endY = Math.min(view.t + view.h, corner.charAt(0) == "T" ? startY + mb.h : pos.y), width = endX - startX, height = endY - startY, overflow = mb.w - width + (mb.h - height);
    if(best == null || overflow < best.overflow) {
      best = {corner:corner, aroundCorner:choice.aroundCorner, x:startX, y:startY, w:width, h:height, overflow:overflow}
    }
    return!overflow
  });
  node.style.left = best.x + "px";
  node.style.top = best.y + "px";
  if(best.overflow && layoutNode) {
    layoutNode(node, best.aroundCorner, best.corner)
  }
  return best
};
midijit.placeOnScreenAroundNode = function(node, aroundNode, aroundCorners, layoutNode) {
  aroundNode = midojo.byId(aroundNode);
  var oldDisplay = aroundNode.style.display;
  aroundNode.style.display = "";
  var aroundNodePos = midojo.position(aroundNode, true);
  aroundNode.style.display = oldDisplay;
  return midijit._placeOnScreenAroundRect(node, aroundNodePos.x, aroundNodePos.y, aroundNodePos.w, aroundNodePos.h, aroundCorners, layoutNode)
};
midijit.placeOnScreenAroundRectangle = function(node, aroundRect, aroundCorners, layoutNode) {
  return midijit._placeOnScreenAroundRect(node, aroundRect.x, aroundRect.y, aroundRect.width, aroundRect.height, aroundCorners, layoutNode)
};
midijit._placeOnScreenAroundRect = function(node, x, y, width, height, aroundCorners, layoutNode) {
  var choices = [];
  for(var nodeCorner in aroundCorners) {
    choices.push({aroundCorner:nodeCorner, corner:aroundCorners[nodeCorner], pos:{x:x + (nodeCorner.charAt(1) == "L" ? 0 : width), y:y + (nodeCorner.charAt(0) == "T" ? 0 : height)}})
  }
  return midijit._place(node, choices, layoutNode)
};
midijit.placementRegistry = new midojo.AdapterRegistry;
midijit.placementRegistry.register("node", function(n, x) {
  return typeof x == "object" && typeof x.offsetWidth != "undefined" && typeof x.offsetHeight != "undefined"
}, midijit.placeOnScreenAroundNode);
midijit.placementRegistry.register("rect", function(n, x) {
  return typeof x == "object" && "x" in x && "y" in x && "width" in x && "height" in x
}, midijit.placeOnScreenAroundRectangle);
midijit.placeOnScreenAroundElement = function(node, aroundElement, aroundCorners, layoutNode) {
  return midijit.placementRegistry.match.apply(midijit.placementRegistry, arguments)
};
midijit.getPopupAlignment = function(position, leftToRight) {
  var align = {};
  midojo.forEach(position, function(pos) {
    switch(pos) {
      case "after":
        align[leftToRight ? "BR" : "BL"] = leftToRight ? "BL" : "BR";
        break;
      case "before":
        align[leftToRight ? "BL" : "BR"] = leftToRight ? "BR" : "BL";
        break;
      case "below":
        align[leftToRight ? "BL" : "BR"] = leftToRight ? "TL" : "TR";
        align[leftToRight ? "BR" : "BL"] = leftToRight ? "TR" : "TL";
        break;
      case "above":
      ;
      default:
        align[leftToRight ? "TL" : "TR"] = leftToRight ? "BL" : "BR";
        align[leftToRight ? "TR" : "TL"] = leftToRight ? "BR" : "BL";
        break
    }
  });
  return align
};
midijit.getPopupAroundAlignment = function(position, leftToRight) {
  var align = {};
  midojo.forEach(position, function(pos) {
    switch(pos) {
      case "after":
        align[leftToRight ? "BR" : "BL"] = leftToRight ? "BL" : "BR";
        break;
      case "before":
        align[leftToRight ? "BL" : "BR"] = leftToRight ? "BR" : "BL";
        break;
      case "below":
        align[leftToRight ? "BL" : "BR"] = leftToRight ? "TL" : "TR";
        align[leftToRight ? "BR" : "BL"] = leftToRight ? "TR" : "TL";
        break;
      case "above":
      ;
      default:
        align[leftToRight ? "TL" : "TR"] = leftToRight ? "BL" : "BR";
        align[leftToRight ? "TR" : "TL"] = leftToRight ? "BR" : "BL";
        break
    }
  });
  return align
};
midojo.provide("midijit._base.popup");
midojo.provide("midijit._base.window");
midijit.getDocumentWindow = function(doc) {
  if(midojo.isIE && window !== document.parentWindow && !doc._parentWindow) {
    doc.parentWindow.execScript("document._parentWindow = window;", "Javascript");
    var win = doc._parentWindow;
    doc._parentWindow = null;
    return win
  }
  return doc._parentWindow || doc.parentWindow || doc.defaultView
};
midijit.popup = new function() {
  var stack = [], beginZIndex = 1E3, idGen = 1;
  this.moveOffScreen = function(node) {
    var s = node.style;
    s.visibility = "hidden";
    s.position = "absolute";
    s.top = "-9999px";
    if(s.display == "none") {
      s.display = ""
    }
    midojo.body().appendChild(node)
  };
  var getTopPopup = function() {
    for(var pi = stack.length - 1;pi > 0 && stack[pi].parent === stack[pi - 1].widget;pi--) {
    }
    return stack[pi]
  };
  var wrappers = [];
  this.open = function(args) {
    var widget = args.popup, orient = args.orient || (midojo._isBodyLtr() ? {"BL":"TL", "BR":"TR", "TL":"BL", "TR":"BR"} : {"BR":"TR", "BL":"TL", "TR":"BR", "TL":"BL"}), around = args.around, id = args.around && args.around.id ? args.around.id + "_dropdown" : "popup_" + idGen++;
    var wrapperobj = wrappers.pop(), wrapper, iframe;
    if(!wrapperobj) {
      wrapper = midojo.create("div", {"class":"dijitPopup"}, midojo.body());
      midijit.setWaiRole(wrapper, "presentation")
    }else {
      wrapper = wrapperobj[0];
      iframe = wrapperobj[1]
    }
    midojo.attr(wrapper, {id:id, style:{zIndex:beginZIndex + stack.length, visibility:"hidden", top:"-9999px"}, dijitPopupParent:args.parent ? args.parent.id : ""});
    var s = widget.domNode.style;
    s.display = "";
    s.visibility = "";
    s.position = "";
    s.top = "0px";
    wrapper.appendChild(widget.domNode);
    if(!iframe) {
      iframe = new midijit.BackgroundIframe(wrapper)
    }else {
      iframe.resize(wrapper)
    }
    var best = around ? midijit.placeOnScreenAroundElement(wrapper, around, orient, widget.orient ? midojo.hitch(widget, "orient") : null) : midijit.placeOnScreen(wrapper, args, orient == "R" ? ["TR", "BR", "TL", "BL"] : ["TL", "BL", "TR", "BR"], args.padding);
    wrapper.style.visibility = "visible";
    var handlers = [];
    handlers.push(midojo.connect(wrapper, "onkeypress", this, function(evt) {
      if(evt.charOrCode == midojo.keys.ESCAPE && args.onCancel) {
        midojo.stopEvent(evt);
        args.onCancel()
      }else {
        if(evt.charOrCode === midojo.keys.TAB) {
          midojo.stopEvent(evt);
          var topPopup = getTopPopup();
          if(topPopup && topPopup.onCancel) {
            topPopup.onCancel()
          }
        }
      }
    }));
    if(widget.onCancel) {
      handlers.push(midojo.connect(widget, "onCancel", args.onCancel))
    }
    handlers.push(midojo.connect(widget, widget.onExecute ? "onExecute" : "onChange", function() {
      var topPopup = getTopPopup();
      if(topPopup && topPopup.onExecute) {
        topPopup.onExecute()
      }
    }));
    stack.push({wrapper:wrapper, iframe:iframe, widget:widget, parent:args.parent, onExecute:args.onExecute, onCancel:args.onCancel, onClose:args.onClose, handlers:handlers});
    if(widget.onOpen) {
      widget.onOpen(best)
    }
    return best
  };
  this.close = function(popup) {
    while(midojo.some(stack, function(elem) {
      return elem.widget == popup
    })) {
      var top = stack.pop(), wrapper = top.wrapper, iframe = top.iframe, widget = top.widget, onClose = top.onClose;
      if(widget.onClose) {
        widget.onClose()
      }
      midojo.forEach(top.handlers, midojo.disconnect);
      if(widget && widget.domNode) {
        this.moveOffScreen(widget.domNode)
      }
      wrapper.style.top = "-9999px";
      wrapper.style.visibility = "hidden";
      wrappers.push([wrapper, iframe]);
      if(onClose) {
        onClose()
      }
    }
  }
};
midijit._frames = new function() {
  var queue = [];
  this.pop = function() {
    var iframe;
    if(queue.length) {
      iframe = queue.pop();
      iframe.style.display = ""
    }else {
      if(midojo.isIE) {
        var burl = midojo.config["dojoBlankHtmlUrl"] || midojo.moduleUrl("midojo", "resources/blank.html") + "" || 'javascript:""';
        var html = "<iframe src='" + burl + "'" + " style='position: absolute; left: 0px; top: 0px;" + 'z-index: -1; filter:Alpha(Opacity="0");\'>';
        iframe = midojo.doc.createElement(html)
      }else {
        iframe = midojo.create("iframe");
        iframe.src = 'javascript:""';
        iframe.className = "dijitBackgroundIframe";
        midojo.style(iframe, "opacity", 0.1)
      }
      iframe.tabIndex = -1
    }
    return iframe
  };
  this.push = function(iframe) {
    iframe.style.display = "none";
    queue.push(iframe)
  }
};
midijit.BackgroundIframe = function(node) {
  if(!node.id) {
    throw new Error("no id");
  }
  if(midojo.isIE || midojo.isMoz) {
    var iframe = midijit._frames.pop();
    node.appendChild(iframe);
    if(midojo.isIE < 7) {
      this.resize(node);
      this._conn = midojo.connect(node, "onresize", this, function() {
        this.resize(node)
      })
    }else {
      midojo.style(iframe, {width:"100%", height:"100%"})
    }
    this.iframe = iframe
  }
};
midojo.extend(midijit.BackgroundIframe, {resize:function(node) {
  if(this.iframe && midojo.isIE < 7) {
    midojo.style(this.iframe, {width:node.offsetWidth + "px", height:node.offsetHeight + "px"})
  }
}, destroy:function() {
  if(this._conn) {
    midojo.disconnect(this._conn);
    this._conn = null
  }
  if(this.iframe) {
    midijit._frames.push(this.iframe);
    delete this.iframe
  }
}});
midojo.provide("midijit._base.scroll");
midijit.scrollIntoView = function(node, pos) {
  try {
    node = midojo.byId(node);
    var doc = node.ownerDocument || midojo.doc, body = doc.body || midojo.body(), html = doc.documentElement || body.parentNode, isIE = midojo.isIE, isWK = midojo.isWebKit;
    if((!(midojo.isMoz || isIE || isWK) || node == body || node == html) && typeof node.scrollIntoView != "undefined") {
      node.scrollIntoView(false);
      return
    }
    var backCompat = doc.compatMode == "BackCompat", clientAreaRoot = backCompat ? body : html, scrollRoot = isWK ? body : clientAreaRoot, rootWidth = clientAreaRoot.clientWidth, rootHeight = clientAreaRoot.clientHeight, rtl = !midojo._isBodyLtr(), nodePos = pos || midojo.position(node), el = node.parentNode, isFixed = function(el) {
      return isIE <= 6 || isIE && backCompat ? false : midojo.style(el, "position").toLowerCase() == "fixed"
    };
    if(isFixed(node)) {
      return
    }
    while(el) {
      if(el == body) {
        el = scrollRoot
      }
      var elPos = midojo.position(el), fixedPos = isFixed(el);
      with(elPos) {
        if(el == scrollRoot) {
          w = rootWidth, h = rootHeight;
          if(scrollRoot == html && isIE && rtl) {
            x += scrollRoot.offsetWidth - w
          }
          if(x < 0 || !isIE) {
            x = 0
          }
          if(y < 0 || !isIE) {
            y = 0
          }
        }else {
          var pb = midojo._getPadBorderExtents(el);
          w -= pb.w;
          h -= pb.h;
          x += pb.l;
          y += pb.t
        }
        with(el) {
          if(el != scrollRoot) {
            var clientSize = clientWidth, scrollBarSize = w - clientSize;
            if(clientSize > 0 && scrollBarSize > 0) {
              w = clientSize;
              if(isIE && rtl) {
                x += scrollBarSize
              }
            }
            clientSize = clientHeight;
            scrollBarSize = h - clientSize;
            if(clientSize > 0 && scrollBarSize > 0) {
              h = clientSize
            }
          }
          if(fixedPos) {
            if(y < 0) {
              h += y, y = 0
            }
            if(x < 0) {
              w += x, x = 0
            }
            if(y + h > rootHeight) {
              h = rootHeight - y
            }
            if(x + w > rootWidth) {
              w = rootWidth - x
            }
          }
          var l = nodePos.x - x, t = nodePos.y - Math.max(y, 0), r = l + nodePos.w - w, bot = t + nodePos.h - h;
          if(r * l > 0) {
            var s = Math[l < 0 ? "max" : "min"](l, r);
            nodePos.x += scrollLeft;
            scrollLeft += isIE >= 8 && !backCompat && rtl ? -s : s;
            nodePos.x -= scrollLeft
          }
          if(bot * t > 0) {
            nodePos.y += scrollTop;
            scrollTop += Math[t < 0 ? "max" : "min"](t, bot);
            nodePos.y -= scrollTop
          }
        }
      }
      el = el != scrollRoot && !fixedPos && el.parentNode
    }
  }catch(error) {
    console.error("scrollIntoView: " + error);
    node.scrollIntoView(false)
  }
};
midojo.provide("midijit._base.sniff");
(function() {
  var d = midojo, html = d.doc.documentElement, ie = d.isIE, opera = d.isOpera, maj = Math.floor, ff = d.isFF, boxModel = d.boxModel.replace(/-/, ""), classes = {dj_ie:ie, dj_ie6:maj(ie) == 6, dj_ie7:maj(ie) == 7, dj_ie8:maj(ie) == 8, dj_iequirks:ie && d.isQuirks, dj_opera:opera, dj_khtml:d.isKhtml, dj_webkit:d.isWebKit, dj_safari:d.isSafari, dj_chrome:d.isChrome, dj_gecko:d.isMozilla, dj_ff3:maj(ff) == 3};
  classes["dj_" + boxModel] = true;
  for(var p in classes) {
    if(classes[p]) {
      if(html.className) {
        html.className += " " + p
      }else {
        html.className = p
      }
    }
  }
  midojo._loaders.unshift(function() {
    if(!midojo._isBodyLtr()) {
      html.className += " dijitRtl";
      for(var p in classes) {
        if(classes[p]) {
          html.className += " " + p + "-rtl"
        }
      }
    }
  })
})();
midojo.provide("midijit._base.typematic");
midijit.typematic = {_fireEventAndReload:function() {
  this._timer = null;
  this._callback(++this._count, this._node, this._evt);
  this._currentTimeout = Math.max(this._currentTimeout < 0 ? this._initialDelay : this._subsequentDelay > 1 ? this._subsequentDelay : Math.round(this._currentTimeout * this._subsequentDelay), 10);
  this._timer = setTimeout(midojo.hitch(this, "_fireEventAndReload"), this._currentTimeout)
}, trigger:function(evt, _this, node, callback, obj, subsequentDelay, initialDelay) {
  if(obj != this._obj) {
    this.stop();
    this._initialDelay = initialDelay || 500;
    this._subsequentDelay = subsequentDelay || 0.9;
    this._obj = obj;
    this._evt = evt;
    this._node = node;
    this._currentTimeout = -1;
    this._count = -1;
    this._callback = midojo.hitch(_this, callback);
    this._fireEventAndReload()
  }
}, stop:function() {
  if(this._timer) {
    clearTimeout(this._timer);
    this._timer = null
  }
  if(this._obj) {
    this._callback(-1, this._node, this._evt);
    this._obj = null
  }
}, addKeyListener:function(node, keyObject, _this, callback, subsequentDelay, initialDelay) {
  if(keyObject.keyCode) {
    keyObject.charOrCode = keyObject.keyCode;
    midojo.deprecated("keyCode attribute parameter for midijit.typematic.addKeyListener is deprecated. Use charOrCode instead.", "", "2.0")
  }else {
    if(keyObject.charCode) {
      keyObject.charOrCode = String.fromCharCode(keyObject.charCode);
      midojo.deprecated("charCode attribute parameter for midijit.typematic.addKeyListener is deprecated. Use charOrCode instead.", "", "2.0")
    }
  }
  return[midojo.connect(node, "onkeypress", this, function(evt) {
    if(evt.charOrCode == keyObject.charOrCode && (keyObject.ctrlKey === undefined || keyObject.ctrlKey == evt.ctrlKey) && (keyObject.altKey === undefined || keyObject.altKey == evt.altKey) && (keyObject.metaKey === undefined || keyObject.metaKey == (evt.metaKey || false)) && (keyObject.shiftKey === undefined || keyObject.shiftKey == evt.shiftKey)) {
      midojo.stopEvent(evt);
      midijit.typematic.trigger(keyObject, _this, node, callback, keyObject, subsequentDelay, initialDelay)
    }else {
      if(midijit.typematic._obj == keyObject) {
        midijit.typematic.stop()
      }
    }
  }), midojo.connect(node, "onkeyup", this, function(evt) {
    if(midijit.typematic._obj == keyObject) {
      midijit.typematic.stop()
    }
  })]
}, addMouseListener:function(node, _this, callback, subsequentDelay, initialDelay) {
  var dc = midojo.connect;
  return[dc(node, "mousedown", this, function(evt) {
    midojo.stopEvent(evt);
    midijit.typematic.trigger(evt, _this, node, callback, node, subsequentDelay, initialDelay)
  }), dc(node, "mouseup", this, function(evt) {
    midojo.stopEvent(evt);
    midijit.typematic.stop()
  }), dc(node, "mouseout", this, function(evt) {
    midojo.stopEvent(evt);
    midijit.typematic.stop()
  }), dc(node, "mousemove", this, function(evt) {
    midojo.stopEvent(evt)
  }), dc(node, "dblclick", this, function(evt) {
    midojo.stopEvent(evt);
    if(midojo.isIE) {
      midijit.typematic.trigger(evt, _this, node, callback, node, subsequentDelay, initialDelay);
      setTimeout(midojo.hitch(this, midijit.typematic.stop), 50)
    }
  })]
}, addListener:function(mouseNode, keyNode, keyObject, _this, callback, subsequentDelay, initialDelay) {
  return this.addKeyListener(keyNode, keyObject, _this, callback, subsequentDelay, initialDelay).concat(this.addMouseListener(mouseNode, _this, callback, subsequentDelay, initialDelay))
}};
midojo.provide("midijit._base.wai");
midijit.wai = {onload:function() {
  var div = midojo.create("div", {id:"a11yTestNode", style:{cssText:"border: 1px solid;" + "border-color:red green;" + "position: absolute;" + "height: 5px;" + "top: -999px;" + 'background-image: url("' + (midojo.config.blankGif || midojo.moduleUrl("midojo", "resources/blank.gif")) + '");'}}, midojo.body());
  var cs = midojo.getComputedStyle(div);
  if(cs) {
    var bkImg = cs.backgroundImage;
    var needsA11y = cs.borderTopColor == cs.borderRightColor || bkImg != null && (bkImg == "none" || bkImg == "url(invalid-url:)");
    midojo[needsA11y ? "addClass" : "removeClass"](midojo.body(), "dijit_a11y");
    if(midojo.isIE) {
      div.outerHTML = ""
    }else {
      midojo.body().removeChild(div)
    }
  }
}};
if(midojo.isIE || midojo.isMoz) {
  midojo._loaders.unshift(midijit.wai.onload)
}
midojo.mixin(midijit, {_XhtmlRoles:/banner|contentinfo|definition|main|navigation|search|note|secondary|seealso/, hasWaiRole:function(elem, role) {
  var waiRole = this.getWaiRole(elem);
  return role ? waiRole.indexOf(role) > -1 : waiRole.length > 0
}, getWaiRole:function(elem) {
  return midojo.trim((midojo.attr(elem, "role") || "").replace(this._XhtmlRoles, "").replace("wairole:", ""))
}, setWaiRole:function(elem, role) {
  var curRole = midojo.attr(elem, "role") || "";
  if(!this._XhtmlRoles.test(curRole)) {
    midojo.attr(elem, "role", role)
  }else {
    if((" " + curRole + " ").indexOf(" " + role + " ") < 0) {
      var clearXhtml = midojo.trim(curRole.replace(this._XhtmlRoles, ""));
      var cleanRole = midojo.trim(curRole.replace(clearXhtml, ""));
      midojo.attr(elem, "role", cleanRole + (cleanRole ? " " : "") + role)
    }
  }
}, removeWaiRole:function(elem, role) {
  var roleValue = midojo.attr(elem, "role");
  if(!roleValue) {
    return
  }
  if(role) {
    var t = midojo.trim((" " + roleValue + " ").replace(" " + role + " ", " "));
    midojo.attr(elem, "role", t)
  }else {
    elem.removeAttribute("role")
  }
}, hasWaiState:function(elem, state) {
  return elem.hasAttribute ? elem.hasAttribute("aria-" + state) : !!elem.getAttribute("aria-" + state)
}, getWaiState:function(elem, state) {
  return elem.getAttribute("aria-" + state) || ""
}, setWaiState:function(elem, state, value) {
  elem.setAttribute("aria-" + state, value)
}, removeWaiState:function(elem, state) {
  elem.removeAttribute("aria-" + state)
}});
midojo.connect(midojo, "_connect", function(widget, event) {
  if(widget && midojo.isFunction(widget._onConnect)) {
    widget._onConnect(event)
  }
});
midijit._connectOnUseEventHandler = function(event) {
};
midijit._lastKeyDownNode = null;
if(midojo.isIE) {
  (function() {
    var keydownCallback = function(evt) {
      midijit._lastKeyDownNode = evt.srcElement
    };
    midojo.doc.attachEvent("onkeydown", keydownCallback);
    midojo.addOnWindowUnload(function() {
      midojo.doc.detachEvent("onkeydown", keydownCallback)
    })
  })()
}else {
  midojo.doc.addEventListener("keydown", function(evt) {
    midijit._lastKeyDownNode = evt.target
  }, true)
}
(function() {
  var _attrReg = {}, getSetterAttributes = function(widget) {
    var dc = widget.declaredClass;
    if(!_attrReg[dc]) {
      var r = [], attrs, proto = widget.constructor.prototype;
      for(var fxName in proto) {
        if(midojo.isFunction(proto[fxName]) && (attrs = fxName.match(/^_set([a-zA-Z]*)Attr$/)) && attrs[1]) {
          r.push(attrs[1].charAt(0).toLowerCase() + attrs[1].substr(1))
        }
      }
      _attrReg[dc] = r
    }
    return _attrReg[dc] || []
  };
  midojo.declare("midijit._Widget", null, {id:"", lang:"", dir:"", "class":"", style:"", title:"", tooltip:"", srcNodeRef:null, domNode:null, containerNode:null, attributeMap:{id:"", dir:"", lang:"", "class":"", style:"", title:""}, _deferredConnects:{onClick:"", onDblClick:"", onKeyDown:"", onKeyPress:"", onKeyUp:"", onMouseMove:"", onMouseDown:"", onMouseOut:"", onMouseOver:"", onMouseLeave:"", onMouseEnter:"", onMouseUp:""}, onClick:midijit._connectOnUseEventHandler, onDblClick:midijit._connectOnUseEventHandler, 
  onKeyDown:midijit._connectOnUseEventHandler, onKeyPress:midijit._connectOnUseEventHandler, onKeyUp:midijit._connectOnUseEventHandler, onMouseDown:midijit._connectOnUseEventHandler, onMouseMove:midijit._connectOnUseEventHandler, onMouseOut:midijit._connectOnUseEventHandler, onMouseOver:midijit._connectOnUseEventHandler, onMouseLeave:midijit._connectOnUseEventHandler, onMouseEnter:midijit._connectOnUseEventHandler, onMouseUp:midijit._connectOnUseEventHandler, _blankGif:(midojo.config.blankGif || midojo.moduleUrl("midojo", 
  "resources/blank.gif")).toString(), postscript:function(params, srcNodeRef) {
    this.create(params, srcNodeRef)
  }, create:function(params, srcNodeRef) {
    this.srcNodeRef = midojo.byId(srcNodeRef);
    this._connects = [];
    this._subscribes = [];
    this._deferredConnects = midojo.clone(this._deferredConnects);
    for(var attr in this.attributeMap) {
      delete this._deferredConnects[attr]
    }
    for(attr in this._deferredConnects) {
      if(this[attr] !== midijit._connectOnUseEventHandler) {
        delete this._deferredConnects[attr]
      }
    }
    if(this.srcNodeRef && typeof this.srcNodeRef.id == "string") {
      this.id = this.srcNodeRef.id
    }
    if(params) {
      this.params = params;
      midojo.mixin(this, params)
    }
    this.postMixInProperties();
    if(!this.id) {
      this.id = midijit.getUniqueId(this.declaredClass.replace(/\./g, "_"))
    }
    midijit.registry.add(this);
    this.buildRendering();
    if(this.domNode) {
      this._applyAttributes();
      var source = this.srcNodeRef;
      if(source && source.parentNode) {
        source.parentNode.replaceChild(this.domNode, source)
      }
      for(attr in this.params) {
        this._onConnect(attr)
      }
    }
    if(this.domNode) {
      this.domNode.setAttribute("widgetId", this.id)
    }
    this.postCreate();
    if(this.srcNodeRef && !this.srcNodeRef.parentNode) {
      delete this.srcNodeRef
    }
    this._created = true
  }, _applyAttributes:function() {
    var condAttrApply = function(attr, scope) {
      if(scope.params && attr in scope.params || scope[attr]) {
        scope.attr(attr, scope[attr])
      }
    };
    for(var attr in this.attributeMap) {
      condAttrApply(attr, this)
    }
    midojo.forEach(getSetterAttributes(this), function(a) {
      if(!(a in this.attributeMap)) {
        condAttrApply(a, this)
      }
    }, this)
  }, postMixInProperties:function() {
  }, buildRendering:function() {
    this.domNode = this.srcNodeRef || midojo.create("div")
  }, postCreate:function() {
  }, startup:function() {
    this._started = true
  }, destroyRecursive:function(preserveDom) {
    this._beingDestroyed = true;
    this.destroyDescendants(preserveDom);
    this.destroy(preserveDom)
  }, destroy:function(preserveDom) {
    this._beingDestroyed = true;
    this.uninitialize();
    var d = midojo, dfe = d.forEach, dun = d.unsubscribe;
    dfe(this._connects, function(array) {
      dfe(array, d.disconnect)
    });
    dfe(this._subscribes, function(handle) {
      dun(handle)
    });
    dfe(this._supportingWidgets || [], function(w) {
      if(w.destroyRecursive) {
        w.destroyRecursive()
      }else {
        if(w.destroy) {
          w.destroy()
        }
      }
    });
    this.destroyRendering(preserveDom);
    midijit.registry.remove(this.id);
    this._destroyed = true
  }, destroyRendering:function(preserveDom) {
    if(this.bgIframe) {
      this.bgIframe.destroy(preserveDom);
      delete this.bgIframe
    }
    if(this.domNode) {
      if(preserveDom) {
        midojo.removeAttr(this.domNode, "widgetId")
      }else {
        midojo.destroy(this.domNode)
      }
      delete this.domNode
    }
    if(this.srcNodeRef) {
      if(!preserveDom) {
        midojo.destroy(this.srcNodeRef)
      }
      delete this.srcNodeRef
    }
  }, destroyDescendants:function(preserveDom) {
    midojo.forEach(this.getChildren(), function(widget) {
      if(widget.destroyRecursive) {
        widget.destroyRecursive(preserveDom)
      }
    })
  }, uninitialize:function() {
    return false
  }, onFocus:function() {
  }, onBlur:function() {
  }, _onFocus:function(e) {
    this.onFocus()
  }, _onBlur:function() {
    this.onBlur()
  }, _onConnect:function(event) {
    if(event in this._deferredConnects) {
      var mapNode = this[this._deferredConnects[event] || "domNode"];
      this.connect(mapNode, event.toLowerCase(), event);
      delete this._deferredConnects[event]
    }
  }, _setClassAttr:function(value) {
    var mapNode = this[this.attributeMap["class"] || "domNode"];
    midojo.removeClass(mapNode, this["class"]);
    this["class"] = value;
    midojo.addClass(mapNode, value)
  }, _setStyleAttr:function(value) {
    var mapNode = this[this.attributeMap.style || "domNode"];
    if(midojo.isObject(value)) {
      midojo.style(mapNode, value)
    }else {
      if(mapNode.style.cssText) {
        mapNode.style.cssText += "; " + value
      }else {
        mapNode.style.cssText = value
      }
    }
    this.style = value
  }, setAttribute:function(attr, value) {
    midojo.deprecated(this.declaredClass + "::setAttribute() is deprecated. Use attr() instead.", "", "2.0");
    this.attr(attr, value)
  }, _attrToDom:function(attr, value) {
    var commands = this.attributeMap[attr];
    midojo.forEach(midojo.isArray(commands) ? commands : [commands], function(command) {
      var mapNode = this[command.node || command || "domNode"];
      var type = command.type || "attribute";
      switch(type) {
        case "attribute":
          if(midojo.isFunction(value)) {
            value = midojo.hitch(this, value)
          }
          var attrName = command.attribute ? command.attribute : /^on[A-Z][a-zA-Z]*$/.test(attr) ? attr.toLowerCase() : attr;
          midojo.attr(mapNode, attrName, value);
          break;
        case "innerText":
          mapNode.innerHTML = "";
          mapNode.appendChild(midojo.doc.createTextNode(value));
          break;
        case "innerHTML":
          mapNode.innerHTML = value;
          break;
        case "class":
          midojo.removeClass(mapNode, this[attr]);
          midojo.addClass(mapNode, value);
          break
      }
    }, this);
    this[attr] = value
  }, attr:function(name, value) {
    var args = arguments.length;
    if(args == 1 && !midojo.isString(name)) {
      for(var x in name) {
        this.attr(x, name[x])
      }
      return this
    }
    var names = this._getAttrNames(name);
    if(args >= 2) {
      if(this[names.s]) {
        args = midojo._toArray(arguments, 1);
        return this[names.s].apply(this, args) || this
      }else {
        if(name in this.attributeMap) {
          this._attrToDom(name, value)
        }
        this[name] = value
      }
      return this
    }else {
      return this[names.g] ? this[names.g]() : this[name]
    }
  }, _attrPairNames:{}, _getAttrNames:function(name) {
    var apn = this._attrPairNames;
    if(apn[name]) {
      return apn[name]
    }
    var uc = name.charAt(0).toUpperCase() + name.substr(1);
    return apn[name] = {n:name + "Node", s:"_set" + uc + "Attr", g:"_get" + uc + "Attr"}
  }, toString:function() {
    return"[Widget " + this.declaredClass + ", " + (this.id || "NO ID") + "]"
  }, getDescendants:function() {
    return this.containerNode ? midojo.query("[widgetId]", this.containerNode).map(midijit.byNode) : []
  }, getChildren:function() {
    return this.containerNode ? midijit.findWidgets(this.containerNode) : []
  }, nodesWithKeyClick:["input", "button"], connect:function(obj, event, method) {
    var d = midojo, dc = d._connect, handles = [];
    if(event == "ondijitclick") {
      if(!this.nodesWithKeyClick[obj.tagName.toLowerCase()]) {
        var m = d.hitch(this, method);
        handles.push(dc(obj, "onkeydown", this, function(e) {
          if((e.keyCode == d.keys.ENTER || e.keyCode == d.keys.SPACE) && !e.ctrlKey && !e.shiftKey && !e.altKey && !e.metaKey) {
            midijit._lastKeyDownNode = e.target;
            d.stopEvent(e)
          }
        }), dc(obj, "onkeyup", this, function(e) {
          if((e.keyCode == d.keys.ENTER || e.keyCode == d.keys.SPACE) && e.target === midijit._lastKeyDownNode && !e.ctrlKey && !e.shiftKey && !e.altKey && !e.metaKey) {
            midijit._lastKeyDownNode = null;
            return m(e)
          }
        }))
      }
      event = "onclick"
    }
    handles.push(dc(obj, event, this, method));
    this._connects.push(handles);
    return handles
  }, disconnect:function(handles) {
    for(var i = 0;i < this._connects.length;i++) {
      if(this._connects[i] == handles) {
        midojo.forEach(handles, midojo.disconnect);
        this._connects.splice(i, 1);
        return
      }
    }
  }, subscribe:function(topic, method) {
    var d = midojo, handle = d.subscribe(topic, this, method);
    this._subscribes.push(handle);
    return handle
  }, unsubscribe:function(handle) {
    for(var i = 0;i < this._subscribes.length;i++) {
      if(this._subscribes[i] == handle) {
        midojo.unsubscribe(handle);
        this._subscribes.splice(i, 1);
        return
      }
    }
  }, isLeftToRight:function() {
    return midojo._isBodyLtr()
  }, isFocusable:function() {
    return this.focus && midojo.style(this.domNode, "display") != "none"
  }, placeAt:function(reference, position) {
    if(reference.declaredClass && reference.addChild) {
      reference.addChild(this, position)
    }else {
      midojo.place(this.domNode, reference, position)
    }
    return this
  }, _onShow:function() {
    this.onShow()
  }, onShow:function() {
  }, onHide:function() {
  }})
})();
midojo.provide("midijit._Templated");
midojo.provide("midojo.string");
midojo.string.rep = function(str, num) {
  if(num <= 0 || !str) {
    return""
  }
  var buf = [];
  for(;;) {
    if(num & 1) {
      buf.push(str)
    }
    if(!(num >>= 1)) {
      break
    }
    str += str
  }
  return buf.join("")
};
midojo.string.pad = function(text, size, ch, end) {
  if(!ch) {
    ch = "0"
  }
  var out = String(text), pad = midojo.string.rep(ch, Math.ceil((size - out.length) / ch.length));
  return end ? out + pad : pad + out
};
midojo.string.substitute = function(template, map, transform, thisObject) {
  thisObject = thisObject || midojo.global;
  transform = transform ? midojo.hitch(thisObject, transform) : function(v) {
    return v
  };
  return template.replace(/\$\{([^\s\:\}]+)(?:\:([^\s\:\}]+))?\}/g, function(match, key, format) {
    var value = midojo.getObject(key, false, map);
    if(format) {
      value = midojo.getObject(format, false, thisObject).call(thisObject, value, key)
    }
    return transform(value, key).toString()
  })
};
midojo.string.trim = String.prototype.trim ? midojo.trim : function(str) {
  str = str.replace(/^\s+/, "");
  for(var i = str.length - 1;i >= 0;i--) {
    if(/\S/.test(str.charAt(i))) {
      str = str.substring(0, i + 1);
      break
    }
  }
  return str
};
midojo.provide("midojo.parser");
midojo.provide("midojo.date.stamp");
midojo.date.stamp.fromISOString = function(formattedString, defaultTime) {
  if(!midojo.date.stamp._isoRegExp) {
    midojo.date.stamp._isoRegExp = /^(?:(\d{4})(?:-(\d{2})(?:-(\d{2}))?)?)?(?:T(\d{2}):(\d{2})(?::(\d{2})(.\d+)?)?((?:[+-](\d{2}):(\d{2}))|Z)?)?$/
  }
  var match = midojo.date.stamp._isoRegExp.exec(formattedString), result = null;
  if(match) {
    match.shift();
    if(match[1]) {
      match[1]--
    }
    if(match[6]) {
      match[6] *= 1E3
    }
    if(defaultTime) {
      defaultTime = new Date(defaultTime);
      midojo.map(["FullYear", "Month", "Date", "Hours", "Minutes", "Seconds", "Milliseconds"], function(prop) {
        return defaultTime["get" + prop]()
      }).forEach(function(value, index) {
        if(match[index] === undefined) {
          match[index] = value
        }
      })
    }
    result = new Date(match[0] || 1970, match[1] || 0, match[2] || 1, match[3] || 0, match[4] || 0, match[5] || 0, match[6] || 0);
    if(match[0] < 100) {
      result.setFullYear(match[0] || 1970)
    }
    var offset = 0, zoneSign = match[7] && match[7].charAt(0);
    if(zoneSign != "Z") {
      offset = (match[8] || 0) * 60 + (Number(match[9]) || 0);
      if(zoneSign != "-") {
        offset *= -1
      }
    }
    if(zoneSign) {
      offset -= result.getTimezoneOffset()
    }
    if(offset) {
      result.setTime(result.getTime() + offset * 6E4)
    }
  }
  return result
};
midojo.date.stamp.toISOString = function(dateObject, options) {
  var _ = function(n) {
    return n < 10 ? "0" + n : n
  };
  options = options || {};
  var formattedDate = [], getter = options.zulu ? "getUTC" : "get", date = "";
  if(options.selector != "time") {
    var year = dateObject[getter + "FullYear"]();
    date = ["0000".substr((year + "").length) + year, _(dateObject[getter + "Month"]() + 1), _(dateObject[getter + "Date"]())].join("-")
  }
  formattedDate.push(date);
  if(options.selector != "date") {
    var time = [_(dateObject[getter + "Hours"]()), _(dateObject[getter + "Minutes"]()), _(dateObject[getter + "Seconds"]())].join(":");
    var millis = dateObject[getter + "Milliseconds"]();
    if(options.milliseconds) {
      time += "." + (millis < 100 ? "0" : "") + _(millis)
    }
    if(options.zulu) {
      time += "Z"
    }else {
      if(options.selector != "time") {
        var timezoneOffset = dateObject.getTimezoneOffset();
        var absOffset = Math.abs(timezoneOffset);
        time += (timezoneOffset > 0 ? "-" : "+") + _(Math.floor(absOffset / 60)) + ":" + _(absOffset % 60)
      }
    }
    formattedDate.push(time)
  }
  return formattedDate.join("T")
};
midojo.parser = new function() {
  var d = midojo;
  this._attrName = d._scopeName + "Type";
  this._query = "[" + this._attrName + "]";
  function val2type(value) {
    if(d.isString(value)) {
      return"string"
    }
    if(typeof value == "number") {
      return"number"
    }
    if(typeof value == "boolean") {
      return"boolean"
    }
    if(d.isFunction(value)) {
      return"function"
    }
    if(d.isArray(value)) {
      return"array"
    }
    if(value instanceof Date) {
      return"date"
    }
    if(value instanceof d._Url) {
      return"url"
    }
    return"object"
  }
  function str2obj(value, type) {
    switch(type) {
      case "string":
        return value;
      case "number":
        return value.length ? Number(value) : NaN;
      case "boolean":
        return typeof value == "boolean" ? value : !(value.toLowerCase() == "false");
      case "function":
        if(d.isFunction(value)) {
          value = value.toString();
          value = d.trim(value.substring(value.indexOf("{") + 1, value.length - 1))
        }
        try {
          if(value.search(/[^\w\.]+/i) != -1) {
            return new Function(value)
          }else {
            return d.getObject(value, false)
          }
        }catch(e) {
          return new Function
        }
      ;
      case "array":
        return value ? value.split(/\s*,\s*/) : [];
      case "date":
        switch(value) {
          case "":
            return new Date("");
          case "now":
            return new Date;
          default:
            return d.date.stamp.fromISOString(value)
        }
      ;
      case "url":
        return d.baseUrl + value;
      default:
        return d.fromJson(value)
    }
  }
  var instanceClasses = {};
  midojo.connect(midojo, "extend", function() {
    instanceClasses = {}
  });
  function getClassInfo(className) {
    if(!instanceClasses[className]) {
      var cls = d.getObject(className);
      if(!d.isFunction(cls)) {
        throw new Error("Could not load class '" + className + "'. Did you spell the name correctly and use a full path, like 'midijit.form.Button'?");
      }
      var proto = cls.prototype;
      var params = {}, dummyClass = {};
      for(var name in proto) {
        if(name.charAt(0) == "_") {
          continue
        }
        if(name in dummyClass) {
          continue
        }
        var defVal = proto[name];
        params[name] = val2type(defVal)
      }
      instanceClasses[className] = {cls:cls, params:params}
    }
    return instanceClasses[className]
  }
  this._functionFromScript = function(script) {
    var preamble = "";
    var suffix = "";
    var argsStr = script.getAttribute("args");
    if(argsStr) {
      d.forEach(argsStr.split(/\s*,\s*/), function(part, idx) {
        preamble += "var " + part + " = arguments[" + idx + "]; "
      })
    }
    var withStr = script.getAttribute("with");
    if(withStr && withStr.length) {
      d.forEach(withStr.split(/\s*,\s*/), function(part) {
        preamble += "with(" + part + "){";
        suffix += "}"
      })
    }
    return new Function(preamble + script.innerHTML + suffix)
  };
  this.instantiate = function(nodes, mixin, args) {
    var thelist = [], dp = midojo.parser;
    mixin = mixin || {};
    args = args || {};
    d.forEach(nodes, function(node) {
      if(!node) {
        return
      }
      var type = dp._attrName in mixin ? mixin[dp._attrName] : node.getAttribute(dp._attrName);
      if(!type || !type.length) {
        return
      }
      var clsInfo = getClassInfo(type), clazz = clsInfo.cls, ps = clazz._noScript || clazz.prototype._noScript;
      var params = {}, attributes = node.attributes;
      for(var name in clsInfo.params) {
        var item = name in mixin ? {value:mixin[name], specified:true} : attributes.getNamedItem(name);
        if(!item || !item.specified && (!midojo.isIE || name.toLowerCase() != "value")) {
          continue
        }
        var value = item.value;
        switch(name) {
          case "class":
            value = "className" in mixin ? mixin.className : node.className;
            break;
          case "style":
            value = "style" in mixin ? mixin.style : node.style && node.style.cssText
        }
        var _type = clsInfo.params[name];
        if(typeof value == "string") {
          params[name] = str2obj(value, _type)
        }else {
          params[name] = value
        }
      }
      if(!ps) {
        var connects = [], calls = [];
        d.query("> script[type^='midojo/']", node).orphan().forEach(function(script) {
          var event = script.getAttribute("event"), type = script.getAttribute("type"), nf = d.parser._functionFromScript(script);
          if(event) {
            if(type == "midojo/connect") {
              connects.push({event:event, func:nf})
            }else {
              params[event] = nf
            }
          }else {
            calls.push(nf)
          }
        })
      }
      var markupFactory = clazz.markupFactory || clazz.prototype && clazz.prototype.markupFactory;
      var instance = markupFactory ? markupFactory(params, node, clazz) : new clazz(params, node);
      thelist.push(instance);
      var jsname = node.getAttribute("jsId");
      if(jsname) {
        d.setObject(jsname, instance)
      }
      if(!ps) {
        d.forEach(connects, function(connect) {
          d.connect(instance, connect.event, null, connect.func)
        });
        d.forEach(calls, function(func) {
          func.call(instance)
        })
      }
    });
    if(!mixin._started) {
      d.forEach(thelist, function(instance) {
        if(!args.noStart && instance && instance.startup && !instance._started && (!instance.getParent || !instance.getParent())) {
          instance.startup()
        }
      })
    }
    return thelist
  };
  this.parse = function(rootNode, args) {
    var root;
    if(!args && rootNode && rootNode.rootNode) {
      args = rootNode;
      root = args.rootNode
    }else {
      root = rootNode
    }
    var list = d.query(this._query, root);
    return this.instantiate(list, null, args)
  }
};
(function() {
  var parseRunner = function() {
    if(midojo.config.parseOnLoad) {
      midojo.parser.parse()
    }
  };
  if(midojo.exists("midijit.wai.onload") && midijit.wai.onload === midojo._loaders[0]) {
    midojo._loaders.splice(1, 0, parseRunner)
  }else {
    midojo._loaders.unshift(parseRunner)
  }
})();
midojo.provide("midojo.cache");
(function() {
  var cache = {};
  midojo.cache = function(module, url, value) {
    if(typeof module == "string") {
      var pathObj = midojo.moduleUrl(module, url)
    }else {
      pathObj = module;
      value = url
    }
    var key = pathObj.toString();
    var val = value;
    if(value !== undefined && !midojo.isString(value)) {
      val = "value" in value ? value.value : undefined
    }
    var sanitize = value && value.sanitize ? true : false;
    if(val || val === null) {
      if(val == null) {
        delete cache[key]
      }else {
        val = cache[key] = sanitize ? midojo.cache._sanitize(val) : val
      }
    }else {
      if(!(key in cache)) {
        val = midojo._getText(key);
        cache[key] = sanitize ? midojo.cache._sanitize(val) : val
      }
      val = cache[key]
    }
    return val
  };
  midojo.cache._sanitize = function(val) {
    if(val) {
      val = val.replace(/^\s*<\?xml(\s)+version=[\'\"](\d)*.(\d)*[\'\"](\s)*\?>/im, "");
      var matches = val.match(/<body[^>]*>\s*([\s\S]+)\s*<\/body>/im);
      if(matches) {
        val = matches[1]
      }
    }else {
      val = ""
    }
    return val
  }
})();
midojo.declare("midijit._Templated", null, {templateString:null, templatePath:null, widgetsInTemplate:false, _skipNodeCache:false, _earlyTemplatedStartup:false, constructor:function() {
  this._attachPoints = []
}, _stringRepl:function(tmpl) {
  var className = this.declaredClass, _this = this;
  return midojo.string.substitute(tmpl, this, function(value, key) {
    if(key.charAt(0) == "!") {
      value = midojo.getObject(key.substr(1), false, _this)
    }
    if(typeof value == "undefined") {
      throw new Error(className + " template:" + key);
    }
    if(value == null) {
      return""
    }
    return key.charAt(0) == "!" ? value : value.toString().replace(/"/g, "&quot;")
  }, this)
}, buildRendering:function() {
  var cached = midijit._Templated.getCachedTemplate(this.templatePath, this.templateString, this._skipNodeCache);
  var node;
  if(midojo.isString(cached)) {
    node = midojo._toDom(this._stringRepl(cached));
    if(node.nodeType != 1) {
      throw new Error("Invalid template: " + cached);
    }
  }else {
    node = cached.cloneNode(true)
  }
  this.domNode = node;
  this._attachTemplateNodes(node);
  if(this.widgetsInTemplate) {
    var parser = midojo.parser, qry, attr;
    if(parser._query != "[dojoType]") {
      qry = parser._query;
      attr = parser._attrName;
      parser._query = "[dojoType]";
      parser._attrName = "dojoType"
    }
    var cw = this._startupWidgets = midojo.parser.parse(node, {noStart:!this._earlyTemplatedStartup});
    if(qry) {
      parser._query = qry;
      parser._attrName = attr
    }
    this._supportingWidgets = midijit.findWidgets(node);
    this._attachTemplateNodes(cw, function(n, p) {
      return n[p]
    })
  }
  this._fillContent(this.srcNodeRef)
}, _fillContent:function(source) {
  var dest = this.containerNode;
  if(source && dest) {
    while(source.hasChildNodes()) {
      dest.appendChild(source.firstChild)
    }
  }
}, _attachTemplateNodes:function(rootNode, getAttrFunc) {
  getAttrFunc = getAttrFunc || function(n, p) {
    return n.getAttribute(p)
  };
  var nodes = midojo.isArray(rootNode) ? rootNode : rootNode.all || rootNode.getElementsByTagName("*");
  var x = midojo.isArray(rootNode) ? 0 : -1;
  for(;x < nodes.length;x++) {
    var baseNode = x == -1 ? rootNode : nodes[x];
    if(this.widgetsInTemplate && getAttrFunc(baseNode, "dojoType")) {
      continue
    }
    var attachPoint = getAttrFunc(baseNode, "dojoAttachPoint");
    if(attachPoint) {
      var point, points = attachPoint.split(/\s*,\s*/);
      while(point = points.shift()) {
        if(midojo.isArray(this[point])) {
          this[point].push(baseNode)
        }else {
          this[point] = baseNode
        }
        this._attachPoints.push(point)
      }
    }
    var attachEvent = getAttrFunc(baseNode, "dojoAttachEvent");
    if(attachEvent) {
      var event, events = attachEvent.split(/\s*,\s*/);
      var trim = midojo.trim;
      while(event = events.shift()) {
        if(event) {
          var thisFunc = null;
          if(event.indexOf(":") != -1) {
            var funcNameArr = event.split(":");
            event = trim(funcNameArr[0]);
            thisFunc = trim(funcNameArr[1])
          }else {
            event = trim(event)
          }
          if(!thisFunc) {
            thisFunc = event
          }
          this.connect(baseNode, event, thisFunc)
        }
      }
    }
    var role = getAttrFunc(baseNode, "waiRole");
    if(role) {
      midijit.setWaiRole(baseNode, role)
    }
    var values = getAttrFunc(baseNode, "waiState");
    if(values) {
      midojo.forEach(values.split(/\s*,\s*/), function(stateValue) {
        if(stateValue.indexOf("-") != -1) {
          var pair = stateValue.split("-");
          midijit.setWaiState(baseNode, pair[0], pair[1])
        }
      })
    }
  }
}, startup:function() {
  midojo.forEach(this._startupWidgets, function(w) {
    if(w && !w._started && w.startup) {
      w.startup()
    }
  });
  this.inherited(arguments)
}, destroyRendering:function() {
  midojo.forEach(this._attachPoints, function(point) {
    delete this[point]
  }, this);
  this._attachPoints = [];
  this.inherited(arguments)
}});
midijit._Templated._templateCache = {};
midijit._Templated.getCachedTemplate = function(templatePath, templateString, alwaysUseString) {
  var tmplts = midijit._Templated._templateCache;
  var key = templateString || templatePath;
  var cached = tmplts[key];
  if(cached) {
    try {
      if(!cached.ownerDocument || cached.ownerDocument == midojo.doc) {
        return cached
      }
    }catch(e) {
    }
    midojo.destroy(cached)
  }
  if(!templateString) {
    templateString = midojo.cache(templatePath, {sanitize:true})
  }
  templateString = midojo.string.trim(templateString);
  if(alwaysUseString || templateString.match(/\$\{([^\}]+)\}/g)) {
    return tmplts[key] = templateString
  }else {
    var node = midojo._toDom(templateString);
    if(node.nodeType != 1) {
      throw new Error("Invalid template: " + templateString);
    }
    return tmplts[key] = node
  }
};
if(midojo.isIE) {
  midojo.addOnWindowUnload(function() {
    var cache = midijit._Templated._templateCache;
    for(var key in cache) {
      var value = cache[key];
      if(typeof value == "object") {
        midojo.destroy(value)
      }
      delete cache[key]
    }
  })
}
midojo.extend(midijit._Widget, {dojoAttachEvent:"", dojoAttachPoint:"", waiRole:"", waiState:""});
midojo.provide("midijit._Container");
midojo.declare("midijit._Container", null, {isContainer:true, buildRendering:function() {
  this.inherited(arguments);
  if(!this.containerNode) {
    this.containerNode = this.domNode
  }
}, addChild:function(widget, insertIndex) {
  var refNode = this.containerNode;
  if(insertIndex && typeof insertIndex == "number") {
    var children = this.getChildren();
    if(children && children.length >= insertIndex) {
      refNode = children[insertIndex - 1].domNode;
      insertIndex = "after"
    }
  }
  midojo.place(widget.domNode, refNode, insertIndex);
  if(this._started && !widget._started) {
    widget.startup()
  }
}, removeChild:function(widget) {
  if(typeof widget == "number" && widget > 0) {
    widget = this.getChildren()[widget]
  }
  if(widget && widget.domNode) {
    var node = widget.domNode;
    node.parentNode.removeChild(node)
  }
}, getChildren:function() {
  return midojo.query("> [widgetId]", this.containerNode).map(midijit.byNode)
}, hasChildren:function() {
  return midojo.query("> [widgetId]", this.containerNode).length > 0
}, destroyDescendants:function(preserveDom) {
  midojo.forEach(this.getChildren(), function(child) {
    child.destroyRecursive(preserveDom)
  })
}, _getSiblingOfChild:function(child, dir) {
  var node = child.domNode, which = dir > 0 ? "nextSibling" : "previousSibling";
  do {
    node = node[which]
  }while(node && (node.nodeType != 1 || !midijit.byNode(node)));
  return node && midijit.byNode(node)
}, getIndexOfChild:function(child) {
  return midojo.indexOf(this.getChildren(), child)
}, startup:function() {
  if(this._started) {
    return
  }
  midojo.forEach(this.getChildren(), function(child) {
    child.startup()
  });
  this.inherited(arguments)
}});
midojo.provide("ria.legend.OverlayLegend");
midojo.provide("midijit._Contained");
midojo.declare("midijit._Contained", null, {getParent:function() {
  var parent = midijit.getEnclosingWidget(this.domNode.parentNode);
  return parent && parent.isContainer ? parent : null
}, _getSibling:function(which) {
  var node = this.domNode;
  do {
    node = node[which + "Sibling"]
  }while(node && node.nodeType != 1);
  return node && midijit.byNode(node)
}, getPreviousSibling:function() {
  return this._getSibling("previous")
}, getNextSibling:function() {
  return this._getSibling("next")
}, getIndexInParent:function() {
  var p = this.getParent();
  if(!p || !p.getIndexOfChild) {
    return-1
  }
  return p.getIndexOfChild(this)
}});
midojo.provide("midojo.fx");
midojo.provide("midojo.fx.Toggler");
midojo.declare("midojo.fx.Toggler", null, {node:null, showFunc:midojo.fadeIn, hideFunc:midojo.fadeOut, showDuration:200, hideDuration:200, constructor:function(args) {
  var _t = this;
  midojo.mixin(_t, args);
  _t.node = args.node;
  _t._showArgs = midojo.mixin({}, args);
  _t._showArgs.node = _t.node;
  _t._showArgs.duration = _t.showDuration;
  _t.showAnim = _t.showFunc(_t._showArgs);
  _t._hideArgs = midojo.mixin({}, args);
  _t._hideArgs.node = _t.node;
  _t._hideArgs.duration = _t.hideDuration;
  _t.hideAnim = _t.hideFunc(_t._hideArgs);
  midojo.connect(_t.showAnim, "beforeBegin", midojo.hitch(_t.hideAnim, "stop", true));
  midojo.connect(_t.hideAnim, "beforeBegin", midojo.hitch(_t.showAnim, "stop", true))
}, show:function(delay) {
  return this.showAnim.play(delay || 0)
}, hide:function(delay) {
  return this.hideAnim.play(delay || 0)
}});
(function() {
  var d = midojo, _baseObj = {_fire:function(evt, args) {
    if(this[evt]) {
      this[evt].apply(this, args || [])
    }
    return this
  }};
  var _chain = function(animations) {
    this._index = -1;
    this._animations = animations || [];
    this._current = this._onAnimateCtx = this._onEndCtx = null;
    this.duration = 0;
    d.forEach(this._animations, function(a) {
      this.duration += a.duration;
      if(a.delay) {
        this.duration += a.delay
      }
    }, this)
  };
  d.extend(_chain, {_onAnimate:function() {
    this._fire("onAnimate", arguments)
  }, _onEnd:function() {
    d.disconnect(this._onAnimateCtx);
    d.disconnect(this._onEndCtx);
    this._onAnimateCtx = this._onEndCtx = null;
    if(this._index + 1 == this._animations.length) {
      this._fire("onEnd")
    }else {
      this._current = this._animations[++this._index];
      this._onAnimateCtx = d.connect(this._current, "onAnimate", this, "_onAnimate");
      this._onEndCtx = d.connect(this._current, "onEnd", this, "_onEnd");
      this._current.play(0, true)
    }
  }, play:function(delay, gotoStart) {
    if(!this._current) {
      this._current = this._animations[this._index = 0]
    }
    if(!gotoStart && this._current.status() == "playing") {
      return this
    }
    var beforeBegin = d.connect(this._current, "beforeBegin", this, function() {
      this._fire("beforeBegin")
    }), onBegin = d.connect(this._current, "onBegin", this, function(arg) {
      this._fire("onBegin", arguments)
    }), onPlay = d.connect(this._current, "onPlay", this, function(arg) {
      this._fire("onPlay", arguments);
      d.disconnect(beforeBegin);
      d.disconnect(onBegin);
      d.disconnect(onPlay)
    });
    if(this._onAnimateCtx) {
      d.disconnect(this._onAnimateCtx)
    }
    this._onAnimateCtx = d.connect(this._current, "onAnimate", this, "_onAnimate");
    if(this._onEndCtx) {
      d.disconnect(this._onEndCtx)
    }
    this._onEndCtx = d.connect(this._current, "onEnd", this, "_onEnd");
    this._current.play.apply(this._current, arguments);
    return this
  }, pause:function() {
    if(this._current) {
      var e = d.connect(this._current, "onPause", this, function(arg) {
        this._fire("onPause", arguments);
        d.disconnect(e)
      });
      this._current.pause()
    }
    return this
  }, gotoPercent:function(percent, andPlay) {
    this.pause();
    var offset = this.duration * percent;
    this._current = null;
    d.some(this._animations, function(a) {
      if(a.duration <= offset) {
        this._current = a;
        return true
      }
      offset -= a.duration;
      return false
    });
    if(this._current) {
      this._current.gotoPercent(offset / this._current.duration, andPlay)
    }
    return this
  }, stop:function(gotoEnd) {
    if(this._current) {
      if(gotoEnd) {
        for(;this._index + 1 < this._animations.length;++this._index) {
          this._animations[this._index].stop(true)
        }
        this._current = this._animations[this._index]
      }
      var e = d.connect(this._current, "onStop", this, function(arg) {
        this._fire("onStop", arguments);
        d.disconnect(e)
      });
      this._current.stop()
    }
    return this
  }, status:function() {
    return this._current ? this._current.status() : "stopped"
  }, destroy:function() {
    if(this._onAnimateCtx) {
      d.disconnect(this._onAnimateCtx)
    }
    if(this._onEndCtx) {
      d.disconnect(this._onEndCtx)
    }
  }});
  d.extend(_chain, _baseObj);
  midojo.fx.chain = function(animations) {
    return new _chain(animations)
  };
  var _combine = function(animations) {
    this._animations = animations || [];
    this._connects = [];
    this._finished = 0;
    this.duration = 0;
    d.forEach(animations, function(a) {
      var duration = a.duration;
      if(a.delay) {
        duration += a.delay
      }
      if(this.duration < duration) {
        this.duration = duration
      }
      this._connects.push(d.connect(a, "onEnd", this, "_onEnd"))
    }, this);
    this._pseudoAnimation = new d.Animation({curve:[0, 1], duration:this.duration});
    var self = this;
    d.forEach(["beforeBegin", "onBegin", "onPlay", "onAnimate", "onPause", "onStop", "onEnd"], function(evt) {
      self._connects.push(d.connect(self._pseudoAnimation, evt, function() {
        self._fire(evt, arguments)
      }))
    })
  };
  d.extend(_combine, {_doAction:function(action, args) {
    d.forEach(this._animations, function(a) {
      a[action].apply(a, args)
    });
    return this
  }, _onEnd:function() {
    if(++this._finished > this._animations.length) {
      this._fire("onEnd")
    }
  }, _call:function(action, args) {
    var t = this._pseudoAnimation;
    t[action].apply(t, args)
  }, play:function(delay, gotoStart) {
    this._finished = 0;
    this._doAction("play", arguments);
    this._call("play", arguments);
    return this
  }, pause:function() {
    this._doAction("pause", arguments);
    this._call("pause", arguments);
    return this
  }, gotoPercent:function(percent, andPlay) {
    var ms = this.duration * percent;
    d.forEach(this._animations, function(a) {
      a.gotoPercent(a.duration < ms ? 1 : ms / a.duration, andPlay)
    });
    this._call("gotoPercent", arguments);
    return this
  }, stop:function(gotoEnd) {
    this._doAction("stop", arguments);
    this._call("stop", arguments);
    return this
  }, status:function() {
    return this._pseudoAnimation.status()
  }, destroy:function() {
    d.forEach(this._connects, midojo.disconnect)
  }});
  d.extend(_combine, _baseObj);
  midojo.fx.combine = function(animations) {
    return new _combine(animations)
  };
  midojo.fx.wipeIn = function(args) {
    var node = args.node = d.byId(args.node), s = node.style, o;
    var anim = d.animateProperty(d.mixin({properties:{height:{start:function() {
      o = s.overflow;
      s.overflow = "hidden";
      if(s.visibility == "hidden" || s.display == "none") {
        s.height = "1px";
        s.display = "";
        s.visibility = "";
        return 1
      }else {
        var height = d.style(node, "height");
        return Math.max(height, 1)
      }
    }, end:function() {
      return node.scrollHeight
    }}}}, args));
    d.connect(anim, "onEnd", function() {
      s.height = "auto";
      s.overflow = o
    });
    return anim
  };
  midojo.fx.wipeOut = function(args) {
    var node = args.node = d.byId(args.node), s = node.style, o;
    var anim = d.animateProperty(d.mixin({properties:{height:{end:1}}}, args));
    d.connect(anim, "beforeBegin", function() {
      o = s.overflow;
      s.overflow = "hidden";
      s.display = ""
    });
    d.connect(anim, "onEnd", function() {
      s.overflow = o;
      s.height = "auto";
      s.display = "none"
    });
    return anim
  };
  midojo.fx.slideTo = function(args) {
    var node = args.node = d.byId(args.node), top = null, left = null;
    var init = function(n) {
      return function() {
        var cs = d.getComputedStyle(n);
        var pos = cs.position;
        top = pos == "absolute" ? n.offsetTop : parseInt(cs.top) || 0;
        left = pos == "absolute" ? n.offsetLeft : parseInt(cs.left) || 0;
        if(pos != "absolute" && pos != "relative") {
          var ret = d.position(n, true);
          top = ret.y;
          left = ret.x;
          n.style.position = "absolute";
          n.style.top = top + "px";
          n.style.left = left + "px"
        }
      }
    }(node);
    init();
    var anim = d.animateProperty(d.mixin({properties:{top:args.top || 0, left:args.left || 0}}, args));
    d.connect(anim, "beforeBegin", anim, init);
    return anim
  }
})();
midojo.provide("ria.legend.SingleSwatchLegendItem");
midojo.declare("ria.legend.SingleSwatchLegendItem", [midijit._Widget, midijit._Templated, midijit._Contained], {templateString:'<li class="singleSwatchLegendItem" dojoAttachPoint="_masterDivElement">\r\n    <img src="${_iconSourceAttrValue}" dojoAttachPoint="_iconElement"></img>\r\n    <span>${data.label}</span>\r\n</li>\r\n', data:null, isHighlighted:false, _iconElement:null, _iconSourceAttrValue:null, _masterDivElement:null, postMixInProperties:function() {
  this._iconSourceAttrValue = this.data.swatch
}, postCreate:function() {
  if(this.isHighlighted) {
    midojo.addClass(this._masterDivElement, "legendItemHighlightedRowStyling")
  }else {
    midojo.addClass(this._masterDivElement, "legendItemNormalRowStyling")
  }
}});
midojo.provide("ria.legend.MultipleSwatchLegendItem");
midojo.declare("ria.legend.MultipleSwatchLegendItem", [midijit._Widget, midijit._Templated, midijit._Container, midijit._Contained], {templateString:'<li class="multipleSwatchLegendItem" dojoAttachPoint="_masterDivElement">\r\n    <h3 class="header">\r\n        <img src="${_placeholderUrl}" class="minusImage" dojoAttachPoint="_detailStateImg" dojoAttachEvent="onclick:_toggleDetail" />\r\n        <span>${data.title}</span>\r\n    </h3>\r\n    <div class="body">\r\n        <div class="detail">\r\n            <ol dojoAttachPoint="containerNode">\r\n            </ol>\r\n        </div>\r\n    </div>\r\n</li>', 
data:null, isHighlighted:false, _placeholderUrl:"", _detailAnimation:null, _detailExpanded:true, _masterDivElement:null, postMixInProperties:function() {
  this._placeholderUrl = midojo.moduleUrl("ria", "legend/transparentPixel.png").uri
}, postCreate:function() {
  var detail = midojo.query(".detail", this.domNode)[0];
  this._detailAnimation = {node:detail, duration:500};
  for(var i = 0;i < this.data.rows.length;i++) {
    this._addLegendLayerItems(this.data.rows[i])
  }
  if(this.isHighlighted) {
    midojo.addClass(this._masterDivElement, "legendItemHighlightedRowStyling")
  }else {
    midojo.addClass(this._masterDivElement, "legendItemNormalRowStyling")
  }
}, _addLegendLayerItems:function(row) {
  var data = {label:row.description, swatch:row.swatch};
  var layerItem = new ria.legend.SingleSwatchLegendItem({data:data, isHighlighted:this.isHighlighted});
  this.addChild(layerItem)
}, _toggleDetail:function(evt) {
  if(this._detailExpanded == true) {
    midojo.removeClass(this._detailStateImg, "minusImage");
    midojo.addClass(this._detailStateImg, "plusImage");
    midojo.fx.wipeOut(this._detailAnimation).play()
  }else {
    midojo.removeClass(this._detailStateImg, "plusImage");
    midojo.addClass(this._detailStateImg, "minusImage");
    midojo.fx.wipeIn(this._detailAnimation).play()
  }
  this._detailExpanded = !this._detailExpanded
}});
midojo.provide("ria.legend.LegendEvent");
midojo.declare("ria.legend.LegendEvent", null, {_overlayId:null, _visible:false, constructor:function(overlayName, visible) {
  this._overlayId = overlayName;
  this._visible = visible
}, getOverlayName:function() {
  return this._overlayId
}, isVisible:function() {
  return this._visible
}});
midojo.declare("ria.legend.OverlayLegend", [midijit._Widget, midijit._Templated, midijit._Container, midijit._Contained], {templateString:'<li class="overlayLegend">\r\n    <h3 class="header" dojoAttachEvent="onclick:_toggleBody">\r\n        <img src="${_placeholderUrl}" dojoAttachPoint="_bodyStateImg" />\r\n        <span>${name}</span>\r\n        <input type="checkbox" checked="true" dojoAttachPoint= "_checkbox" dojoAttachEvent="onclick:_checkboxClick" />\r\n    </h3>\r\n    <ol dojoAttachPoint="containerNode">\r\n    </ol>\r\n</li>', 
name:null, data:null, _placeholderUrl:null, _bodyStateImg:null, _bodyAnimation:null, _bodyExpanded:true, _checkbox:false, _suppressToggle:false, postMixInProperties:function() {
  this._placeholderUrl = midojo.moduleUrl("ria", "legend/transparentPixel.png").uri
}, postCreate:function() {
  if(this.data.isVisible == true) {
    midojo.addClass(this._bodyStateImg, "plusImage")
  }else {
    midojo.addClass(this._bodyStateImg, "minusImage")
  }
  var layers = this.data.LegendResponse;
  for(var i = 0;i < layers.length;i++) {
    if(i % 2 == 0) {
      this._createLayerItem(layers[i], true)
    }else {
      this._createLayerItem(layers[i], false)
    }
  }
  this._bodyExpanded = this.data.isVisible;
  this._checkbox.checked = this.data.isVisible;
  var ol = midojo.query("ol", this.domNode)[0];
  this._bodyAnimation = {node:ol, duration:500};
  this._toggleBody()
}, _createLayerItem:function(legendItem, isHighlighted) {
  var legendItemCtl;
  if(legendItem.rows.length > 1) {
    legendItemCtl = new ria.legend.MultipleSwatchLegendItem({data:legendItem, isHighlighted:isHighlighted})
  }else {
    var data = {label:legendItem.layerName, swatch:legendItem.rows[0].swatch};
    legendItemCtl = new ria.legend.SingleSwatchLegendItem({data:data, isHighlighted:isHighlighted})
  }
  this.addChild(legendItemCtl)
}, _checkboxClick:function(evt) {
  this._suppressToggle = true;
  var legendEvent = new ria.legend.LegendEvent(this.name, evt.target.checked);
  this.getParent().onOverlayToggled(legendEvent)
}, _toggleBody:function() {
  if(this._suppressToggle) {
    this._suppressToggle = false;
    return
  }
  if(this._bodyExpanded == true) {
    midojo.removeClass(this._bodyStateImg, "minusImage");
    midojo.addClass(this._bodyStateImg, "plusImage");
    midojo.fx.wipeIn(this._bodyAnimation).play()
  }else {
    midojo.removeClass(this._bodyStateImg, "plusImage");
    midojo.addClass(this._bodyStateImg, "minusImage");
    midojo.fx.wipeOut(this._bodyAnimation).play()
  }
  this._bodyExpanded = !this._bodyExpanded
}});
midojo.declare("ria.legend.LegendControl", [midijit._Widget, midijit._Templated, midijit._Container], {templateString:'<div class="legendControl">\r\n    <ol dojoAttachPoint="containerNode">\r\n    </ol>\r\n</div>\r\n', constructor:function(props$$1, element) {
}, renderData:function(data) {
  this.destroyDescendants(true);
  this.onPreRenderData(data);
  var overlayName;
  for(overlayName in data) {
    this._addOverlayLegend(overlayName, data[overlayName])
  }
  this.onLegendControlUpdated()
}, onPreRenderData:function(data$$1) {
}, onLegendControlUpdated:function() {
}, onOverlayToggled:function(legendEvent) {
}, _addOverlayLegend:function(overlayName$$1, overlayLegend) {
  var overlay = new ria.legend.OverlayLegend({name:overlayName$$1, data:overlayLegend});
  this.addChild(overlay)
}});
midojo.provide("ria.legend.LegendEvent");
midojo.declare("ria.legend.LegendEvent", null, {_overlayId:null, _visible:false, constructor:function(overlayName$$2, visible) {
  this._overlayId = overlayName$$2;
  this._visible = visible
}, getOverlayName:function() {
  return this._overlayId
}, isVisible:function() {
  return this._visible
}});
midojo.provide("ria.legend.LegendMapOrchestrator");
midojo.provide("midojo.DeferredList");
midojo.declare("midojo.DeferredList", midojo.Deferred, {constructor:function(list, fireOnOneCallback, fireOnOneErrback, consumeErrors, canceller) {
  this.list = list;
  this.resultList = new Array(this.list.length);
  this.chain = [];
  this.id = this._nextId();
  this.fired = -1;
  this.paused = 0;
  this.results = [null, null];
  this.canceller = canceller;
  this.silentlyCancelled = false;
  if(this.list.length === 0 && !fireOnOneCallback) {
    this.callback(this.resultList)
  }
  this.finishedCount = 0;
  this.fireOnOneCallback = fireOnOneCallback;
  this.fireOnOneErrback = fireOnOneErrback;
  this.consumeErrors = consumeErrors;
  midojo.forEach(this.list, function(d, index) {
    d.addCallback(this, function(r) {
      this._cbDeferred(index, true, r);
      return r
    });
    d.addErrback(this, function(r) {
      this._cbDeferred(index, false, r);
      return r
    })
  }, this)
}, _cbDeferred:function(index, succeeded, result) {
  this.resultList[index] = [succeeded, result];
  this.finishedCount += 1;
  if(this.fired !== 0) {
    if(succeeded && this.fireOnOneCallback) {
      this.callback([index, result])
    }else {
      if(!succeeded && this.fireOnOneErrback) {
        this.errback(result)
      }else {
        if(this.finishedCount == this.list.length) {
          this.callback(this.resultList)
        }
      }
    }
  }
  if(!succeeded && this.consumeErrors) {
    result = null
  }
  return result
}, gatherResults:function(deferredList) {
  var d = new midojo.DeferredList(deferredList, false, true, false);
  d.addCallback(function(results) {
    var ret = [];
    midojo.forEach(results, function(result) {
      ret.push(result[1])
    });
    return ret
  });
  return d
}});
midojo.declare("ria.legend.LegendMapOrchestrator", null, {_map:null, _legendControl:null, _overlays:null, _legendService:null, _handles:null, _resources:null, constructor:function(map, legendControl, legendService) {
  this._map = map;
  this._legendControl = legendControl;
  this._overlays = this._map.getLayersByClass("OpenLayers.Layer.MappingServiceLayer");
  this._legendService = legendService;
  this._handles = [];
  this._resources = midojo.i18n.getLocalization("ria", "resources")
}, init:function() {
  var handle = midojo.connect(this._legendControl, "onOverlayToggled", this, "_handleOverlayToggled");
  this._handles.push(handle);
  this.updateLegendControl()
}, destroy:function() {
  var i$$17 = 0;
  for(;i$$17 < this._handles.length;i$$17++) {
    midojo.disconnect(this._handles[i$$17])
  }
  this._handles = []
}, _handleOverlayToggled:function(legendEvent$$1) {
  var overlay$$1 = legendEvent$$1.getOverlayName();
  var visibility = legendEvent$$1.isVisible();
  var layers = this._map.getLayersByName(overlay$$1);
  if(layers.length > 0) {
    layers[0].setVisibility(visibility)
  }
}, setLegendService:function(legendService$$1) {
  this._legendService = legendService$$1
}, updateLegendControl:function() {
  var list = [];
  var i$$18 = 0;
  for(;i$$18 < this._overlays.length;i$$18++) {
    var deferred = this._legendService.getLegendData(this._overlays[i$$18].getMapName());
    list.push(deferred)
  }
  var deferredList = new midojo.DeferredList(list);
  var cb$$3 = midojo.hitch(this, this._legendServiceSuccessCallback);
  deferredList.addCallback(cb$$3)
}, _legendServiceSuccessCallback:function(data$$2) {
  var overlays = {};
  var i$$19 = 0;
  for(;i$$19 < data$$2.length;i$$19++) {
    if(!data$$2[i$$19][0]) {
      console.error(data$$2[i$$19][1])
    }else {
      var key = this._overlays[i$$19].name;
      var legendResponse = this._removeLowerPriorityLayers(data$$2[i$$19][1]);
      overlays[key] = legendResponse;
      overlays[key].isVisible = this._overlays[i$$19].getVisibility()
    }
  }
  this._legendControl.renderData(overlays)
}, _removeLowerPriorityLayers:function(data$$3) {
  var layers$$1 = data$$3.LegendResponse;
  var preferredLayers = {};
  var updatedLayers = [];
  var i$$20 = 0;
  for(;i$$20 < layers$$1.length;i$$20++) {
    var type = layers$$1[i$$20].type;
    if(type == "FEATURE_STYLE_OVERRIDE") {
      if(preferredLayers[layers$$1[i$$20].layerName] == null) {
        preferredLayers[layers$$1[i$$20].layerName] = layers$$1[i$$20]
      }
    }else {
      if(type == "RANGED_THEME" || type == "INDIVIDUAL_VALUE_THEME") {
        preferredLayers[layers$$1[i$$20].layerName] = layers$$1[i$$20]
      }
    }
  }
  i$$20 = 0;
  for(;i$$20 < layers$$1.length;i$$20++) {
    var layerName = layers$$1[i$$20].layerName;
    if(preferredLayers[layerName]) {
      if(preferredLayers[layerName].type == layers$$1[i$$20].type) {
        updatedLayers.push(layers$$1[i$$20])
      }
    }else {
      updatedLayers.push(layers$$1[i$$20])
    }
  }
  data$$3.LegendResponse = updatedLayers;
  return data$$3
}});
midojo.provide("ria.legend.LegendService");
midojo.provide("ria.RestService");
midojo.declare("ria.RestService", null, {_env:null, constructor:function(env) {
  if(!env) {
    env = new ria.Environment
  }
  this._env = env
}, call:function(url) {
  return midojo.xhrGet({url:url, handleAs:"json", headers:{"X-Requested-With":""}, load:function(result, ioArgs) {
  }, error:function(error, ioArgs) {
  }})
}, makeXdSafe:function(url) {
  var origin = this._env.getOrigin();
  if(url.indexOf(origin) === 0) {
    return url
  }
  return ria.RestService.toProxyUrl(url)
}});
ria.RestService.addProxy = function(path) {
  ria.RestService._proxyUrl = path;
  midojox.io.xhrPlugins.addProxy(path)
};
ria.RestService.toProxyUrl = function(url) {
  if(!ria.RestService._proxyUrl) {
    return url
  }
  return ria.RestService._proxyUrl + encodeURIComponent(url)
};
midojo.provide("ria.Contract");
midojo.declare("ria.Contract", null, {});
ria.Contract.pre = function(condition, message) {
  if(!condition) {
    throw new ria.IllegalArgumentException(message);
  }
};
midojo.declare("ria.legend.LegendService", null, {_service:null, _baseUrl:null, constructor:function(url$$8, restService$$2) {
  if(url$$8.charAt(url$$8.length - 1) != "/") {
    url$$8 = url$$8 + "/"
  }
  if(!restService$$2) {
    restService$$2 = new ria.RestService
  }
  this._baseUrl = url$$8;
  this._service = restService$$2
}, getLegendData:function(overlay$$2) {
  ria.Contract.pre(overlay$$2 || overlay$$2 === "", "Overlay required");
  var d$$3 = this._service.call(this._createUrl(overlay$$2));
  var outerDeferred = new midojo.Deferred;
  d$$3.addCallback(midojo.hitch(this, this._convertSwatchUrls, outerDeferred));
  d$$3.addErrback(function(err$$1) {
    outerDeferred.errback(err$$1)
  });
  return outerDeferred
}, _createUrl:function(overlay$$3) {
  if(overlay$$3.charAt(0) === "/") {
    overlay$$3 = overlay$$3.substring(1)
  }
  return this._baseUrl + "maps/" + this._urlEncode(overlay$$3) + "/legends.json;w=16;h=16;t=png?inlineSwatch=true"
}, _urlEncode:function(str) {
  var ecodedStr = encodeURIComponent(str);
  return ecodedStr.replace(/%2F/g, "/")
}, _convertSwatchUrls:function(deferred$$1, data$$4) {
  var layers$$2 = data$$4.LegendResponse;
  var i$$21 = 0;
  for(;i$$21 < layers$$2.length;i$$21++) {
    var rows = layers$$2[i$$21].rows;
    var j$$3 = 0;
    for(;j$$3 < rows.length;j$$3++) {
      rows[j$$3].swatch = rows[j$$3].swatch
    }
  }
  deferred$$1.callback(data$$4)
}});
midojo.provide("ria.legend.MultipleSwatchLegendItem");
midojo.declare("ria.legend.MultipleSwatchLegendItem", [midijit._Widget, midijit._Templated, midijit._Container, midijit._Contained], {templateString:'<li class="multipleSwatchLegendItem" dojoAttachPoint="_masterDivElement">\r\n    <h3 class="header">\r\n        <img src="${_placeholderUrl}" class="minusImage" dojoAttachPoint="_detailStateImg" dojoAttachEvent="onclick:_toggleDetail" />\r\n        <span>${data.title}</span>\r\n    </h3>\r\n    <div class="body">\r\n        <div class="detail">\r\n            <ol dojoAttachPoint="containerNode">\r\n            </ol>\r\n        </div>\r\n    </div>\r\n</li>', 
data:null, isHighlighted:false, _placeholderUrl:"", _detailAnimation:null, _detailExpanded:true, _masterDivElement:null, postMixInProperties:function() {
  this._placeholderUrl = midojo.moduleUrl("ria", "legend/transparentPixel.png").uri
}, postCreate:function() {
  var detail = midojo.query(".detail", this.domNode)[0];
  this._detailAnimation = {node:detail, duration:500};
  var i$$22 = 0;
  for(;i$$22 < this.data.rows.length;i$$22++) {
    this._addLegendLayerItems(this.data.rows[i$$22])
  }
  if(this.isHighlighted) {
    midojo.addClass(this._masterDivElement, "legendItemHighlightedRowStyling")
  }else {
    midojo.addClass(this._masterDivElement, "legendItemNormalRowStyling")
  }
}, _addLegendLayerItems:function(row$$1) {
  var data$$5 = {label:row$$1.description, swatch:row$$1.swatch};
  var layerItem = new ria.legend.SingleSwatchLegendItem({data:data$$5, isHighlighted:this.isHighlighted});
  this.addChild(layerItem)
}, _toggleDetail:function(evt) {
  if(this._detailExpanded == true) {
    midojo.removeClass(this._detailStateImg, "minusImage");
    midojo.addClass(this._detailStateImg, "plusImage");
    midojo.fx.wipeOut(this._detailAnimation).play()
  }else {
    midojo.removeClass(this._detailStateImg, "plusImage");
    midojo.addClass(this._detailStateImg, "minusImage");
    midojo.fx.wipeIn(this._detailAnimation).play()
  }
  this._detailExpanded = !this._detailExpanded
}});
midojo.provide("ria.legend.OverlayLegend");
midojo.declare("ria.legend.OverlayLegend", [midijit._Widget, midijit._Templated, midijit._Container, midijit._Contained], {templateString:'<li class="overlayLegend">\r\n    <h3 class="header" dojoAttachEvent="onclick:_toggleBody">\r\n        <img src="${_placeholderUrl}" dojoAttachPoint="_bodyStateImg" />\r\n        <span>${name}</span>\r\n        <input type="checkbox" checked="true" dojoAttachPoint= "_checkbox" dojoAttachEvent="onclick:_checkboxClick" />\r\n    </h3>\r\n    <ol dojoAttachPoint="containerNode">\r\n    </ol>\r\n</li>', 
name:null, data:null, _placeholderUrl:null, _bodyStateImg:null, _bodyAnimation:null, _bodyExpanded:true, _checkbox:false, _suppressToggle:false, postMixInProperties:function() {
  this._placeholderUrl = midojo.moduleUrl("ria", "legend/transparentPixel.png").uri
}, postCreate:function() {
  if(this.data.isVisible == true) {
    midojo.addClass(this._bodyStateImg, "plusImage")
  }else {
    midojo.addClass(this._bodyStateImg, "minusImage")
  }
  var layers$$3 = this.data.LegendResponse;
  var i$$23 = 0;
  for(;i$$23 < layers$$3.length;i$$23++) {
    if(i$$23 % 2 == 0) {
      this._createLayerItem(layers$$3[i$$23], true)
    }else {
      this._createLayerItem(layers$$3[i$$23], false)
    }
  }
  this._bodyExpanded = this.data.isVisible;
  this._checkbox.checked = this.data.isVisible;
  var ol = midojo.query("ol", this.domNode)[0];
  this._bodyAnimation = {node:ol, duration:500};
  this._toggleBody()
}, _createLayerItem:function(legendItem, isHighlighted) {
  var legendItemCtl;
  if(legendItem.rows.length > 1) {
    legendItemCtl = new ria.legend.MultipleSwatchLegendItem({data:legendItem, isHighlighted:isHighlighted})
  }else {
    var data$$6 = {label:legendItem.layerName, swatch:legendItem.rows[0].swatch};
    legendItemCtl = new ria.legend.SingleSwatchLegendItem({data:data$$6, isHighlighted:isHighlighted})
  }
  this.addChild(legendItemCtl)
}, _checkboxClick:function(evt$$1) {
  this._suppressToggle = true;
  var legendEvent$$2 = new ria.legend.LegendEvent(this.name, evt$$1.target.checked);
  this.getParent().onOverlayToggled(legendEvent$$2)
}, _toggleBody:function() {
  if(this._suppressToggle) {
    this._suppressToggle = false;
    return
  }
  if(this._bodyExpanded == true) {
    midojo.removeClass(this._bodyStateImg, "minusImage");
    midojo.addClass(this._bodyStateImg, "plusImage");
    midojo.fx.wipeIn(this._bodyAnimation).play()
  }else {
    midojo.removeClass(this._bodyStateImg, "plusImage");
    midojo.addClass(this._bodyStateImg, "minusImage");
    midojo.fx.wipeOut(this._bodyAnimation).play()
  }
  this._bodyExpanded = !this._bodyExpanded
}});
midojo.provide("ria.legend.SingleSwatchLegendItem");
midojo.declare("ria.legend.SingleSwatchLegendItem", [midijit._Widget, midijit._Templated, midijit._Contained], {templateString:'<li class="singleSwatchLegendItem" dojoAttachPoint="_masterDivElement">\r\n    <img src="${_iconSourceAttrValue}" dojoAttachPoint="_iconElement"></img>\r\n    <span>${data.label}</span>\r\n</li>\r\n', data:null, isHighlighted:false, _iconElement:null, _iconSourceAttrValue:null, _masterDivElement:null, postMixInProperties:function() {
  this._iconSourceAttrValue = this.data.swatch
}, postCreate:function() {
  if(this.isHighlighted) {
    midojo.addClass(this._masterDivElement, "legendItemHighlightedRowStyling")
  }else {
    midojo.addClass(this._masterDivElement, "legendItemNormalRowStyling")
  }
}});
({});
midojo.provide("ria.search.FeatureService");
midojo.declare("ria.search.FeatureService", null, {_service:null, _baseUrl:null, constructor:function(url$$9, restService$$3) {
  if(url$$9.charAt(url$$9.length - 1) != "/") {
    url$$9 = url$$9 + "/"
  }
  this._baseUrl = url$$9;
  if(restService$$3 == null) {
    restService$$3 = new ria.RestService
  }
  this._service = restService$$3
}, searchAtPoint:function(point, srs, tableparam) {
  ria.Contract.pre(point, "Please specify point object.");
  ria.Contract.pre(srs, "Please specify srs.");
  ria.Contract.pre(tableparam, "Please specify table param Object.");
  var url$$10 = this._createSearchAtPointUrl(point, srs, tableparam);
  return this._service.call(url$$10)
}, searchById:function(tableName, attributes, Id) {
  ria.Contract.pre(tableName, "Please specify tableName.");
  ria.Contract.pre(Id, "Please specify Id.");
  var url$$11 = this._createSearchByIdRequestURL(tableName, attributes, Id);
  var deferredObj = this._service.call(url$$11);
  return deferredObj
}, searchBySQL:function(query) {
  ria.Contract.pre(query, "Please specify a query");
  var url$$12 = this._createQueryBySqlRequestURL(query);
  var deferredObj$$1 = this._service.call(url$$12);
  return deferredObj$$1
}, searchNearest:function(parameters) {
  ria.Contract.pre(parameters.table, "Table required");
  ria.Contract.pre(parameters.geometry, "Geometry required");
  ria.Contract.pre(parameters.srs, "SRS required");
  return this._service.call(this._createSearchNearestUrl(parameters))
}, _createSearchAtPointUrl:function(point$$1, srs$$1, table) {
  var encodedsrs = encodeURIComponent(srs$$1);
  encodedsrs = encodedsrs.replace(/%3A/g, ":");
  var restUrl = this._baseUrl + "tables" + this._encodeTableName(table.name) + "/features.json";
  if(table.attributes) {
    restUrl += ";attributes=" + this._toCommaSeparatedString(table.attributes)
  }
  restUrl += "?q=searchAtPoint&point=" + point$$1.x + "," + point$$1.y + "," + encodedsrs + "&tolerance=" + encodeURIComponent(table.tolerance);
  return restUrl
}, _createSearchNearestUrl:function(searchNearestParams) {
  var restUrl$$1 = this._baseUrl + "tables" + this._encodeTableName(searchNearestParams.table) + "/features.json";
  if(searchNearestParams.attributes) {
    restUrl$$1 += ";attributes=" + this._toCommaSeparatedString(searchNearestParams.attributes)
  }
  if(searchNearestParams.orderBy) {
    restUrl$$1 += ";orderBy=" + this._toCommaSeparatedString(searchNearestParams.orderBy)
  }
  restUrl$$1 += "?q=searchNearest&geometry=" + this._toEncodedJson(searchNearestParams.geometry, searchNearestParams.srs);
  if(searchNearestParams.withinDistance) {
    restUrl$$1 += "&withinDistance=" + encodeURIComponent(searchNearestParams.withinDistance)
  }
  if(searchNearestParams.distanceAttributeName) {
    restUrl$$1 += "&distanceAttributeName=" + encodeURIComponent(searchNearestParams.distanceAttributeName)
  }
  if(searchNearestParams.maxFeatures) {
    restUrl$$1 += "&maxFeatures=" + searchNearestParams.maxFeatures
  }
  return restUrl$$1
}, _toCommaSeparatedString:function(array) {
  var str$$1 = "";
  var i$$24 = 0;
  for(;i$$24 < array.length;i$$24++) {
    if(i$$24 > 0) {
      str$$1 += ","
    }
    str$$1 += encodeURIComponent(array[i$$24])
  }
  return str$$1
}, _toEncodedJson:function(geometry, srs$$2) {
  var formatter = new OpenLayers.Format.GeoJSON;
  var obj$$8 = formatter.extract.geometry.apply(formatter, [geometry]);
  var srsObj = {type:"name", properties:{name:srs$$2}};
  obj$$8.crs = srsObj;
  var jsonFormatter = new OpenLayers.Format.JSON;
  var json = jsonFormatter.write(obj$$8);
  return encodeURIComponent(json)
}, _encodeTableName:function(tableName$$1) {
  var encodedTableName = encodeURIComponent(tableName$$1);
  encodedTableName = encodedTableName.replace(/%2F/g, "/");
  return encodedTableName
}, _createQueryBySqlRequestURL:function(query$$1) {
  var encodedQuery = encodeURIComponent(query$$1);
  return this._baseUrl + "tables/features.json?q=" + encodedQuery
}, _createSearchByIdRequestURL:function(tableName$$2, attributes$$1, Id$$1) {
  var encodedTableName$$1 = encodeURIComponent(tableName$$2);
  encodedTableName$$1 = encodedTableName$$1.replace(/%2F/g, "/");
  var attrString = "";
  if(attributes$$1 && attributes$$1.length > 0) {
    attrString = ";attributes=";
    var i$$25 = 0;
    for(;i$$25 < attributes$$1.length;i$$25++) {
      if(i$$25 != attributes$$1.length - 1) {
        attrString = attrString + attributes$$1[i$$25] + ","
      }else {
        attrString = attrString + attributes$$1[i$$25]
      }
    }
  }
  return this._baseUrl + "tables" + encodedTableName$$1 + "/features.json" + "/" + Id$$1 + attrString
}});
midojo.provide("ria.search.MultiTableFeatureService");
midojo.declare("ria.search.MultiTableFeatureService", null, {_featureService:null, _url:null, constructor:function(url$$13) {
  if(url$$13.charAt(url$$13.length - 1) != "/") {
    this._url = url$$13 + "/"
  }else {
    this._url = url$$13
  }
  this._featureService = new ria.search.FeatureService(this._url)
}, searchAtPoint:function(point$$2, srs$$3, tableparameters) {
  ria.Contract.pre(point$$2, "Please specify point object.");
  ria.Contract.pre(srs$$3, "Please specify srs.");
  ria.Contract.pre(tableparameters, "Please specify table param Object.");
  var list$$1 = [];
  var i$$26 = 0;
  for(;i$$26 < tableparameters.length;i$$26++) {
    var deferred$$2 = this._featureService.searchAtPoint(point$$2, srs$$3, tableparameters[i$$26]);
    list$$1[i$$26] = deferred$$2
  }
  var deferredList$$1 = new midojo.DeferredList(list$$1);
  return deferredList$$1
}});
midojo.provide("ria.template.CustomHtmlTemplate");
midojo.provide("ria.template.Template");
midojo.declare("ria.template.Template", null, {format:function(obj) {
  return""
}});
midojo.declare("ria.template.CustomHtmlTemplate", ria.template.Template, {constructor:function(templateString) {
  this._templateString = templateString
}, format:function(obj$$9) {
  var template = this._templateString;
  var placeholders = this._getPlaceholdersFromTemplate(template);
  if(placeholders === null) {
    return template
  }
  var i$$27 = 0;
  for(;i$$27 < placeholders.length;i$$27++) {
    var attribute = this._getAttributeNameFromPlaceholder(placeholders[i$$27]);
    var value$$2 = obj$$9[attribute];
    if(value$$2 || typeof value$$2 === "boolean") {
      template = this._replacePlaceholderWithValue(template, placeholders[i$$27], value$$2)
    }else {
      template = this._replacePlaceholderWithMissingValue(template, placeholders[i$$27])
    }
  }
  return template
}, _getPlaceholdersFromTemplate:function(template$$1) {
  return template$$1.match(/@\{.*?\}/g)
}, _getAttributeNameFromPlaceholder:function(placeholder) {
  if(placeholder.indexOf(":") !== -1) {
    return placeholder.match(/@\{(.*)\:/)[1]
  }else {
    return placeholder.match(/@\{(.*)\}/)[1]
  }
}, _replacePlaceholderWithValue:function(template$$2, placeholder$$1, value$$3) {
  if(this._isSupportedType(value$$3)) {
    return template$$2.replace(placeholder$$1, value$$3.toString())
  }else {
    return this._replacePlaceholderWithMissingValue(template$$2, placeholder$$1)
  }
}, _isSupportedType:function(value$$4) {
  var type$$1 = typeof value$$4;
  return type$$1 === "string" || type$$1 === "number" || type$$1 === "boolean"
}, _replacePlaceholderWithMissingValue:function(template$$3, placeholder$$2) {
  var missing = "";
  if(placeholder$$2.indexOf(":") !== -1) {
    missing = placeholder$$2.match(/:(.*)\}/)[1]
  }
  return template$$3.replace(placeholder$$2, missing)
}});
midojo.provide("ria.template.DefaultTemplate");
midojo.provide("midojox.html.entities");
(function() {
  var _applyEncodingMap = function(str, map) {
    var mapper, regexp;
    if(map._encCache && map._encCache.regexp && map._encCache.mapper && map.length == map._encCache.length) {
      mapper = map._encCache.mapper;
      regexp = map._encCache.regexp
    }else {
      mapper = {};
      regexp = ["["];
      var i;
      for(i = 0;i < map.length;i++) {
        mapper[map[i][0]] = "&" + map[i][1] + ";";
        regexp.push(map[i][0])
      }
      regexp.push("]");
      regexp = new RegExp(regexp.join(""), "g");
      map._encCache = {mapper:mapper, regexp:regexp, length:map.length}
    }
    str = str.replace(regexp, function(c) {
      return mapper[c]
    });
    return str
  };
  var _applyDecodingMap = function(str, map) {
    var mapper, regexp;
    if(map._decCache && map._decCache.regexp && map._decCache.mapper && map.length == map._decCache.length) {
      mapper = map._decCache.mapper;
      regexp = map._decCache.regexp
    }else {
      mapper = {};
      regexp = ["("];
      var i;
      for(i = 0;i < map.length;i++) {
        var e = "&" + map[i][1] + ";";
        if(i) {
          regexp.push("|")
        }
        mapper[e] = map[i][0];
        regexp.push(e)
      }
      regexp.push(")");
      regexp = new RegExp(regexp.join(""), "g");
      map._decCache = {mapper:mapper, regexp:regexp, length:map.length}
    }
    str = str.replace(regexp, function(c) {
      return mapper[c]
    });
    return str
  };
  midojox.html.entities.html = [["&", "amp"], ['"', "quot"], ["<", "lt"], [">", "gt"], ["\u00a0", "nbsp"]];
  midojox.html.entities.latin = [["\u00a1", "iexcl"], ["\u00a2", "cent"], ["\u00a3", "pound"], ["\u20ac", "euro"], ["\u00a4", "curren"], ["\u00a5", "yen"], ["\u00a6", "brvbar"], ["\u00a7", "sect"], ["\u00a8", "uml"], ["\u00a9", "copy"], ["\u00aa", "ordf"], ["\u00ab", "laquo"], ["\u00ac", "not"], ["\u00ad", "shy"], ["\u00ae", "reg"], ["\u00af", "macr"], ["\u00b0", "deg"], ["\u00b1", "plusmn"], ["\u00b2", "sup2"], ["\u00b3", "sup3"], ["\u00b4", "acute"], ["\u00b5", "micro"], ["\u00b6", "para"], ["\u00b7", 
  "middot"], ["\u00b8", "cedil"], ["\u00b9", "sup1"], ["\u00ba", "ordm"], ["\u00bb", "raquo"], ["\u00bc", "frac14"], ["\u00bd", "frac12"], ["\u00be", "frac34"], ["\u00bf", "iquest"], ["\u00c0", "Agrave"], ["\u00c1", "Aacute"], ["\u00c2", "Acirc"], ["\u00c3", "Atilde"], ["\u00c4", "Auml"], ["\u00c5", "Aring"], ["\u00c6", "AElig"], ["\u00c7", "Ccedil"], ["\u00c8", "Egrave"], ["\u00c9", "Eacute"], ["\u00ca", "Ecirc"], ["\u00cb", "Euml"], ["\u00cc", "Igrave"], ["\u00cd", "Iacute"], ["\u00ce", "Icirc"], 
  ["\u00cf", "Iuml"], ["\u00d0", "ETH"], ["\u00d1", "Ntilde"], ["\u00d2", "Ograve"], ["\u00d3", "Oacute"], ["\u00d4", "Ocirc"], ["\u00d5", "Otilde"], ["\u00d6", "Ouml"], ["\u00d7", "times"], ["\u00d8", "Oslash"], ["\u00d9", "Ugrave"], ["\u00da", "Uacute"], ["\u00db", "Ucirc"], ["\u00dc", "Uuml"], ["\u00dd", "Yacute"], ["\u00de", "THORN"], ["\u00df", "szlig"], ["\u00e0", "agrave"], ["\u00e1", "aacute"], ["\u00e2", "acirc"], ["\u00e3", "atilde"], ["\u00e4", "auml"], ["\u00e5", "aring"], ["\u00e6", 
  "aelig"], ["\u00e7", "ccedil"], ["\u00e8", "egrave"], ["\u00e9", "eacute"], ["\u00ea", "ecirc"], ["\u00eb", "euml"], ["\u00ec", "igrave"], ["\u00ed", "iacute"], ["\u00ee", "icirc"], ["\u00ef", "iuml"], ["\u00f0", "eth"], ["\u00f1", "ntilde"], ["\u00f2", "ograve"], ["\u00f3", "oacute"], ["\u00f4", "ocirc"], ["\u00f5", "otilde"], ["\u00f6", "ouml"], ["\u00f7", "divide"], ["\u00f8", "oslash"], ["\u00f9", "ugrave"], ["\u00fa", "uacute"], ["\u00fb", "ucirc"], ["\u00fc", "uuml"], ["\u00fd", "yacute"], 
  ["\u00fe", "thorn"], ["\u00ff", "yuml"], ["\u0192", "fnof"], ["\u0391", "Alpha"], ["\u0392", "Beta"], ["\u0393", "Gamma"], ["\u0394", "Delta"], ["\u0395", "Epsilon"], ["\u0396", "Zeta"], ["\u0397", "Eta"], ["\u0398", "Theta"], ["\u0399", "Iota"], ["\u039a", "Kappa"], ["\u039b", "Lambda"], ["\u039c", "Mu"], ["\u039d", "Nu"], ["\u039e", "Xi"], ["\u039f", "Omicron"], ["\u03a0", "Pi"], ["\u03a1", "Rho"], ["\u03a3", "Sigma"], ["\u03a4", "Tau"], ["\u03a5", "Upsilon"], ["\u03a6", "Phi"], ["\u03a7", "Chi"], 
  ["\u03a8", "Psi"], ["\u03a9", "Omega"], ["\u03b1", "alpha"], ["\u03b2", "beta"], ["\u03b3", "gamma"], ["\u03b4", "delta"], ["\u03b5", "epsilon"], ["\u03b6", "zeta"], ["\u03b7", "eta"], ["\u03b8", "theta"], ["\u03b9", "iota"], ["\u03ba", "kappa"], ["\u03bb", "lambda"], ["\u03bc", "mu"], ["\u03bd", "nu"], ["\u03be", "xi"], ["\u03bf", "omicron"], ["\u03c0", "pi"], ["\u03c1", "rho"], ["\u03c2", "sigmaf"], ["\u03c3", "sigma"], ["\u03c4", "tau"], ["\u03c5", "upsilon"], ["\u03c6", "phi"], ["\u03c7", "chi"], 
  ["\u03c8", "psi"], ["\u03c9", "omega"], ["\u03d1", "thetasym"], ["\u03d2", "upsih"], ["\u03d6", "piv"], ["\u2022", "bull"], ["\u2026", "hellip"], ["\u2032", "prime"], ["\u2033", "Prime"], ["\u203e", "oline"], ["\u2044", "frasl"], ["\u2118", "weierp"], ["\u2111", "image"], ["\u211c", "real"], ["\u2122", "trade"], ["\u2135", "alefsym"], ["\u2190", "larr"], ["\u2191", "uarr"], ["\u2192", "rarr"], ["\u2193", "darr"], ["\u2194", "harr"], ["\u21b5", "crarr"], ["\u21d0", "lArr"], ["\u21d1", "uArr"], ["\u21d2", 
  "rArr"], ["\u21d3", "dArr"], ["\u21d4", "hArr"], ["\u2200", "forall"], ["\u2202", "part"], ["\u2203", "exist"], ["\u2205", "empty"], ["\u2207", "nabla"], ["\u2208", "isin"], ["\u2209", "notin"], ["\u220b", "ni"], ["\u220f", "prod"], ["\u2211", "sum"], ["\u2212", "minus"], ["\u2217", "lowast"], ["\u221a", "radic"], ["\u221d", "prop"], ["\u221e", "infin"], ["\u2220", "ang"], ["\u2227", "and"], ["\u2228", "or"], ["\u2229", "cap"], ["\u222a", "cup"], ["\u222b", "int"], ["\u2234", "there4"], ["\u223c", 
  "sim"], ["\u2245", "cong"], ["\u2248", "asymp"], ["\u2260", "ne"], ["\u2261", "equiv"], ["\u2264", "le"], ["\u2265", "ge"], ["\u2282", "sub"], ["\u2283", "sup"], ["\u2284", "nsub"], ["\u2286", "sube"], ["\u2287", "supe"], ["\u2295", "oplus"], ["\u2297", "otimes"], ["\u22a5", "perp"], ["\u22c5", "sdot"], ["\u2308", "lceil"], ["\u2309", "rceil"], ["\u230a", "lfloor"], ["\u230b", "rfloor"], ["\u2329", "lang"], ["\u232a", "rang"], ["\u25ca", "loz"], ["\u2660", "spades"], ["\u2663", "clubs"], ["\u2665", 
  "hearts"], ["\u2666", "diams"], ["\u0152", "Elig"], ["\u0153", "oelig"], ["\u0160", "Scaron"], ["\u0161", "scaron"], ["\u0178", "Yuml"], ["\u02c6", "circ"], ["\u02dc", "tilde"], ["\u2002", "ensp"], ["\u2003", "emsp"], ["\u2009", "thinsp"], ["\u200c", "zwnj"], ["\u200d", "zwj"], ["\u200e", "lrm"], ["\u200f", "rlm"], ["\u2013", "ndash"], ["\u2014", "mdash"], ["\u2018", "lsquo"], ["\u2019", "rsquo"], ["\u201a", "sbquo"], ["\u201c", "ldquo"], ["\u201d", "rdquo"], ["\u201e", "bdquo"], ["\u2020", "dagger"], 
  ["\u2021", "Dagger"], ["\u2030", "permil"], ["\u2039", "lsaquo"], ["\u203a", "rsaquo"]];
  midojox.html.entities.encode = function(str, m) {
    if(str) {
      if(!m) {
        str = _applyEncodingMap(str, midojox.html.entities.html);
        str = _applyEncodingMap(str, midojox.html.entities.latin)
      }else {
        str = _applyEncodingMap(str, m)
      }
    }
    return str
  };
  midojox.html.entities.decode = function(str, m) {
    if(str) {
      if(!m) {
        str = _applyDecodingMap(str, midojox.html.entities.html);
        str = _applyDecodingMap(str, midojox.html.entities.latin)
      }else {
        str = _applyDecodingMap(str, m)
      }
    }
    return str
  }
})();
midojo.declare("ria.template.DefaultTemplate", ria.template.Template, {format:function(obj$$10) {
  var html = '<table class="defaultTemplate">';
  var p$$2;
  for(p$$2 in obj$$10) {
    var name$$4 = this._encode(p$$2);
    var value$$5 = this._encode(this._getString(obj$$10[p$$2]));
    html += '<tr><td class="name">' + name$$4 + "</td>" + '<td class="value">' + value$$5 + "</td></tr>"
  }
  html += "</table>";
  return html
}, _getString:function(value$$6) {
  if(typeof value$$6 === "string" || typeof value$$6 === "number" || typeof value$$6 === "boolean") {
    return value$$6.toString()
  }else {
    return""
  }
}, _encode:function(value$$7) {
  return midojox.html.entities.encode(value$$7)
}});
midojo.provide("ria.template.FeatureCollectionsFormatter");
midojo.declare("ria.template.FeatureCollectionsFormatter", null, {_templateRepository:null, constructor:function(templateRepository) {
  this._templateRepository = templateRepository
}, format:function(tables, featureCollections) {
  var content = '<ul class="featureCollections">';
  var i$$28 = 0;
  for(;i$$28 < featureCollections.length;i$$28++) {
    content += "<li>";
    content += "<h2>";
    content += tables[i$$28];
    content += "</h2>";
    content += this._formatFeatureCollection(tables[i$$28], featureCollections[i$$28]);
    content += "</li>"
  }
  content += "</ul>";
  return content
}, _formatFeatureCollection:function(name$$5, featureCollection) {
  var content$$1 = '<ul class="features">';
  var features = featureCollection.features;
  var template$$4 = this._templateRepository.getTemplate(name$$5);
  if(features) {
    var i$$29 = 0;
    for(;i$$29 < features.length;i$$29++) {
      content$$1 += "<li>";
      content$$1 += this._formatFeature(features[i$$29], template$$4);
      content$$1 += "</li>"
    }
  }
  content$$1 += "</ul>";
  return content$$1
}, _formatFeature:function(feature, template$$5) {
  return template$$5.format(feature.properties)
}, _generateTableContent:function(table$$1, feature$$1) {
  var content$$2 = '<div><span class="riaTemplatedLayerName">' + table$$1 + "</span>";
  var template$$6 = this._templateRepository.getTemplate(table$$1);
  var x$$3 = 0;
  for(;x$$3 < feature$$1.features.length;x$$3++) {
    var json$$1 = eval(feature$$1.features[x$$3].properties);
    content$$2 += "<div>" + template$$6.format(json$$1) + "</div>"
  }
  content$$2 += "</div>";
  return content$$2
}});
midojo.provide("ria.template.Template");
midojo.declare("ria.template.Template", null, {format:function(obj$$11) {
  return""
}});
midojo.provide("ria.template.TemplateRepository");
midojo.provide("ria.template.DefaultTemplate");
midojo.declare("ria.template.DefaultTemplate", ria.template.Template, {format:function(obj) {
  var html = '<table class="defaultTemplate">';
  for(var p in obj) {
    var name = this._encode(p);
    var value = this._encode(this._getString(obj[p]));
    html += '<tr><td class="name">' + name + "</td>" + '<td class="value">' + value + "</td></tr>"
  }
  html += "</table>";
  return html
}, _getString:function(value) {
  if(typeof value === "string" || typeof value === "number" || typeof value === "boolean") {
    return value.toString()
  }else {
    return""
  }
}, _encode:function(value) {
  return midojox.html.entities.encode(value)
}});
midojo.declare("ria.template.TemplateRepository", null, {_defaultTemplate:null, _templates:null, constructor:function() {
  this._templates = {};
  this._defaultTemplate = new ria.template.DefaultTemplate
}, setTemplate:function(name$$6, template$$7) {
  this._templates[name$$6] = template$$7
}, getTemplate:function(name$$7) {
  var template$$8 = this._templates[name$$7];
  if(template$$8) {
    return template$$8
  }
  return this._defaultTemplate
}});
midojo.provide("ria.nls.resources");
ria.nls.resources._built = true;
midojo.provide("ria.nls.resources.ROOT");
ria.nls.resources.ROOT = {};
